@include('header')

<?php if (Auth::user()->role == 0 || Auth::user()->role == 7) {?>
    
<div class="row"> 
  <div class="col-md-2 pull-right">
    <br/>
    <a  href="<?php echo url('/contact/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add Contact</a>
    <br/>
  </div>
</div>
  <?php } ?>
<div class="row">

  <div class="col-md-12">

    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>

        </div>
        <div class="portlet-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover filter_table" id="contact_queue">
              <thead>
                <tr>
                  <th> Name </th>
                  <th> Email </th>
                  <th> Phone </th>
                  <th> Location </th>
                  <?php if(Auth::user()->role == 0 || Auth::user()->role == 7) {?>
                  <th> Edit </th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>


               <?php 
               $i  = 1;
               $html = "";

               if(count($list) > 0){
                foreach($list as $key=>$row)  { 
                  $html .= '<tr class="row-'.$row['con_id'].' list">';
                  $html .= '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
                  $html .= '<td>'.$row['email'].'</td>';
                  $html .= '<td>'.$row['phone_number'].'</td>';
                  $html .= '<td>'.$row['location_name'].'</td>';
                  if(Auth::user()->role == 0 || Auth::user()->role == 7){
                    $html .= '<td><a class="btn btn-xs blue" href="'.url('/contact/update/').'/'.$row->con_id.'"><i class="fa fa-edit"></i></a> -  
                                        <a class="delete btn btn-xs red" data-url="'.url('/contact/delete/').'/'.$row->con_id.'" href="javascript:void(0);" data-remove="row-'.$row['con_id'].'"><i class="fa fa-trash"></i></a></td>';
                  }
                  $html .= '</tr>';
                }
                echo $html;
              }
              else {
                echo '<tr><td colspan="6" align="center"><h4>You have not any Stores. to add <a href="'.url('/').'/'.$module.'/rss/add">click here</a></h4></td><tr>'; 
              }
              ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
  @include('footer')
</div>