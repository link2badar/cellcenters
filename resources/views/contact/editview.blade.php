@include('header')
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/contact/update/'.$data_row['con_id'])}}" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-body row">
                    
                    <div class="col-md-6">
                      <label class="control-label">First Name* :</label>
                        <input type="text" class="form-control" required="required" id="first_name" name="first_name" value="{{@$data_row['first_name']}}" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">Last Name*:</label>
                        <input type="text" class="form-control" required="required" id="last_name" name="last_name"  value="{{@$data_row['last_name']}}">
                    </div>
                   
                    <div class="col-md-6">
                      <label class="control-label">Email Address*</label>
                        <input type="text" class="form-control" required="required"  name="email" value="{{@$data_row['email']}}" >
                    </div>
                    
                    <div class="col-md-6">
                      <label class="control-label">Phone Number </label>
                        <input type="text" class="form-control" name="phone_number" value="{{@$data_row['phone_number']}}"  >
                    </div>
                    

                     <div class="col-md-12 form-group {{ ($errors->has('phone_number')? 'has-error': '' )}}">
                                <label class="control-label">Clinics  </label>
                                <select type="text" class="form-control select2 preferredClinics" name="location_id" style="width:100%">
                                    <?php
                                        if(!empty($clinics)):
                                            foreach ($clinics as $key=>$val) {
                                              if ($data_row['location_id'] == $val['loc_id']) {
                                                $sel = 'selected="selected"';
                                              }else{
                                                $sel = '';
                                              }
                                                echo '<option '.$sel.' value="'.$val['loc_id'].'">'.$val['location_name'].'</option>';
                                            }
                                        endif;
                                    ?>
                                </select>
                            </div>

                    <div class="col-md-12">
                      <label class="control-label">Address </label>
                        <input type="text" class="form-control" name="address" value="{{@$data_row['address']}}" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">City </label>
                        <input type="text" class="form-control" name="city" value="{{@$data_row['city']}}" >
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">State </label>
                        <input type="text" class="form-control" name="state" value="{{@$data_row['state']}}" >
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">Zip/Postal Code  </label>
                        <input type="text" class="form-control" name="zipcode" value="@if($data_row['zipcode']  != '0'){{@$data_row['zipcode']}}@endif" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">How Did You Hear About Us?* </label>
                        <select type="text" class="form-control select2" required="required" name="haboutus" id="houaboutus">
                          <option value="">Choose How Did You Hear About Us</option>
                          <?php 
                          $arr = config('constants.haboutus');
                          foreach ($arr as $key=>$val) {
                            $selected = ($key==$data_row['haboutus'])?'selected="selected"':"";
                            echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                          }
                          ?>
                          
                        </select>
                    </div>
                <?php if( $data_row['haboutus'] == 6) { ?>
                      <div class="col-md-6" id="refrel_name">
                      <label class="control-label">Referal Name</label>
                        <input type="text" class="form-control" name="refrel_name" id="required_referal" placeholder="Enter name of the person who refered you" value="{{@$data_row['refrel_name']}}">
                    </div>

                <?php }else{ ?>
                    <div class="col-md-6 no_diplay" id="refrel_name">
                      <label class="control-label">Referal Name</label>
                        <input type="text" class="form-control" name="refrel_name" id="required_referal" placeholder="Enter name of the person who refered you" value="{{@$data_row['refrel_name']}}">
                    </div>
                  <?php } ?>
                    <div class="col-md-12">
                      <label class="control-label">Message:  </label>
                        <textarea  class="form-control" name="Message" id="summernote_1" >{{@$data_row['Message']}}</textarea>
                    </div>
                    
                  </div>
                 
                  
                  <div class="form-actions">
                    <span class="pull-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('footer')
</div>