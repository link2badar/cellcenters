@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/seminar-user/update/').'/'.$user['seminar_user_id'] }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="seminar_user_id" value="{{ $user['seminar_user_id'] }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('first_name')? 'has-error': '' )}}">
                                <label class="control-label" for="first_name">First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ $user['first_name'] }}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('last_name')? 'has-error': '' )}}">
                                <label class="control-label" for="last_name">Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ $user['last_name'] }}">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{ $user['email'] }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="phone_no">Phone Number </label>
                                <input type="text" class="form-control" name="phone_no" id="phone_no" placeholder="Phone Number" value="{{ $user['phone_no'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="cell_no">Cell Phone Number </label>
                                <input type="text" class="form-control" name="cell_no" id="cell_no" placeholder="Cell Phone Number" value="{{ $user['cell_no'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="address">Address </label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{ $user['address'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="city">City </label>
                                <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ $user['city'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="state">State </label>
                                <input type="text" class="form-control" name="state" id="state" placeholder="State" value="{{ $user['state'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Date of Birth (MM/DD/YYYY)  </label>
                                <input type="text" class="form-control" name="date_birth" placeholder="(MM/DD/YYYY)" value="{{ $user['date_birth'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Gender  </label>
                                <select type="text" class="form-control select2" name="gender">
                                    <option value="">Choose One Option</option>
                                    <?php 
                                        $arr    =   config('constants.gender');
                                        foreach ($arr as $key => $val) {
                                        $selected = ($key==$user['gender'])?'selected="selected"':"";
                                        echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>                       
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="user_image">User Image  </label>
                                <input type="file" class="form-control upload-image" name="user_image" id="user_image" placeholder="user Image">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="zipcode">Zip/Postal Code  </label>
                                <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip/Postal Code" value="{{ $user['zipcode'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                @if(isset($user['image']) && !empty($user['image']))
                                    <image src="{{ url('/users_imgs').'/'.$user['image']}}" style="width:100px;height:100px" class="view_upload_image" />
                                @else
                                   <image src="{{ url('/') }}/assets/img/preview.jpg" style="width:100px;height:100px" class="view_upload_image" />
                                @endif 
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')