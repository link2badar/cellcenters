@include('header')
<br/>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
      </div>
      <div class="portlet-body form">
        <form role="form" action="{{url('/template/update/'.$data_row['id'])}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-body row">

            <div class="col-md-6">
              <label class="control-label" for="title">Title* :</label>
              <input type="text" class="form-control" required="required" id="title" name="title" value="{{@$data_row['title']}}" >
            </div>

            <div class="col-md-12 form-group">
              <label class="control-label">Note:  </label>
              <textarea  class="form-control ckeditor" name="template_data"><?php echo $data_row['template_data']; ?></textarea>
            </div>
          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  @include('footer')
</div>