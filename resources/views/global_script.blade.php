<script type="text/javascript">    
  var site_url = "{{url('/')}}";

  $(document).on("click", ".image-viewer", function(event) {
    var viewer = ImageViewer();
    viewer.show($(this).attr('src'));
});

  $(document).on("change","select[name='seminar_filter']",function(){
    console.log($(this).val());
});

  $(document).ready(function() {
    /* Init Select2 and hide selected option */
    $('.select2').select2();
    $("select").on("select2:select", function (evt) {
      var element = evt.params.data.element;
      var $element = $(element);
      $element.detach();
      $(this).append($element);
      $(this).trigger("change");
  });
    $("select[name='seminar_admin_ids[]']").select2({
      maximumSelectionLength: 4
  });

    $('.datepicker').datepicker({
      todayHighlight:  true,
      format: "yyyy/mm/dd",
      autoclose: true,
  });

    $('.timepicker').timepicker();

        $('.list thead input[type="checkbox"]').click(function(event) { //on click 
            if (this.checked) { // check select status
                $('.list tbody input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = true; //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.list tbody input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        $('.singcheck input[type="checkbox"]').click(function(event) { //on click 
            if (this.checked) { // check select status
                $('.mulcheck input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = true; //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.mulcheck input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        /* MAKE AJAX CALL */
        $(document).on("submit", "form.make_ajax", function(event) {
           event.preventDefault();
           var form    =   $(this).serialize();
           var btn     =   "form.make_ajax button[type=submit]";
           var btntxt  =   $(btn).html();
           type        =   $(this).attr("method");
           addWait(btn, "working...");
           $.ajax({
             type:       $(this).attr("method"),
             cache:      false,
             data:       form,
             url:        $(this).attr("action"),
             dataType:   "json",
             type:       type,
             headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             success: function(res) {
               removeWait(btn, btntxt);
               toastr["success"](res.msg, "Completed!");
               if(typeof(res.reload) !== 'undefined' && res.reload == true){
                 location.reload();
             }
         }
     });
           return false;
       });

        $('select[name="registrant_attendance"]').on('change',function(){
           $.ajax({
             type:       "POST",
             cache:      false,
             data:       {param:$(this).val()},
             url:        site_url+"/seminar/registrant-attendance",
             dataType:   "json",
             headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             success: function(res) {
               removeWait(btn, btntxt);
               toastr["success"](res.msg, "Completed!");
           }
       });
       });
        $('.preferredClinics').on('change',function(){
           $.ajax({
             type:       "POST",
             cache:      false,
             data:       {param:$(this).val()},
             url:        site_url+"/patient/filter-provider",
             dataType:   "json",
             headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             success: function(res) {
               filteredResult  = ''; filteredPhysicianResult ='';
               for(var i = 0; i < res.manager.length; i++) {
                 filteredResult += '<option value="'+res.manager[i].id+'">'+res.manager[i].name+'</option>';
             }
             for (var i = 0; i < res.physician.length; i++) {
                 filteredPhysicianResult += '<option value="'+res.physician[i].id+'">'+res.physician[i].name+'</option>';
             }
             patients = '<option value="">Choose A Patient</option>';
             if (res.patients.length > 0 ) {
                for (var i = 0; i < res.patients.length; i++) {
                    patients += '<option value="'+res.patients[i].pa_id+'">'+res.patients[i].first_name+' '+ res.patients[i].last_name+'</option>';
                }                
            }
            $('#patient_id').html(patients);
             $('select[name="case_manager"]').html(filteredResult);
             $('select[name="primary_physician"]').html(filteredPhysicianResult);
             $('#primary_physician').html(filteredPhysicianResult);
             
         }

     });
       });
    });

function loadModal(url, param) {
  $("#data_modal .modal-content").html('<p style="text-align: center;"><i class="fa fa-spinner fa-spin"></i>  Please wait loading...</p>');
  if (typeof(param) === 'undefined') param = null;
  url = site_url +'/'+ url + "?param=" + param;
  console.log(url);
  $.ajax({
    type: "GET",
    cache: false,
    url: url,
    success: function(result) {
      $("#data_modal .modal-content").html(result);
      $('.select2').select2({});
      tinymce.init({ selector: '#mytextarea' });
  }
});
}

function addWait(dom, lable) {
  $(dom).attr("disabled", "disabled");
  string = '<i class="fa fa-spinner fa-spin"></i> ' + lable;
  $(dom).html(string);
}

function removeWait(dom, lable) {
  $(dom).removeAttr("disabled");
  $(dom).html(lable);
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.upload-image-preview').attr('src', e.target.result);
  }
  reader.readAsDataURL(input.files[0]);
}
}

$(".upload-image").change(function() {
  readURL(this);
});

$(".appointment_patient").change(function() {

   /*  event.preventDefault();*/

   id =   $(this).val();
   url   = "/filter/conditions/";
   url = site_url + url + id;
   $.ajax({
      type: "GET",
      cache: false,
      url: url,
      success: function(result) {
        var results = result.split(',');
        $('.concerns').select2().val(results).trigger("change");
    }
});

});

$("#houaboutus").change(function() {

   $("#required_referal").prop("required", false);
   jQuery('#refrel_name').addClass('no_diplay');

   id =   $(this).val();
   if( id == 6 ){
      jQuery('#refrel_name').removeClass('no_diplay');
      $("#required_referal").prop('required',true);
  }

});

$("#location_select").change(function() {

   id =   $(this).val();
   url   = "/update/sessionlocation/";
   url = site_url + url + id;
   $.ajax({
     type: "GET",
     cache: false,
     url: url,
     success: function(result) {
       window.location.href=site_url;
   }
});
});
var doctorID = 0; var onchangeChecker = false;
$("#primary_physician").change(function() {
  doctorID      =   $(this).val();
  onchangeChecker = true;

});

$("#physicianFilter").change(function() {
  doctorID      =   $(this).val();
  approvalCheck =   $("#approvalCheck").val();
  if (approvalCheck == 0) {
    url     =   "/calendar/";
}
else{
    url     =   "/appointments/cancel/";
}
url     =   site_url + url + doctorID;
$.ajax({
    type: "GET",
    cache: false,
    async: false,
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  url: url,
  success: function(result) {
      window.location.href=url;
  }
});
});

/*    $(function(){
  */     $("#appointment_date").change(function() {
    id      =   $(this).val();
    if (!onchangeChecker) {
      doctorID = $("#primary_physician").val();
      onchangeChecker = false;
  }
  locID = $("#preferredClinics").val();
  dd      =   moment(id, 'YYYY/MM/DD').format('dd MM D YYYY');
  d       =   new Date(dd);
  d       =   d.getDay();
  url     =   "/appointment/filtertime/";
  url     =   site_url + url + d + '/' + doctorID+'?date='+id+'&locid='+locID;
  result  =   0;
  $.ajax({
      type: "GET",
      cache: false,
      async: false,
      dataType: "json",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: url,
    success: function(result) {
        if (result != 0) {
          html = '';
          for (var i = 0; i< result.from.length; i++) {
            html += '<option value="'+result.from[i]+'">'+result.from[i]+'</option>';
        }
        $("#from").html(html);
        html = '';
        for (var i = 0; i< result.to.length; i++) {
            html += '<option value="'+result.to[i]+'">'+result.to[i]+'</option>';
        }
        $("#to").html(html);
    }
    else{
       $("#from").html('<option value="">Choose An Start Time</option>');
       $("#to").html('<option value="">Choose An End Time</option>');
   }
}
});
});
  /* });*/









  $("#appointment_status").change(function() {
     $("#required_reason").prop("required", false);
     jQuery('#cancel_reason').addClass('no_diplay');

     id =   $(this).val();
     if( id == 3 ){
        jQuery('#cancel_reason').removeClass('no_diplay');
        $("#required_reason").prop('required',true);
    }

});



  $("#locationFilter").change(function() {
      locationID      =   $(this).val();
      approvalCheck   =   $("#approvalCheck").val();
      if (approvalCheck == 0) {
        url     =   "/calendar/";
    }
    else{
        url     =   "/appointments/cancel/";
    }
    url     =   site_url + url + doctorID;
    $.ajax({
        type: "GET",
        cache: false,
        async: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: url,
      success: function(result) {
          window.location.href=url;
      }
  });
});




  $(document).on("click", ".list .delete", function(event) {
    url      = $(this).attr("data-url");
    remvove  = $(this).attr("data-remove");
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this record!",
      type: "warning",
      showCancelButton:   true,
      confirmButtonClass: "btn-danger",
      confirmButtonText:  "Yes, delete it!",
      cancelButtonText:   "No, cancel !",
      closeOnConfirm:     false,
      closeOnCancel:      false
  },
  function(isConfirm) {
      if(isConfirm) {
        $.ajax({
          type:  "GET",
          cache: false,
          url:   url,
          dataType: "json",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(res) {
            if(res.flag == true) {
              swal("Deleted!", "Your record has been deleted.", "success");
              $("." + remvove).remove();
              if(typeof(res.reload) !== 'undefined' && res.reload == true){
                location.reload();
            }
        }
    }
});
    }else{
        swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
});
});

  var today = new Date();
  var seminardate = today.getDate()+'_'+(today.getMonth()+1)+'_'+today.getFullYear();
  $(document).ready(function() {

    $('#seminars_tables').dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
              "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [

            {
              extend: 'csv',
              text: 'Export CSV',
              exportOptions: {
                 columns: [1,2,3,4,5]
             },
             title: 'Seminar_data_'+seminardate,
             footer: true,
             className: 'btn purple btn-outline '
         }

         ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
            [0, 'asc']
            ],

            "lengthMenu": [
            [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        });

    $('td').prop('tabIndex', -1);
});

  $(document).ready(function() {

    $('#registrants_tables').dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
              "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [

            {
              extend: 'csv',
              text: 'Export CSV',
              exportOptions: {
                 columns: [1,3,4,5,7]
             },
             title: 'Registrant_data_'+seminardate,
             footer: true,
             className: 'btn purple btn-outline '
         }

         ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
            [0, 'asc']
            ],

            "lengthMenu": [
            [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        });

    $('td').prop('tabIndex', -1);
});

  $(document).ready(function() {

    $('.filter_table').dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
              "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [

            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
            [0, 'asc']
            ],

            "lengthMenu": [
            [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        });

    $('td').prop('tabIndex', -1);
});

  $('#fakeEmail').click(function(){
    first_name = $('#first_name').val();
    last_name  = $('#last_name').val();
    number     = Math.floor(Math.random()*(999-100+1)+100);
    fakeemail  = first_name+last_name+number+"_fake@"+window.location.hostname;
    $('#fakeemail').val(fakeemail);

});

  $(document).on("click", "#add_transaction" , function(event) {
     payment.addForm();
 });

  $('.selectTransaction').change(function(){
    selected = $('.selectTransaction').val();
    if( selected != "Payment" ){
       transaction.notPayment()
   }
   if( selected == "Payment" ){
       transaction.Payment()
   }
});

  $('#payment_type_1').change(function(){
    selected = $('#payment_type_1').val();

    if( selected == "credit" ){
       paymentType.creditCard()
   }
   if( selected == "check" ){
       paymentType.checkNumber()
   }
   if( selected == "financing" ){
       paymentType.finaNcing()
   }
   if( selected == "cash" ){
       paymentType.cashPayment()
   }
});




  var transaction = {

    notPayment : function(){
      jQuery('#notpaymenttype').removeClass('no_diplay');
      jQuery('#paymenttype').addClass('no_diplay');

  },

  Payment : function(){
      jQuery('#paymenttype').removeClass('no_diplay');
      jQuery('#notpaymenttype').addClass('no_diplay');
  }
}

var paymentType = {

 creditCard : function(){
   jQuery('#creditcard').removeClass('no_diplay');
   jQuery('#checknumber').addClass('no_diplay');
   jQuery('#financing').addClass('no_diplay');

},

checkNumber : function(){
   jQuery('#creditcard').addClass('no_diplay');
   jQuery('#checknumber').removeClass('no_diplay');
   jQuery('#financing').addClass('no_diplay');

},

finaNcing : function(){
   jQuery('#creditcard').addClass('no_diplay');
   jQuery('#checknumber').addClass('no_diplay');
   jQuery('#financing').removeClass('no_diplay');
},

cashPayment : function(){
   jQuery('#creditcard').addClass('no_diplay');
   jQuery('#checknumber').addClass('no_diplay');
   jQuery('#financing').addClass('no_diplay');
},

}

</script>
@if(session()->has('message'))
<script type="text/javascript">toastr["success"]('{{ session()->get('message') }}',"Completed!" );</script>
@endif
@if(session()->has('danger_message'))
<script type="text/javascript">toastr["error"]('{{ session()->get('danger_message') }}',"Not Uploaded!" );</script>
@endif
@if(session()->has('warning-message'))
<script type="text/javascript">toastr["warning"]('{{ session()->get('warning-message') }}',"Oops!" );</script>
@endif



