@include('header')
<br>
<div class="row">
    <div class="col-md-12">
    	<span class="pull-right">
           <a href="{{ url('patient/add')}}" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Patient</a>
           <a href="{{ url('appointment/add')}}" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Appointment</a>
       </span>
   </div>
   <div class="col-md-12" style="margin-top:12px;">
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-layers font-green"></i>
                <span class="caption-subject font-green sbold uppercase">Calendar</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-4">
                    <a class="btn btn-primary" href="?date=<?php echo date('d/m/Y');?>">Today</a>
                    <a class="btn btn-primary" href="?date=<?php echo date('d/m/Y', strtotime('-7 days'));?>"><</a>
                    <a class="btn btn-primary" href="?date=<?php echo date('d/m/Y', strtotime('+7 days'));?>">></a>
                    
                </div>
                <div class="col-md-4"><?php echo date("D, M d, Y"); ?> <input type="date" onchange="changeCalendarType(this.value)" style="padding: 10px 20px;"></div>
                <div class="col-md-4">
                    <a class="btn btn-primary">Daily view</a>
                    <a class="btn btn-primary" href="?date=<?php echo date('d/m/Y');?>">Weekly view</a>
                    <a class="btn btn-primary">show cancellation</a>
                </div>
            </div><vr /><br />
            <div class="row">
                <div class="col-md-12"><div  style="float: left"><i class="fa fa-key"></i> Key:&nbsp;&nbsp;&nbsp; </div>
                <?php 
                $calendarTypes    =   config('constants.calendarTypes');
                $calendarColor   =   config('constants.calendarColor');

                foreach ($calendarTypes as $key => $value) {

                    echo '<div style="float: left"><div class="calendarColor" style="background:'.$calendarColor[$key].'"></div><b> '.$value.'&nbsp;&nbsp;&nbsp;&nbsp;</b></div>';
                }
                ?>
            </div>
        </div>
        <vr /><br />
        <div class="row">
                
                    <div class="col-md-12 col-sm-12">
                        <div id="calendar" class="has-toolbar"> </div>
                        <div class="mycal" style="width:100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')