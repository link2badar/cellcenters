@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/appointment/update_reschedule')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="appointment_id" value="<?php echo $appointment['appointment_id']; ?>">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Physician</label>
                            <select disabled="disabled" class="select2" name="doctor_id" style="width:100%" id="doctor_id" required>
                                <option value="">Choose A Physician</option>
                                @if(!empty($physicians))
                                    @foreach($physicians as $physician)
                                        <option {{ $physician['id'] == $appointment['doctor_id'] ? "selected" : " " }} value="{{ $physician['id'] }}"  >{{ $physician['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="patient">Patient</label>
                            <select disabled="disabled" class="select2 appointment_patient"  name="patient_id" style="width:100%" id="patient_id" required>
                                <option value="">Choose A Patient</option>
                                @if(!empty($patients))
                                    @foreach($patients as $patient)
                                        <option value="{{ $patient['pa_id'] }}" 
                                        {{ $patient['pa_id'] == $appointment['patient_id'] ? "selected" : " " }} >{{ $patient['first_name'].' '.$patient['last_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                         <div class="col-md-6 form-group">
                            <label class="control-label" for="appointment_type">Appointment Type </label>
                            <select disabled="disabled" class="select2" name="appointment_type" style="width:100%" id="appointment_type" required>
                                <option value="">Choose A Appointment Type</option>
                                <?php $appointment_types = config('constants.appointment_types'); ?>
                                @if(!empty($appointment_types))
                                    @foreach($appointment_types as $key => $appointment_type)
                                        <option value="{{ $key }}" 
                                        {{ $key == $appointment['appointment_type'] ? "selected" : " " }}>{{ $appointment_type }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Date (MM/DD/YYYY)</label>
                            <div class="input-group">
                                <input required disabled="disabled"  type="text" class="form-control required datepicker" placeholder=" (MM/DD/YYYY) " name="appointment_date" id="appointment_date"
                                value="{{ $appointment['appointment_date'] }}">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="start_time">Available Start Time (* indicates booked times) </label>
                            <select class="select2" disabled="disabled" name="start_time" style="width:100%" id="start_time" required>
                                <option value="">Choose an Available Start Time</option>
                                                   <?php 
                        $array = array(); 

                        $hour = 0; 
                        $min = 0; // Lets start at "00:15" 
                        $length = 24 * 2; // The number of times we need to run the loop 
                        for ($i=0;$i<$length;++$i) 
                        { 
                          $array[] = str_pad($hour, 2, "0", STR_PAD_LEFT) .':'. str_pad($min, 2, "0", STR_PAD_LEFT); 
                          if ($min < 30) { $min = $min + 30; } else { $min = 0; ++$hour; } 
                      } 

/*                        $array[] = "00:00"; // Adds the last line to the end of the array
*/                        foreach ($array as $key => $value) {
                            $selectedslot = '';
                            if (date('h:i a' , strtotime($value)) == $appointment['start_time']) {
                                $selectedslot = 'selected="selected"';
                            }
                            echo '<option '.$selectedslot.' value="'.date('h:i a' , strtotime($value)).'">'.date('h:i a' , strtotime($value)).'</option>';                             
                         } 
                        ?>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="end_time">Available End Time  </label>
                            <select class="select2" disabled="disabled" name="end_time" style="width:100%" id="end_time" required>
                                <option value="">Choose An End Time</option>
                                <?php 
                        $array = array(); 

                        $hour = 0; 
                        $min = 0; // Lets start at "00:15" 
                        $length = 24 * 2; // The number of times we need to run the loop 
                        for ($i=0;$i<$length;++$i) 
                        { 
                          $array[] = str_pad($hour, 2, "0", STR_PAD_LEFT) .':'. str_pad($min, 2, "0", STR_PAD_LEFT); 
                          if ($min < 30) { $min = $min + 30; } else { $min = 0; ++$hour; } 
                      } 

/*                        $array[] = "00:00"; // Adds the last line to the end of the array
*/                        foreach ($array as $key => $value) {
                            $selectedslot = '';
                            if (date('h:i a' , strtotime($value)) == $appointment['end_time']) {
                                $selectedslot = 'selected="selected"';
                            }
                            echo '<option '.$selectedslot.' value="'.date('h:i a' , strtotime($value)).'">'.date('h:i a' , strtotime($value)).'</option>';                             
                         } 
                        ?>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label">Primary Concern(s)</label>
                            <select disabled="disabled" multiple="multiple" type="text" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="primary_concerns[]">
                                <?php 
                                    $arr = config('constants.primary_concerns');
                                    $selected_concerns   = explode(',', $appointment['primary_concerns']);
                                    foreach ($arr as $key=>$val) {
                                        $sel =  (isset($selected_concerns) && in_array($key , $selected_concerns)) ? "selected" : "";
                                        echo '<option value="'.$key.'" '.$sel.'>'.$val.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <br>
                         <div class="col-md-12 form-group">
                                <label class="control-label bold">Completed Forms:</label>
                                <textarea  class="form-control ckeditor" name="completed_forms">{{ $appointment['completed_forms'] }}</textarea>
                            </div>
                        <br>
                        <div class="col-md-12 form-group">
                                <label class="control-label bold">Consultation Notes:</label>
                                <textarea  class="form-control ckeditor" name="consultation_notes">{{ $appointment['consultation_notes'] }}</textarea>
                            </div>
                            <br>
                            <div class="col-md-12 form-group">
                                <label class="control-label bold">Exam Notes: </label>
                                <textarea  class="form-control ckeditor" name="exam_notes">{{ $appointment['exam_notes'] }}</textarea>
                            </div>
                            <br>
                            <div class="col-md-12 form-group">
                                <label class="control-label bold">Treatment Notes:</label>
                                <textarea  class="form-control ckeditor" name="treatment_notes">{{ $appointment['treatment_notes'] }}</textarea>
                            </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')