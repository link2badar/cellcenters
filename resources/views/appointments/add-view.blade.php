@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/appointment/add')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body row">
                      <?php if (Auth::user()->role == 0 || Auth::user()->role == 7) {?>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="preferredClinics">Locations</label>
                            <select class="select2 preferredClinics" name="preferredClinics" style="width:100%" id="preferredClinics" required>
                                <option value="">Choose A location</option>
                                <?php 
                                if(!empty($locations)){
                                    foreach ($locations as $location) {
                                       echo '<option  value="'.$location['loc_id'].'">'.$location['location_name'].'</option>';
                                   }
                               }
                               ?>
                           </select>
                       </div>
                     <?php } ?>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Physician</label>
                            <select class="select2" name="doctor_id" style="width:100%" id="primary_physician" required>
                                <option value="">Choose A Physician</option>
                                <?php 
                                if(!empty($physicians)){
                                    foreach ($physicians as $physician) {
                                        $selected  = "";
                                        if(isset($data_row['primary_physician'])){
                                           $selected = ($physician['id']==$data_row['primary_physician'])?'selected="selected"':"";

                                       }
                                       echo '<option '.$selected.' value="'.$physician['id'].'">'.$physician['name'].'</option>';
                                   }
                               }
                               ?>
                           </select>
                       </div>
                       <div class="col-md-6 form-group">
                        <label class="control-label" for="patient">Patient</label>
                        <select class="select2 appointment_patient"  name="patient_id" style="width:100%" id="patient_id" required>
                            <option value="">Choose A Patient</option>
                            <?php 
                            if(!empty($patients)){
                                foreach ($patients as $patient) {
                                    $selected  = "";
                                    if(isset($data_row['pa_id'])){
                                       $selected = ($patient['pa_id']==$data_row['pa_id'])?'selected="selected"':"";

                                   }
                                   echo '<option '.$selected.' value="'.$patient['pa_id'].'">'.$patient['first_name'].' '.$patient['last_name'].'</option>';
                               }
                           }
                           ?>
                       </select>
                   </div>
                   <div class="col-md-6 form-group">
                    <label class="control-label" for="appointment_type">Appointment Type </label>
                    <select class="select2" name="appointment_type" style="width:100%" id="appointment_type" required>
                        <option value="">Choose A Appointment Type</option>
                        <?php $appointment_types = config('constants.appointment_types'); ?>
                        @if(!empty($appointment_types))
                        @foreach($appointment_types as $key => $appointment_type)
                        <option value="{{ $key }}">{{ $appointment_type }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label class="control-label" for="date">Date (MM/DD/YYYY)</label>
                    <div class="input-group">
                        <?php if (isset($_GET['date'])) {
                            $date = date("Y/m/d", strtotime($_GET['date']));
                        }else{
                            $date = date('Y/m/d');
                        } ?>
                        <input required  type="text" class="form-control datepicker" value="<?php //echo $date ?>" placeholder=" (YYYY/MM/DD) " name="appointment_date" id="appointment_date">
                        <span class="input-group-addon bg-blue bg-font-blue">
                            <i class="fa fa-calendar-plus-o"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label" for="start_time">Available Start Time (* indicates booked times) </label>
                    <select class="select2" name="start_time" style="width:100%" id="from" required>
                        <option value="">Choose an Available Start Time</option>
                    </select>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label" for="end_time">Available End Time  </label>
                    <select class="select2" name="end_time" style="width:100%" id="to" required>
                        <option value="">Choose An End Time</option>
                    </select>
                </div>
                <div class="col-md-12 form-group">
                    <label class="control-label">Primary Concern(s)</label>
                    <select multiple="multiple" type="text" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="primary_concerns[]">
                        <?php 
                        if(isset($data_row['primary_concern'])){
                           $selected_concerns   = explode(',', $data_row['primary_concern']);

                       }
                       foreach ($primaryConcern as $key=>$val) {
                           $sel =  (isset($data_row['primary_concern']) && in_array($val['id'] , $selected_concerns)) ? "selected" : "";
                           echo '<option value="'.$val['id'].'">'.$val['title'].'</option>';
                       }
                       ?>
                   </select>
               </div>
           </div>
           <div class="form-actions">
            <span class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
            </span>
        </div>
    </form>
</div>
</div>
</div>
</div>
@include('footer')