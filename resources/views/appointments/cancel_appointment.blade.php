@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/appointment/change_status')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="appointment_id" value="<?php echo $appointment['appointment_id']; ?>">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Physician</label>
                            <select disabled="disabled" class="select2" name="doctor_id" style="width:100%" id="doctor_id" required>
                                <option value="">Choose A Physician</option>
                                @if(!empty($physicians))
                                    @foreach($physicians as $physician)
                                        <option {{ $physician['id'] == $appointment['doctor_id'] ? "selected" : " " }} value="{{ $physician['id'] }}"  >{{ $physician['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="patient">Patient</label>
                            <select disabled="disabled" class="select2 appointment_patient"  name="patient_id" style="width:100%" id="patient_id" required>
                                <option value="">Choose A Patient</option>
                                @if(!empty($patients))
                                    @foreach($patients as $patient)
                                        <option value="{{ $patient['pa_id'] }}" 
                                        {{ $patient['pa_id'] == $appointment['patient_id'] ? "selected" : " " }} >{{ $patient['first_name'].' '.$patient['last_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                         <div class="col-md-6 form-group">
                            <label class="control-label" for="appointment_type">Appointment Type </label>
                            <select disabled="disabled" class="select2" name="appointment_type" style="width:100%" id="appointment_type" required>
                                <option value="">Choose A Appointment Type</option>
                                <?php $appointment_types = config('constants.appointment_types'); ?>
                                @if(!empty($appointment_types))
                                    @foreach($appointment_types as $key => $appointment_type)
                                        <option value="{{ $key }}" 
                                        {{ $key == $appointment['appointment_type'] ? "selected" : " " }}>{{ $appointment_type }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Date (MM/DD/YYYY)</label>
                            <div class="input-group">
                                <input required disabled="disabled"  type="text" class="form-control required datepicker" placeholder=" (MM/DD/YYYY) " name="appointment_date" id="appointment_date"
                                value="{{ $appointment['appointment_date'] }}">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                         <div class="col-md-6 form-group">
                            <label class="control-label" for="start_time">Available Start Time (* indicates booked times) </label>
                            <select class="select2" name="start_time" style="width:100%" id="frsdom" required disabled="disabled">

                                <option value="<?php echo $appointment['start_time'] ?>" selected="selected" ><?php echo $appointment['start_time'] ?></option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="end_time">Available End Time  </label>
                            <select class="select2" name="end_time" style="width:100%" id="tjo" required disabled="disabled">
                                <option value="<?php echo $appointment['end_time'] ?>" selected="selected"><?php echo $appointment['end_time'] ?></option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label">Primary Concern(s)</label>
                            <select disabled="disabled" multiple="multiple" type="text" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="primary_concerns[]">
                                <?php 
                                    $arr = config('constants.primary_concerns');
                                    $selected_concerns   = explode(',', $appointment['primary_concerns']);
                                    foreach ($arr as $key=>$val) {
                                        $sel =  (isset($selected_concerns) && in_array($key , $selected_concerns)) ? "selected" : "";
                                        echo '<option value="'.$key.'" '.$sel.'>'.$val.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    <div class="col-md-6">
                      <label class="control-label">Appointment status </label>
                        <select type="text" class="form-control select2" name="status" id="appointment_status">
                          <option value="">Choose Option</option>
                          <?php $arr = config('constants.status');
                            foreach ($arr as $key=>$val) {
                                $selected = '';
                                 if ($key == $appointment['status'] ) {
                                    echo $key;
                                    $selected = 'selected="selected"';
                                 }
                                    echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                          } ?>
                        </select>
                    </div>
                    <?php if( $appointment['status'] == 3) { ?>
                    <div class="col-md-6" id="cancel_reason">
                      <label class="control-label">Cancel Reason</label>
                        <input type="text" class="form-control" name="reason" id="required_reason" placeholder="Enter cancel reason" value="{{ $appointment['reason'] }}">
                    </div>
                    <?php }else{ ?>
                     <div class="col-md-6 no_diplay" id="cancel_reason">
                      <label class="control-label">Cancel Reason</label>
                        <input type="text" class="form-control" name="reason" id="required_reason" placeholder="Enter cancel reason" value="{{ $appointment['reason'] }}">
                    </div>
                    <?php } ?>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')