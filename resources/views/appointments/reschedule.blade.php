@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/appointment/update_reschedule')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="appointment_id" value="<?php echo $appointment['appointment_id']; ?>">
                    <div class="form-body row">
                        <?php if (Auth::user()->role == 0 || Auth::user()->role == 7) {?>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="preferredClinics">Locations</label>
                            <select  disabled="disabled" class="select2 preferredClinics" name="preferredClinics" style="width:100%" id="preferredClinics" required>
                                <option value="">Choose A location</option>
                                <?php 
                                if(!empty($locations)){
                                    foreach ($locations as $location) {
                                        $yes = '';
                                        if($location['loc_id'] == $appointment['location_id']) $yes = 'selected="selected"'; else $yes;
                                       echo '<option  value="'.$location['loc_id'].'" '.$yes.'>'.$location['location_name'].'</option>';
                                   }
                               }
                               ?>
                           </select>
                       </div>
                     <?php } ?>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Physician</label>
                            <select disabled="disabled" class="select2" name="doctor_id" style="width:100%" id="primary_physician" required>
                                <option value="">Choose A Physician</option>
                                @if(!empty($physicians))
                                    @foreach($physicians as $physician)
                                        <option {{ $physician['id'] == $appointment['doctor_id'] ? "selected" : " " }} value="{{ $physician['id'] }}"  >{{ $physician['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="patient">Patient</label>
                            <select disabled="disabled" class="select2 appointment_patient"  name="patient_id" style="width:100%" id="patient_id" required>
                                <option value="">Choose A Patient</option>
                                @if(!empty($patients))
                                    @foreach($patients as $patient)
                                        <option value="{{ $patient['pa_id'] }}" 
                                        {{ $patient['pa_id'] == $appointment['patient_id'] ? "selected" : " " }} >{{ $patient['first_name'].' '.$patient['last_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                         <div class="col-md-6 form-group">
                            <label class="control-label" for="appointment_type">Appointment Type </label>
                            <select class="select2" name="appointment_type" style="width:100%" id="appointment_type" required>
                                <option value="">Choose A Appointment Type</option>
                                <?php $appointment_types = config('constants.appointment_types'); ?>
                                @if(!empty($appointment_types))
                                    @foreach($appointment_types as $key => $appointment_type)
                                        <option value="{{ $key }}" 
                                        {{ $key == $appointment['appointment_type'] ? "selected" : " " }}>{{ $appointment_type }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Previous Date <?php echo $appointment['appointment_date']; ?></label>
                            <div class="input-group">
                                <input required  type="text" class="form-control required datepicker" placeholder=" (YYYY/MM/DD)" name="appointment_date" id="appointment_date"
                                value="">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="start_time">Available Start Time (* indicates booked times) </label>
                            <select class="select2" name="start_time" style="width:100%" id="from" required>

                                <option value="<?php echo $appointment['start_time'] ?>" selected="selected" ><?php echo $appointment['start_time'] ?></option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="end_time">Available End Time  </label>
                            <select class="select2" name="end_time" style="width:100%" id="to" required>
                                <option value="<?php echo $appointment['end_time'] ?>" selected="selected"><?php echo $appointment['end_time'] ?></option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label">Primary Concern(s)</label>
                            <select disabled="disabled" multiple="multiple" type="text" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="primary_concerns[]">
                                <?php 
                                    $selected_concerns   = explode(',', $appointment['primary_concerns']);
                                    foreach ($primaryConcern as $key=>$val) {
                                        $sel =  (isset($selected_concerns) && in_array($key , $selected_concerns)) ? "selected" : "";
                                        echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['title'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')