@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{ url('/clinic/update/').'/'.$clinic['clinic_id'] }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="clinic_id" value="{{ $clinic['clinic_id'] }}"> 
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_name">Clinic Name</label>
                            <input type="text" class="form-control" required="required" id="clinic_name" name="clinic_name" placeholder="Clinic Name" value="{{ $clinic['clinic_name'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_phone_no">Phone Number </label>
                            <input type="text" class="form-control" name="clinic_phone_no" id="clinic_phone_no" placeholder="Phone Number" value="{{ $clinic['clinic_phone_no'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_cell_no">Cell Phone Number </label>
                            <input type="text" class="form-control" name="clinic_cell_no" id="clinic_cell_no" placeholder="Cell Phone Number" value="{{ $clinic['clinic_cell_no'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_address">Address </label>
                            <input type="text" class="form-control" name="clinic_address" id="clinic_address" placeholder="Address" value="{{ $clinic['clinic_address'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_city">City </label>
                            <input type="text" class="form-control" name="clinic_city" id="clinic_city" placeholder="City" value="{{ $clinic['clinic_city'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_state">State </label>
                            <input type="text" class="form-control" name="clinic_state" id="clinic_state" placeholder="State" value="{{ $clinic['clinic_state'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_image">Clinic Image  </label>
                            <input type="file" class="form-control upload-image" name="clinic_image" id="clinic_image" placeholder="Clinic Image">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="clinic_zipcode">Zip/Postal Code  </label>
                            <input type="text" class="form-control" name="clinic_zipcode" id="clinic_zipcode" placeholder="Zip/Postal Code" value="{{ $clinic['clinic_zipcode'] }}">
                        </div>
                        <div class="col-md-6 form-group">
                            @if(isset($clinic['image']) && !empty($clinic['image']))
                                <image src="{{ url('/clinics_imgs').'/'.$clinic['image']}}" style="width:100px;height:100px" class="view_upload_image" />
                            @else
                               <image src="{{ url('/') }}/assets/img/preview.jpg" style="width:100px;height:100px" class="view_upload_image" />
                            @endif 
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')