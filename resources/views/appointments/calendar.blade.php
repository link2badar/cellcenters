@include('header')
<br>
<div class="row">
    <div class="col-md-12">
    	<span class="pull-right">
         <a href="{{ url('patient/add')}}" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Patient</a>
         <a href="{{ url('appointment/add')}}" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Appointment</a>
     </span>
 </div>
 <div class="col-md-12" style="margin-top:12px;">
    <div class="portlet light portlet-fit bordered calendar">
        <form action="" method="get">
            <div class="portlet-title">
                <div class="caption col-md-3">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Calendar</span>
                </div>
                <?php if (@$show_cancel) {
                    echo '<input type="hidden" id="approvalCheck" value="'.@$show_cancel.'">';
                }else{
                    echo '<input type="hidden" id="approvalCheck" value="0">';
                } ?>
                
                <div class="col-md-3">
                    <?php if (Auth::user()->role == 0 || Auth::user()->role == 7) {?>
                        <label for="doctor">Locations</label>
                        <select class="select2 preferredClinics" name="location_id" style="width:100%" id="" required>
                            <option value="" disabled="disabled" selected="selected">Choose A Location</option>
                            <?php 
                            if(!empty($locations)){
                                foreach ($locations as $listOfLocations) {
                                   echo '<option value="'.$listOfLocations['loc_id'].'" >'.$listOfLocations['location_name'].'</option>';
                               }
                           }  ?>
                       </select>
                       <?php if (isset($_GET['primary_physician'])) {
                        $phy = App\Models\Locations::where('loc_id', @$_GET['location_id'])->get()->toArray();
                        echo 'selected Location: '.@$phy[0]['location_name'];
                    } 
                }?>
            </div>

            <div class="col-md-3">
                <?php if (Auth::user()->role == 0 || Auth::user()->role == 7  || Auth::user()->role == 3) {?>
                <label for="doctor">Physician</label>
                <select class="select2" name="primary_physician" style="width:100%" id="<?php if(Auth::user()->role == 3) echo 'physicianFilter'; else echo 'doctor_id';?>" required>
                    <option value="" disabled="disabled" selected="selected">Choose A Physician</option>
                    <?php 
                            if(!empty($physicians)){
                                foreach ($physicians as $physician) {
                                   echo '<option value="'.$physician['id'].'" >'.$physician['name'].'</option>';
                               }
                           }  ?>
                </select>

                <?php if (isset($_GET['primary_physician'])) {
                    $phy = App\User::where('id', @$_GET['primary_physician'])->get()->toArray();
                    echo 'selected Physician: '.@$phy[0]['name'];
                } ?>
            
            </div>
            <?php if(Auth::user()->role != 3){ ?>
            <div class="col-md-3">
                <input type="submit" name="query" class="btn btn-primary" value="Filter" style="margin-top: 25px;" />
            </div>
            <?php } }?>


        </div>
    </form>
    
    <div class="portlet-body">
        <hr>
        <div class="row">
            <div class="col-md-3">
                <?php if (isset($_GET['daily']) == 'yes') {
                    $daily = '&daily=yes';
                }else {
                    $daily =  '';
                } ?>



                <?php 
                if(isset($_GET['location_id']) && isset($_GET['primary_physician'])){?>
                    <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y').$daily;?>">Today</a>
                    <?php if($daily != ''){?>

                        <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y', strtotime('-1 days')).$daily;?>"><</a>
                        <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y', strtotime('+1 days')).$daily;?>">></a>

                    <?php }else{?>
                        <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y', strtotime('-7 days')).$daily;?>"><</a>
                        <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y', strtotime('+7 days')).$daily;?>">></a>

                    <?php }
                } else{?>
                    <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y').$daily;?>">Today</a>
                    <?php if($daily != ''){?>

                        <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y', strtotime('-1 days')).$daily;?>"><</a>
                        <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y', strtotime('+1 days')).$daily;?>">></a>

                    <?php }else{?>
                        <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y', strtotime('-7 days')).$daily;?>"><</a>
                        <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y', strtotime('+7 days')).$daily;?>">></a>

                    <?php }
                }?>                

            </div>
            <div class="col-md-4"><?php echo date("D, M d, Y"); ?> 
            <?php  if(isset($_GET['location_id']) && isset($_GET['primary_physician'])){ ?>
                <?php if ($daily != '') {?>
                    <input type="date" onchange="changeCalendarType(this.value, 'yes',<?php echo $_GET['location_id'];?>,<?php echo $_GET['primary_physician'];?>)" style="padding: 10px 20px;">
                <?php }else{?>
                    <input type="date" onchange="changeCalendarType(this.value, '', <?php echo @$_GET['location_id'];?>,<?php echo @$_GET['primary_physician'];?>)" style="padding: 10px 20px;">
                <?php } ?>
            <?php } else{?>
                <?php if ($daily != '') {?>
                    <input type="date" onchange="changeCalendarType(this.value, 'yes')" style="padding: 10px 20px;">
                <?php }else{?>
                    <input type="date" onchange="changeCalendarType(this.value)" style="padding: 10px 20px;">
                <?php } ?>
            <?php } ?>


        </div>

        <div class="col-md-5">
            <?php if(isset($_GET['location_id']) && isset($_GET['primary_physician'])){ ?>
                <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y').'&daily=yes';?>">Daily view</a>
                <a class="btn btn-primary" href="?location_id=<?php echo @$_GET['location_id'] ?>&primary_physician=<?php echo @$_GET['primary_physician'] ?>&date=<?php echo date('d-m-Y');?>">Weekly view</a>
                <a class="btn btn-primary" href="{{ url('/appointments/cancel?location_id='.@$_GET['location_id'].'&primary_physician='.@$_GET['primary_physician'])}}">show cancellation</a>
                <a class="btn btn-primary" href="{{ url('/calendar/'.@$approvalCheck.'?location_id='.@$_GET['location_id'].'&primary_physician='.@$_GET['primary_physician'].'&date='.date('d-m-Y'))}}">show Approved</a>
            <?php }else{?>
                <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y').'&daily=yes';?>">Daily view</a>
                <a class="btn btn-primary" href="?date=<?php echo date('d-m-Y');?>">Weekly view</a>
                <a class="btn btn-primary" href="{{ url('/appointments/cancel')}}">show cancellation</a>
                <a class="btn btn-primary" href="{{ url('/calendar/'.@$approvalCheck.'?date='.date('d-m-Y'))}}">show Approved</a>
            <?php } ?>
        </div>
    </div><vr /><br />
    <div class="row">
        <div class="col-md-12"><div  style="float: left"><i class="fa fa-key"></i> Key:&nbsp;&nbsp;&nbsp; </div>
        <?php 
        $calendarTypes    =   config('constants.calendarTypes');
        $calendarColor    =   config('constants.calendarColor');

        foreach ($calendarTypes as $key => $value) {

            echo '<div style="float: left"><div class="calendarColor" style="background:'.$calendarColor[$key].'"></div><b> '.$value.'&nbsp;&nbsp;&nbsp;&nbsp;</b></div>';
        }
        ?>
    </div>
</div>
<vr /><br />
<div class="row">

    <div class="col-md-12 col-sm-12">
        <div id="calendar" class="has-toolbar"> </div>
        <div class="mycal" style="width:100%;"></div>
    </div>
</div>
</div>
</div>
</div>
</div>
@include('footer')