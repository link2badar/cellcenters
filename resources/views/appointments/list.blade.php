@include('header')
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <div class="pull-right">
            <a  href="<?php echo url('/clinic/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add New {{ $module }}
            </a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>  
                                <th class="text-center"> #</th>
                                <th class="text-center"> Image </th>
                                <th class="text-center"> Clinic Name </th>
                                <th class="text-center"> Location </th>
                                <th class="text-center"> Phone Number</th>
                                <th class="text-center"> Cell Number </th>
                                <th class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($list))
                                @foreach($list as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }}
                                        </td>
                                        <td>
                                            @if($li['image'] && !empty($li['image']))
                                                <img height="80px" src="{{ url('/clinics_imgs/')}}/{{$li['image']}}" width="80px" class="image-viewer">
                                            @else
                                                <img height="80px" src="{{ url('/assets/img/')}}/no-image.jpeg" width="80px">
                                            @endif
                                        </td>
                                        <td>
                                            {!! ($li['clinic_name']) ? $li['clinic_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {{ $li['clinic_address'] }} {{ $li['clinic_city'] }} {{ $li['clinic_state'] }}
                                        </td>
                                        <td>
                                            {!! ($li['clinic_phone_no']) ? $li['clinic_phone_no'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li['clinic_cell_no']) ? $li['clinic_cell_no'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            <a class="btn btn-xs blue" href="{{ url('/clinic/update/').'/'.$li['clinic_id'] }}"><i class="fa fa-edit"></i></a> -  
                                            <a class="delete btn btn-xs red" data-url="{{ url('/clinic/delete/').'/'.$li['clinic_id'] }}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')