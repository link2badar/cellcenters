@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/physician/add')}}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row">                            
                            <div class="col-md-6 form-group {{ ($errors->has('name')? 'has-error': '' )}}">
                                <label class="control-label" for="name">Full Name</label>
                                <input type="text" class="form-control"  id="name" name="name" placeholder="Full Name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div> 
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password">Password </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="confirmed">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <label class="control-label" for="phone_number">Phone no</label>
                                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Enter Phone Number" value="{{ old('phone_number') }}">
                                @if ($errors->has('phone_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group has-error">
                                <label class="control-label">Location(s)  </label>
                                <select type="text" class="form-control select2" name="location_id[]" multiple>
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php 
                                        foreach ($allLocation as $key => $val) {
                                            echo '<option value="'.$val['loc_id'].'">'.$val['location_name'].'</option>';
                                        }
                                    ?>
                                </select>
                                @if ($errors->has('location_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('location_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')