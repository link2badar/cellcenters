@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/staff/add')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="name">Name</label>
                                <input type="text" class="form-control" required="required" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label" for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{ old('email') }}">
                                @if($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password">Password </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password_confirmation">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Position">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Position(s)  </label>
                                <select type="text" class="form-control select2" name="positions[]" multiple>
                                    <option value="">Choose One Option</option>
                                    <?php 
                                        $arr    =   config('constants.positions');
                                        foreach ($arr as $key => $val) {
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')