@include('header')
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <div class="pull-right">
            <a  href="<?php echo url('/physician/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add New {{ $module }} </a>            
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover filter_table" id="physicians_tables">
                        <thead>
                            <tr>  
                                <th class="text-center"> #</th>
                                <th class="text-center"> Full Name </th>
                                <th class="text-center"> Email </th>
                                <th class="text-center"> Phone No </th>
                                <th class="text-center"> Location</th>
                                <th class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($physicians))
                                @foreach($physicians as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }}
                                        </td>
                                        <td>
                                            {!! ($li->name) ? $li->name : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                       <td>
                                            {!! ($li->email) ? $li->email : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li->phone_number) ? $li->phone_number : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li->location_name) ? $li->location_name : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            <a class="btn btn-xs blue" href="{{ url('/physician/update/').'/'.$li->id }}"><i class="fa fa-edit"></i></a> -  
                                            <a class="delete btn btn-xs red" data-url="{{ url('/physician/delete/').'/'.$li->id }}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')