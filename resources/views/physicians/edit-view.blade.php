@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/physician/update/').'/'.$physician['id'] }}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $physician['id'] }}">
                    <div class="form-body">
                        <div class="row">                            
                            <div class="col-md-6 form-group {{ ($errors->has('name')? 'has-error': '' )}}">
                                <label class="control-label" for="name">Full Name</label>
                                <input type="text" class="form-control" value="{{$physician['name']}}" id="name" name="name" placeholder="Full Name">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div> 
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{$physician['email']}}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password">Password </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="confirmed">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Position">
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-6 form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <label class="control-label" for="phone_number">Phone no</label>
                                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Enter Phone Number" value="{{$physician['phone_number']}}">
                                @if ($errors->has('phone_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                      <label class="control-label">Location * </label>
                        <select type="text" class="form-control select2" required name="location_id[]" multiple>
                          <option value="" disabled="disabled">Choose How Did You Hear About Us</option>
                          <?php 
                          $location = explode(",", $physician['location_id']);
                          foreach ($allLocation as $lockey => $locvalue) {
                            $selected = ''; 
                                if (in_array($locvalue['loc_id'], $location)) {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option '.$selected.' value="'.$locvalue['loc_id'].'">'.$locvalue['location_name'].'</option>';
                      }
                          ?>
                          
                        </select>
                      
                    </div>
                        </div>
                    </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')