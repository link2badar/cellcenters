@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/clinic/add')}}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('clinic_name')? 'has-error': '' )}}">
                                <label class="control-label" for="clinic_name">Clinic Name</label>
                                <input type="text" class="form-control required"  id="clinic_name" name="clinic_name" placeholder="Clinic Name">
                                @if ($errors->has('clinic_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('clinic_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('clinic_phone_no')? 'has-error': '' )}}">
                                <label class="control-label" for="clinic_phone_no">Phone Number </label>
                                <input type="text" class="form-control required" name="clinic_phone_no" id="clinic_phone_no" placeholder="Phone Number">
                                @if ($errors->has('clinic_phone_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('clinic_phone_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_cell_no">Cell Phone Number </label>
                                <input type="text" class="form-control" name="clinic_cell_no" id="clinic_cell_no" placeholder="Cell Phone Number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_address">Address </label>
                                <input type="text" class="form-control" name="clinic_address" id="clinic_address" placeholder="Address">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_city">City </label>
                                <input type="text" class="form-control" name="clinic_city" id="clinic_city" placeholder="City">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_state">State </label>
                                <input type="text" class="form-control" name="clinic_state" id="clinic_state" placeholder="State">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_image">Clinic Image  </label>
                                <input type="file" class="form-control upload-image" name="clinic_image" id="clinic_image" placeholder="Clinic Image">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="clinic_zipcode">Zip/Postal Code  </label>
                                <input type="text" class="form-control" name="clinic_zipcode" id="clinic_zipcode" placeholder="Zip/Postal Code">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')