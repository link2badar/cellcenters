@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/seminar/add')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="title">Title</label>
                            <input type="text" class="form-control" required="required" id="title" name="title" placeholder="Seminar Title">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Date</label>
                            <div class="input-group">
                                <input  type="text" class="form-control required datepicker" placeholder=" (MM/DD/YYYY) " name="date" id="date">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group">
                                <label class="control-label" for="start_time"></label>
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="start_time" name="start_time">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group">
                                <label class="control-label" for="end_time"></label>
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="end_time" name="end_time">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="location_id">Connected Clinic</label>
                            <select class="select2" name="location_id" style="width:100%" id="location_id">
                                @if(!empty($clinics))
                                    @foreach($clinics as $clinic)
                                        <option value="{{ $clinic['loc_id'] }}">{{ $clinic['location_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="location_name">Location Name</label>
                            <input type="text" class="form-control" name="location_name" id="location_name" placeholder="Location Name">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="address">Address</label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="city">City</label>
                            <input type="text" class="form-control" name="city" id="city" placeholder="City">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="state">State</label>
                            <input type="text" class="form-control" name="state" id="state" placeholder="State">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="zip_code">Zip/Postal Code</label>
                            <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="Zip/Postal Code">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="country">Country</label>
                            <?php  $countries = config('constants.countries') ?>
                            <select class="select2" name="country" style="width:100%" id="country">
                                @if(!empty($countries))
                                    @foreach($countries as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="neighborhood">Neighborhood</label>
                            <input type="text" class="form-control" name="neighborhood" id="neighborhood" placeholder="Neighborhood">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="registration_open">Registration Open</label>
                            <select class="select2" name="registration_open" style="width:100%" id="registration_open">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="registrant_notification">Notify an admin and close online registration when registration reaches...</label>
                            <input type="text" class="form-control" id="registrant_notification" name="registrant_notification">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="image">Image</label>
                            <input type="file" class="form-control" id="image" name="image" placeholder="Image">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="seminar_admin_ids">Seminar Users (4 Seminar Users Max)</label>
                            <select class="select2" name="seminar_admin_ids[]" style="width:100%" multiple id="seminar_admin_ids">
                                @if(!empty($users))
                                    @foreach($users as $user)
                                        <option value="{{ $user['id']}}">{{ $user['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="description">Description</label>
                            <textarea id="mytextarea" class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')