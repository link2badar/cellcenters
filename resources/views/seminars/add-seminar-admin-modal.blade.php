<div class="modal-content">
    <div class="modal-header bg-blue bg-font-blue">
        <h5 class="modal-title" id="exampleModalLabel"><b>{{ $title }}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                ×
            </span>
        </button>
    </div> 
    <form action="{{ url('/seminar/add-admin') }}" method="post">
    <div class="modal-body">
        <input type="hidden" name="seminar" value="{{ $seminar_id }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-12">
                <label class="control-label" for="user">Seminar Users</label>
                <select class="select2" id="user" name="user" style="width: 100%">
                    @if(!empty($users))
                        @foreach($users as $user)
                            <option value="{{ $user['id']}}">{{ $user['name'] }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div><!-- modal body -->
    <div class="modal-footer">
        <button type="submit" class="btn green-soft m-btn m-btn--icon"><span><i class="fa fa-plus"></i> {{ $title }}</span></button>
        <button type="button" class="btn btn-danger m-btn m-btn--icon" data-dismiss="modal"><span>Cancel</span></button>
    </div>
    </form>
</div>