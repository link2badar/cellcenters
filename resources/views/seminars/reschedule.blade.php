<div class="modal-content">
    <div class="modal-header bg-blue bg-font-blue">
        <h5 class="modal-title" id="exampleModalLabel"><b>{{ date('d/M/Y', strtotime($currentSeminar[0]->date)) }} -  {{ $currentSeminar[0]->title }}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                ×
            </span>
        </button>
    </div> 
    <form action="{{ url('/seminar/update-schedule/'.$patientID) }}" method="post">
        <input type="hidden" name="patientID" value="{{ $patientID }}">
        <input type="hidden" name="oldSeminar" value="{{ $seminarID }}">
        
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-12">
                <label class="control-label" for="user">Seminar</label>
                <select class="select2" id="seminarID" name="seminarID" style="width: 100%">
                    @if(!empty($allSeminar))
                        @foreach($allSeminar as $value)
                        <?php 
                        $startTime = date('h:i A', $value['start_time']);
                        $endTime   = date('h:i A', $value['end_time']);
                        $date      = date('d/M/Y', strtotime($value['date']));
                        ?>
                            <option value="{{ $value['seminar_id']}}">{{ $date.' - '.$value['title'].' - '.$startTime.' - '.$endTime }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div><!-- modal body -->
    <div class="modal-footer">
        <button type="submit" class="btn green-soft m-btn m-btn--icon"><span><i class="fa fa-plus"></i> Update Seminar for Patient</span></button>
        <button type="button" class="btn btn-danger m-btn m-btn--icon" data-dismiss="modal"><span>Cancel</span></button>
    </div>
    </form>
</div>