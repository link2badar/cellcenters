@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/seminar/update' )}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="seminar_id" value="{{ $seminar['seminar_id'] }}">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="title">Title</label>
                            <input type="text" class="form-control" required="required" id="title" name="title" placeholder="Seminar Title" value="{{ $seminar['title']}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Date (MM/DD/YYYY)</label>
                            <div class="input-group">
                                <input  type="text" class="form-control required datepicker" placeholder=" (MM/DD/YYYY) " name="date" id="date" value="{{ $seminar['date'] }}">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group">
                                <label class="control-label" for="start_time"></label>
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="start_time" name="start_time" value="{{ date('h:i A',$seminar['start_time']) }}" data-date-format="HH:mm">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group">
                                <label class="control-label" for="end_time"></label>
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="end_time" name="end_time" value="{{ date('h:i A',$seminar['end_time']) }}">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="location_id">Connected Clinic</label>
                            <select class="select2" name="location_id" style="width:100%" id="location_id">
                                <?php
                                    if(!empty($clinics)):
                                        foreach($clinics as $clinic):
                                            $sel = ($seminar['location_id'] == $clinic['loc_id']) ? 'selected="selected"' : '';
                                            echo '<option '.$sel.' value="'.$clinic['loc_id'].'">'.$clinic['location_name'].'</option>';
                                        endforeach;
                                    endif;
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="location_name">Location Name</label>
                            <input type="text" class="form-control" name="location_name" id="location_name" placeholder="Location Name" value="{{ $seminar['location_name'] }}">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="address">Address</label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{ $seminar['address'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="city">City</label>
                            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ $seminar['city'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="state">State</label>
                            <input type="text" class="form-control" name="state" id="state" placeholder="State" value="{{ $seminar['state'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="zip_code">Zip/Postal Code</label>
                            <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="Zip/Postal Code" value="{{ $seminar['zip_code'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="country">Country</label>
                            <select class="select2" name="country" style="width:100%" id="country">
                                <?php  
                                    $countries = config('constants.countries');
                                    if(!empty($countries)):
                                        foreach($countries as $key => $value):
                                            $sel = ($seminar['country'] == $key) ? 'selected="selected"' : '';
                                            echo '<option '.$sel.' value="'.$key.'">'.$value.'</option>';
                                        endforeach;
                                    endif;
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="neighborhood">Neighborhood</label>
                            <input type="text" class="form-control" name="neighborhood" id="neighborhood" placeholder="Neighborhood" value="{{ $seminar['neighborhood'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="registration_open">Registration Open</label>
                            <select class="select2" name="registration_open" style="width:100%" id="registration_open">
                                <option value="1" <?php if($seminar['registration_open'] == 1) echo 'selected="selected"'; ?>>Yes</option>
                                <option value="0" <?php if($seminar['registration_open'] == 0) echo 'selected="selected"'; ?>>No</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="registrant_notification">Notify an admin and close online registration when registration reaches...</label>
                            <input type="text" class="form-control" id="registrant_notification" name="registrant_notification"  value="{{ $seminar['registrant_notification'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="image">Image</label>
                            <input type="file" class="form-control" id="image" name="image" placeholder="Image">
                            <img src="{{ url('seminars_imgs/'.$seminar['image'])}}" alt="No image found" height="100">
                        </div>
                            <div class="col-md-12 form-group">
                                <label class="control-label">Seminar User(s)  </label>
                                <select type="text" class="form-control select2" name="seminar_admin_ids[]" multiple>
                                    <option value="" disabled="disabled">Choose Seminar User</option>
                                    <?php 
                                    foreach ($usersIn as $key => $val) {
                                        echo '<option selected="selected" value="'.$val['id'].'">'.$val['name'].'</option>';
                                    }

                                    foreach ($usersOut as $key => $val) {
                                        echo '<option  value="'.$key['id'].'">'.$val['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <div class="form-group col-md-12">
                            <label class="control-label" for="description">Description</label>
                            <textarea id="mytextarea" class="form-control" name="description">{{ $seminar['description'] }}</textarea>
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')