@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/staff/update/').'/'.$staff['id'] }}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $staff['id'] }}">
                    <div class="form-body">
                        <div class="row">                            
                            <div class="col-md-6 form-group {{ ($errors->has('name')? 'has-error': '' )}}">
                                <label class="control-label" for="name">Full Name</label>
                                <input type="text" class="form-control" value="{{$staff['name']}}" id="name" name="name" placeholder="Full Name">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div> 
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{$staff['email']}}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password">Password </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="confirmed">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Position(s)  </label>
                                <select type="text" class="form-control select2" name="positions[]" multiple required>
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php 
                                    $arr    =   config('constants.positions');
                                    $positions = explode(",", $staff['positions']);
                                    foreach ($arr as $key => $val) {
                                        $selected = ''; 
                                        if (in_array($key, $positions)) {
                                            $selected = 'selected="selected"';
                                        }
                                        echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Location(s)  </label>
                                <select type="text" class="form-control select2" name="location_id[]" multiple required>
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php 
                                        $arr    =   $locations;
                                        $location_id = explode(",", $staff['location_id']);
                                        foreach ($arr as $key => $val) {
                                            $selected = ''; 
                                            if (in_array($val['loc_id'], $location_id)) {
                                            $selected = 'selected="selected"';
                                        }
                                            echo '<option value="'.$val['loc_id'].'" '.$selected.'>'.$val['location_name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@include('footer')