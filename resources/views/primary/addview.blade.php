@include('header')
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
      </div>
      <div class="portlet-body form">
        <form role="form" action="{{url('/primary-concern/add')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-body row">
            
            <div class="col-md-12">
              <label class="control-label" for="title">Title* :</label>
              <input type="text" class="form-control" required="required" id="title" name="title" placeholder="Enter Primary Concern">
            </div>
          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  @include('footer')
</div>