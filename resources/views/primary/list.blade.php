@include('header')

<div class="row">
  <div class="col-md-2 pull-right">
    <br/>
    <a  href="<?php echo url('/primary-concern/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add Primary Concern</a>
    <br/>
  </div>
</div>
<div class="row">

  <div class="col-md-12">

    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
        </div>
        <div class="portlet-body">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead class="list">
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Action</th>
                </tr>
              </thead>
              <?php 
              $i  = 1;
              $html = "";
              if(count($list) > 0){
                foreach($list as $key=>$row)  { ?>
                 
                      <tbody>
                        <tr class="row-<?php echo $row['id'].' list_'.$i.' list'; ?>">
                          <td>{{ $i }}</td>
                          <td> {{ $row->title}}</td><?php
                           $html = '<td><a class="btn btn-xs blue" href="'.url('/primary-concern/update/'.$row['id']).'"><i class="fa fa-edit"></i></a>
                          <a class="delete btn btn-xs red" data-url="'.url('/primary-concern/delete/').'/'.$row['id'].'" href="javascript:void(0);" data-remove="list_'.$i.'"><i class="fa fa-trash"></i></a></td>';
                       echo $html; $i++;?>
                        <?php }
                      }
                    else {
                    echo '<tr><td colspan="2" align="center"><h4>You have not any Stores. to add <a href="'.url('/').'/'.$module.'/add">click here</a></h4></td><tr>'; 
                    }
                    ?>




                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
        @include('footer')
      </div>