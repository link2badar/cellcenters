@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/speaker/update/').'/'.$speaker['id'] }}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $speaker['id'] }}">
                    <div class="form-body">
                        <div class="row">                            
                            <div class="col-md-6 form-group {{ ($errors->has('name')? 'has-error': '' )}}">
                                <label class="control-label" for="name">Full Name</label>
                                <input type="text" class="form-control" value="{{$speaker['name']}}" id="name" name="name" placeholder="Full Name">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div> 
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{$speaker['email']}}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="password">Password </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label" for="confirmed">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Type  </label>
                                <select type="text" class="form-control select2" name="role" required>                                    
                                    <option value="7" <?php if ($speaker['role'] == 7) { echo 'selected="selected"'; } ?>>Admin Employee</option>
                                    <option value="6" <?php if ($speaker['role'] == 6) { echo 'selected="selected"'; } ?>>Speaker</option>
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="form-actions">
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update {{ $module}}</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@include('footer')