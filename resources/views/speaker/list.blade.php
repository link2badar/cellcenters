@include('header')
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <div class="pull-right">
            <a  href="<?php echo url('/speaker/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add {{ $module }} 
            </a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover filter_table" id="staff_tables">
                        <thead>
                            <tr>  
                                <th class="text-center"> #</th>
                                <th class="text-center"> Name </th>
                                <th class="text-center"> Email </th>
                                <th class="text-center"> Type </th>
                                <th class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(!empty($speaker))
                                @foreach($speaker as $key => $li)
                                <tr class="text-center list_{{++$key}} list">
                                    <td>
                                        {{ $key }}
                                    </td>
                                    <td>
                                        {!! ($li->name) ? $li->name : '<span class="badge badge-danger"> N/A </span>' !!}
                                    </td>
                                    <td>
                                        {!! ($li->email) ? $li->email : '<span class="badge badge-danger"> N/A </span>' !!}
                                    </td>
                                    <td>
                                        <?php if ($li->role == 6) {
                                            echo 'Speaker';
                                        }else{
                                            echo 'Admin Employee';
                                        } ?>
                                    </td>
                                    
                                    <td>
                                        <a class="btn btn-xs blue" href="{{ url('/speaker/update/').'/'.$li->id }}"><i class="fa fa-edit"></i></a> -  
                                        <a class="delete btn btn-xs red" data-url="{{ url('/speaker/delete/').'/'.$li->id }}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-trash"></i> 
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                             
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')