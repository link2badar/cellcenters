<div class="modal-content">
    <div class="modal-header bg-blue bg-font-blue">
        <h5 class="modal-title" id="exampleModalLabel">
            <b>Add Seminar Note</b>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                ×
            </span>
        </button>
    </div>
    <form action="{{ url('/seminar/create-note') }}" method="post" class="make_ajax">
        {{ csrf_field() }}
        <input type="hidden" name="note_seminar_id" value="{{ $seminar_id }}">
        <input type="hidden" name="note_pa_id"      value="{{ $patient_id }}">
        <input type="hidden" name="note_user_id"    value="{{ $user_id}}">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="name" class="control-label">Note:</label>
                    <div class="input-icon right">
                        <i class="fa"></i>
                         <textarea class="form-control ckeditor" name="note_detail"></textarea>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn green-soft m-btn m-btn--icon"><span><i class="fa fa-plus"></i><span> Add </span></span></button>
            <button type="button" class="btn btn-danger m-btn m-btn--icon" data-dismiss="modal"><span>Close</span></button>                
        </div>
    </form>
</div>