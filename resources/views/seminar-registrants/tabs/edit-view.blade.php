@extends('seminar-registrants.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active" id="tab_1">
    <form role="form" action="{{url('/seminar/update-registrant')}}" method="post">
        <input type="hidden" name="_token"  value="{{ csrf_token() }}">
        <input type="hidden" name="seminar" value="{{ $registrant['seminar'] }}">
        <input type="hidden" name="pa_id"   value="{{ $registrant['pa_id'] }}">
        <div class="form-body row">
            <div class="col-md-6 form-group">
                <label class="control-label" for="first_name">First Name</label>
                <input type="text" class="form-control" required="required" id="first_name" name="first_name" placeholder="First Name" value="{{ $registrant['first_name'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label" for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ $registrant['last_name'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label" for="email">Email Address </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{{ $registrant['email'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label">How Did You Hear About Us?* </label>
                <select type="text" class="form-control select2" name="haboutus">
                    <option value="">Choose How Did You Hear About Us</option>
                    <?php 
                        $arr = config('constants.haboutus');
                        foreach ($arr as  $key  =>  $val) {
                            $sel = ($key == $registrant['haboutus'])? 'selected="selected"' : '';
                            echo '<option '.$sel.' value="'.$key.'">'.$val.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label" for="phone_number">Phone Number </label>
                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" value="{{ $registrant['phone_number'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label" for="cell_phone_number">Cell Phone Number </label>
                <input type="text" class="form-control" name="cell_phone_number" id="cell_phone_number" placeholder="Cell Phone Number" value="{{ $registrant['cell_phone_number'] }}">
            </div>
            <div class="col-md-12 form-group">
                <label class="control-label" for="address">Address </label>
                <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{ $registrant['address'] }}">
            </div>
            <div class="col-md-4 form-group">
                <label class="control-label" for="city">City </label>
                <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ $registrant['city'] }}">
            </div>
            <div class="col-md-4 form-group">
                <label class="control-label" for="state">State </label>
                <input type="text" class="form-control" name="state" id="state" placeholder="State" value="{{ $registrant['state'] }}">
            </div>
            <div class="col-md-4 form-group">
                <label class="control-label" for="zipcode">Zip/Postal Code  </label>
                <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip/Postal Code" value="{{ $registrant['zipcode'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label">Date of Birth (MM/DD/YYYY)  </label>
                <input type="text" class="form-control" name="date_birth" placeholder="(MM/DD/YYYY)" value="{{ $registrant['date_birth'] }}">
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label" >Gender</label>
                <select type="text" class="form-control select2" name="gender" style="width:100% ">
                    <option value="">Choose One Option</option>
                    <?php
                        $arr = config('constants.gender');
                        foreach ($arr as $key => $val) {
                            $sel = ($key == $registrant['gender'])? 'selected="selected"' : '';
                            echo '<option '.$sel.' value="'.$key.'">'.$val.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label">Preferred Clinic  </label>
                <select type="text" class="form-control select2" name="preferred_clinic" style="width:100% ">
                    <option value="">Choose One Option</option>
                    <?php
                        if(!empty($clinics)):
                            foreach ($clinics as $key=>$val) {
                                $sel = ($val['clinic_id'] == $registrant['preferred_clinic'])? 'selected="selected"' : '';
                                echo '<option '.$sel.' value="'.$val['clinic_id'].'">'.$val['clinic_name'].'</option>';
                            }
                        endif;
                    ?>
                </select>
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label"> Case Manager   </label>
                <select type="text" class="form-control select2" name="case_manager" style="width:100%">
                    <option value="">Choose One Option</option>
                    <?php
                        if(!empty($managers)):
                            foreach ($managers as $key=>$val) {
                                $sel = ($val['manager_id'] == $registrant['case_manager'])? 'selected="selected"' : '';
                                echo '<option '.$sel.' value="'.$val['manager_id'].'">'.$val['first_name']." ".$val['last_name'].'</option>';
                            }
                        endif;
                    ?>
                </select>
            </div>
            <div class="col-md-6 form-group">
                <label class="control-label">Primary Physician   </label>
                <select type="text" class="form-control select2" name="primary_physician" style="width:100%">
                    <option value="">Choose One Option</option>
                    <?php
                        if(!empty($physicians)):
                            foreach ($physicians as $key=>$val) {
                                $sel = ($val['physician_id'] == $registrant['primary_physician'])? 'selected="selected"' : '';
                                echo '<option '.$sel.' value="'.$val['physician_id'].'">'.$val['first_name']." ".$val['last_name'].'</option>';
                            }
                        endif;
                    ?>
                </select>
            </div>
            <div class="col-md-12 form-group">
                <label class="control-label">Primary Concern(s)</label>
                <select type="text" class="form-control select2" name="primary_concern" style="width:100%">
                    <?php 
                        $arr = config('constants.primary_concerns');
                        foreach ($arr as $key=>$val) {
                            $sel = ($key == $registrant['primary_concern'])? 'selected="selected"' : '';
                            echo '<option '.$sel.' value="'.$key.'">'.$val.'</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Save Changes</button>
        </div>
    </form>
</div>
@endsection