@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/seminar/add-registrant')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="seminar" value="{{ $seminar_id }}">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="first_name">First Name</label>
                            <input type="text" class="form-control" required="required" id="first_name" name="first_name" placeholder="First Name">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="last_name">Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="email">Email Address </label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label">How Did You Hear About Us?* </label>
                            <select type="text" class="form-control select2" name="haboutus">
                                <option value="">Choose How Did You Hear About Us</option>
                                <?php 
                                    $arr = config('constants.haboutus');
                                    foreach ($arr as  $key  =>  $val) {
                                        echo '<option value="'.$key.'">'.$val.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="phone_number">Phone Number </label>
                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number">
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="cell_phone_number">Cell Phone Number </label>
                            <input type="text" class="form-control" name="cell_phone_number" id="cell_phone_number" placeholder="Cell Phone Number">
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="address">Address </label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                        </div>
                        <div class="col-md-4 form-group">
                            <label class="control-label" for="city">City </label>
                            <input type="text" class="form-control" name="city" id="city" placeholder="City">
                        </div>
                        <div class="col-md-4 form-group">
                            <label class="control-label" for="state">State </label>
                            <input type="text" class="form-control" name="state" id="state" placeholder="State">
                        </div>
                        <div class="col-md-4 form-group">
                            <label class="control-label" for="zipcode">Zip/Postal Code  </label>
                            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip/Postal Code">
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label">Primary Concern(s)</label>
                            <select type="text" class="form-control select2" name="primary_concern">
                                <?php 
                                    $arr = config('constants.primary_concerns');
                                    foreach ($arr as $key=>$val) {
                                        echo '<option value="'.$key.'">'.$val.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <div class="mt-repeater">
                                <div data-repeater-list="guest">
                                    <div data-repeater-item class="row">
                                        <div class="col-md-5">
                                            <label class="control-label">Guest First Name</label>
                                            <input type="text" placeholder="Guest First Name" class="form-control" name="first_name" /> </div>
                                        <div class="col-md-5">
                                            <label class="control-label">Guest Last Name</label>
                                            <input type="text" placeholder="Guest Last Name" class="form-control" name="last_name" /> </div>
                                        <div class="col-md-2">
                                            <label class="control-label">&nbsp;</label><br>
                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                <i class="fa fa-close"> Delete</i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                    <i class="fa fa-plus"></i> Add Variation</a>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')