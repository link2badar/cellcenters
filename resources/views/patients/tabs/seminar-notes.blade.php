@extends('patients.tabs.pills-header')
@section('pill-content')
<!-- <div class="tab-pane"> -->
    <hr class="border-yellow-gold">
	<div class="row">
		<div class="col-md-4"><b>Name: <?php echo $patient['first_name'].' '.$patient['last_name']; ?></b></div>
        <div class="col-md-4"><b>Phone:  <?php echo $patient['phone_number'] ?></b></div>
        <div class="col-md-4"><b>Email:  <?php echo $patient['email'] ?></b></div>
		<div class="col-md-12">
			<div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>Seminar Notes: </div>
                    <div class="actions">
                        <a href="#data_modal" data-toggle="modal" class="btn btn-success" onclick="loadModal('/seminar/create-note',{{ $registrant_id }})"><i class="fa fa-plus"></i> Add Seminar Note</a>
                    </div>
                </div>
                <div class="portlet-body">
                    @if(!empty($notes))
                        @foreach($notes as $key => $note)
                            <dl>
                                <dt class="bold">
                                    <span>{{ $note['name'] }}</span>
                                    <span class="font-blue-oleo">
                                        <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($note['created_at']))->toDayDateTimeString(); ?>
                                    </span>
                                </dt> 
                                <dd>
                                    <?php echo substr($note['note_detail'],0,'80') ;?> <a href="#data_modal" data-toggle="modal" onclick="loadModal('/seminar/full-note',{{ $note['note_id'] }} )" class="font-yellow-gold"> Read Full Note</a>
                                </dd>   
                            </dl>
                            <hr>
                        @endforeach
                    @else
                	   <p>There are no seminar notes for this patient.</p>
                    @endif
                </div>
            </div>
		</div>
	</div>
<!-- </div> -->
@endsection