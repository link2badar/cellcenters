@extends('patients.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active">
    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-pills">
            <li class="{{  ($current_pill == 'Completed Forms')? 'active' : '' }}">
                <a href="{{ url('patient/log/'.$registrant_id) }}">Completed Forms</a>
            </li>
            <li class="{{  ($current_pill == 'Seminar Notes')? 'active' : '' }}">
                <a href="{{ url('patient/seminar-notes/'.$registrant_id) }}" data-toggle="" aria-expanded="false">Seminar Notes</a>
            </li>
            <li class="{{  ($current_pill == 'Consultation Notes')? 'active' : '' }}"> 
                <a href="{{ url('patient/consultation-notes/'.$registrant_id) }}" data-toggle="" aria-expanded="false">Consultation Notes</a>
            </li>
            <li class="{{  ($current_pill == 'Exam Notes')? 'active' : '' }}">
                <a href="{{ url('patient/exam-notes/'.$registrant_id) }}" data-toggle="" aria-expanded="false">Exam Notes</a>
            </li>  
            <li class="{{  ($current_pill == 'Treatment Notes')? 'active' : '' }}">
                <a href="{{ url('patient/treatment-notes/'.$registrant_id) }}" data-toggle="" aria-expanded="false">Treatment Notes</a>
            </li>                                    
        </ul>
        <div class="tab-content">
            @yield('pill-content')
        </div>
    </div>
</div>
@endsection