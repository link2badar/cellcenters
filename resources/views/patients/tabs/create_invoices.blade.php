@extends('patients.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active" id="tab_1">
   </br>
   <div class="row">
      <div class="col-md-4">Name: <strong><?php echo $patient['first_name']." ".$patient['last_name'] ?></strong></div>
      <div class="col-md-4">Email: <strong><?php echo $patient['email'] ?></strong></div>
      <div class="col-md-4">Phone: <strong><?php echo $patient['cell_phone_number'] ?></strong></div>
   </div>
   <div class="portlet-body form">
      <form role="form" action="{{url('/account') }}" method="post" enctype="multipart/form-data" class="validate_form">
         <div class="form-body">
            <div class="row">
               <div class="form-group col-md-6">
                  <label class="control-label" for="invoice_date">Date (MM/DD/YYYY)</label>
                  <div class="input-group">
                     <input required  type="text" class="form-control required datepicker" value="<?php echo date("Y/m/d") ?>" placeholder=" (YYYY/MM/DD) " name="invoice_date" id="invoice_date">
                     <span class="input-group-addon bg-blue bg-font-blue">
                     <i class="fa fa-calendar-plus-o"></i>
                     </span>
                  </div>
               </div>
                  <div class="form-group col-md-6">
                     <label class="control-label" for="date">Transaction Type</label>
                     <div class="input-group">
                        <select name="type[1]" id="type_1" style="width:100%" class="select2 selectTransaction">
                           <option selected="selected" disabled="disabled" value="">Choose A Transaction Type</option>
                           <option value="Seminar">Seminar</option>
                           <option value="Consultation">Consultation</option>
                           <option value="Exam">Exam</option>
                           <option value="Treatment">Treatment</option>
                           <option value="Payment">Payment</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row no_diplay" id="notpaymenttype">
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Primary Concern(s)</label>
                     <select style="width:100%;" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="pain_area[1]" id="pain_area_1">
                     <?php 
                        $arr = config('constants.primary_concerns');
                        
                        foreach ($arr as $key=>$val) {
                            echo '<option value="'.$key.'">'.$val.'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Product Used</label>
                     <input type="text" id="product_used_1" class="form-control required" name="product_used[1]" >
                  </div>
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Amount of Product Used</label>
                     <input type="text" id="quantity_1" class="form-control required" name="quantity[1]">
                  </div>
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Doctor</label>
                     <select style="width:100%;" data-placeholder="Choose Condition(s)" class="form-control select2 concerns" name="provider_id[1]" id="provider_id_1">
                     <?php 
                        $arr = config('constants.primary_concerns');
                        
                        foreach ($arr as $key=>$val) {
                            echo '<option value="'.$key.'">'.$val.'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Price</label>
                     <input type="text" class="form-control required" name="amount[1]" id="amount_1">
                  </div>
               </div>
               <div class="row no_diplay" id="paymenttype">
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Amount of Payment</label>
                     <input type="text" id="amount_1" class="form-control required" name="amount[1]">
                  </div>
                  <div class="col-md-6 form-group ">
                     <label class="control-label">Payment Type</label>
                     <select style="width:100%;" name="payment_type[1]" class="form-control select2 concerns" id="payment_type_1">
                        <option selected="selected" disabled="disabled" value="">Choose A Payment Type</option>
                        <option value="cash">Cash</option>
                        <option value="credit">Credit Card</option>
                        <option value="check">Check</option>
                        <option value="financing">Financing</option>
                     </select>
                  </div>
                  <div id="creditcard" class="no_diplay">
                     <div class="col-md-6 form-group">
                        <label class="control-label">Credit Card Authorization Number*</label>
                        <input type="text" id="cc_auth_number_1" class="form-control required" name="cc_auth_number[1]">
                     </div>
                  </div>
                  <div id="checknumber" class="no_diplay">
                     <div class="col-md-6 form-group">
                        <label class="control-label">Check Number*</label>
                        <input type="text" id="check_number_1" class="form-control required" name="check_number[1]">
                     </div>
                  </div>
                  <div id="financing" class="no_diplay">
                     <div class="col-md-6 form-group">
                        <label class="control-label">Financing Amount</label>
                        <input type="text" id="finance_amount_1" class="form-control required" name="finance_amount[1]">
                     </div>
                     <div class="col-md-6 form-group">
                        <label class="control-label">Financing Fees</label>
                        <input type="text" id="finance_fees_1" class="form-control required" name="finance_fees[1]">
                     </div>
                     <div class="col-md-12 form-group">
                        <label class="control-label">Financing Terms</label>
                        <textarea id="finance_note_1" name="finance_note[1]"  class="form-control"></textarea>
                     </div>
                  </div>
               </div>
         </div>
         <div class="form-actions">
            <span class="pull-left">
            <button type="button" id="transactioncounter" class="btn btn green" data-count="1">Add Another Transaction</button>
            </span>
            <span class="pull-right">
            <button type="submit" class="btn btn-primary">Save Changes</button>
            </span>
         </div>
      </form>
   </div>
</div>
@endsection