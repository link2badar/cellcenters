@include('header')
<br>
<?php if(isset($patient['pa_id'])){ ?>
<div class="row">
     <div class="col-md-2 pull-right">
        <a  href="<?php echo url('patient/invoice/create/'.$patient['pa_id']); ?>" class="btn btn-block btn blue-madison"><i class="fa fa-plus"></i> Invoice</a>
        <br/>
    </div>
    <div class="col-md-2 pull-right">
        <a  href="<?php echo url('/appointment/add/'.$patient['pa_id'] ); ?>" class="btn btn-block btn-info"><i class="fa fa-plus"></i> Appointment</a>
        <br/>
    </div>
    <?php if(Auth::user()->role == 0){ ?>
     <div class="col-md-2 pull-right">
        <a  href="<?php echo url('/contact/add/'.$patient['pa_id'] ); ?>" class="btn btn-block btn grey-cascade"><i class="fa fa-plus"></i> Contact Request</a>
        <br/>
    </div>
<?php } ?>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Patient Managment </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="{{  ($current_tab == 'patient_info')? 'active' : '' }}">
                            <a href="{{ url('patient/update/'.$registrant_id) }}" data-toggle="" aria-expanded="false"> Patient Info </a>
                        </li>
                        <li class="{{  ($current_tab == 'log')? 'active' : '' }}">
                            <a href="{{ url('/patient/log/'.$registrant_id)}}" data-toggle="" aria-expanded="false"> Log </a>
                        </li>
                        <li class="{{  ($current_tab == 'appointments')? 'active' : '' }}">
                            <a href="{{ url('/patient/appointments/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Appointments </a>
                        </li>
                        <li class="{{  ($current_tab == 'invoices')? 'active' : '' }}">
                            <a href="{{ url('/patient/invoices/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Invoices </a>
                        </li>
                        <li class="{{  ($current_tab == 'seminars')? 'active' : '' }}">
                            <a href="{{ url('/patient/seminars/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Seminars </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        @yield('tab-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')