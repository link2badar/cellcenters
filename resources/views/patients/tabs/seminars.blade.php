@extends('patients.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active" id="tab_1">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet bg-blue-oleo box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>Patient Seminar List: </div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                	<div class="table-responsive">                		
	                	<table class="table table-bordered">
	                		<thead>
	                			<th class="text-center">Seminar</th>
	                			<th class="text-center">Attended</th>
	                			<th class="text-center">Location</th>
	                			<th class="text-center">Date</th>
	                		</thead>
	                		<tbody>
	                		@if(!empty($seminars))
	                			@foreach($seminars as $li)
	                				<tr class="text-center">
	                					<td>
	                						<a href="{{ url('/seminar/detail/'.$li['seminar_id']) }}" target="_blank">{!! ($li['title']) ? $li['title'] : '<span class="badge badge-danger"> N/A </span>' !!}</a>
	                					</td>
	                					<td>
	                						{!! ($li['attendance'] == 1) ? 'Present' : 'Absent' !!}
	                					</td>
	                					<td>
	                						{!! ($li['location_name']) ? $li['location_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
	                					</td>
	                					<td>
	                						{!! ($li['date']) ? $li['date'] : '<span class="badge badge-danger"> N/A </span>' !!}
	                					</td>
	                				</tr>
	                			@endforeach
	                		@endif	
	                		</tbody>	
	                	</table>
	                </div>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection