@extends('patients.tabs.pills-header')
@section('pill-content')
<!-- <div class="tab-pane"> -->
	<div class="row">
		<div class="col-md-4"><b>Name: <?php echo $patient['first_name'].' '.$patient['last_name']; ?></b></div>
        <div class="col-md-4"><b>Phone:  <?php echo $patient['phone_number'] ?></b></div>
        <div class="col-md-4"><b>Email:  <?php echo $patient['email'] ?></b></div>
		<div class="col-md-12 box">
			<div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>Consultation Notes: </div>
                    <div class="actions"></div>
                </div>
            </div>
		</div>
	</div>
    <?php $i    =  0; 
        if(isset($appointments)){ 
            foreach ($appointments as $key => $appointment) {
                if(!empty($appointments[$key]->consultation_notes)){
                    $i++;
                if( $i % 2 == 0) { echo "<div class=\"row note note-info\">"; }else{ echo "<div class=\"row note note-danger\">"; } ?>
                <div class="col-md-3">
                    <p class="bold"><?php echo date("m/d/Y", strtotime($appointments[$key]->appointment_date)); ?></p>
                </div>
                 <div class="col-md-3">
                    <p class="bold"><?php echo $appointments[$key]->start_time." - ".$appointments[0]->end_time ?></p>
                </div>
                <div class="col-md-3">
                    <p class="bold">Type: <?php
                                    $appointment_types = config('constants.appointment_types');
                                    foreach($appointment_types as $keyval => $appointment_type){
                                        if( $keyval == $appointments[$key]->appointment_type){
                                            echo $appointment_type;
                                        }
                                    } ?></p>
                </div>
                <div class="col-md-3">
                    <p class="bold">Physician:  <?php echo $appointments[$key]->name ?></p>
                </div>
                <br>
                <br>
                <div class="col-md-12">
                    <div class="portlet-body">
                    <h4 class="bold">Note:</h4>
                    <p><?php echo $appointments[$key]->consultation_notes ?></p>
                    </div>
                </div>
            </div>
    <?php } } } if( $i == 0){?>
    <div class="row">
        <div class="col-md-12">
            <p class="bold">There are no consultation notes for this patient.</p>
            </div>
        </div>

    <?php } ?>
<!-- </div> -->
@endsection