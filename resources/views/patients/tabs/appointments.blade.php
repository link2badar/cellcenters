@extends('patients.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active" id="tab_1">
	<div class="row">
		<div class="col-md-12">
			<h3 class="bold">Upcoming Scheduled Appointments</h3>
		</div>
	</div>
<?php $i    =  0; 
		if(isset($appointments)){ 
			foreach ($appointments as $key => $appointment) {
			if( date("m/d/Y", strtotime($appointments[$key]->appointment_date)) >= date("m/d/Y")  ){

					$i++;
				if( $key % 2 == 0) { echo "<div class=\"row note note-info\">"; }else{ echo "<div class=\"row note note-danger\">"; } ?>
				<div class="col-md-3">
					<p class="bold"><?php echo date("m/d/Y", strtotime($appointments[$key]->appointment_date)); ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Type: <?php
									$appointment_types = config('constants.appointment_types');
									foreach($appointment_types as $keyval => $appointment_type){
										if( $keyval == $appointments[$key]->appointment_type){
											echo $appointment_type;
										}
									} ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Physician:  <?php echo $appointments[$key]->name ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Created: <?php echo date("m/d/Y", strtotime($appointments[$key]->appcreated_at)); ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold"><?php echo $appointments[$key]->start_time." - ".$appointments[$key]->end_time ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Status: <?php if($appointments[$key]->status == 1) {
						echo "Active"; }
						if($appointments[$key]->status == 2 ) {
						echo "Confirm"; }
						if($appointments[$key]->status == 3 ) {
						echo "Cancel"; } ?></p>
				</div>
				<?php if( $appointments[$key]->status == 3 ){ ?>
				<div class="col-md-3">
					<p class="bold">Cancel Reason : <?php echo $appointments[$key]->reason ?></p>
				</div>
				<?php } ?>
				<div class="col-md-3">
					<?php foreach ($userlist as $userlistkey => $userlistvalue) {
						if ($userlistvalue['id'] == $appointments[$key]->appcreated_by) {
							echo '<p class="bold">By : '.$userlistvalue['name'].'</p>';
							break;
						}
					}
						?>
					
				</div>
			</div>
	<?php } } } if( $i == 0){?>
	<div class="row">
		<div class="col-md-12">
			<p class="bold">No Upcoiming appointment</p>
			</div>
		</div>

	<?php } ?>

	<div class="row">
		<div class="col-md-12">
			<h3 class="bold">Past Scheduled Appointments</h3>
			</div>
		</div>
		<?php $i = 0;
		 if(isset($appointments)){ 
			$i    =  0;
			foreach ($appointments as $key => $appointment) {
			if( date("m/d/Y", strtotime($appointments[$key]->appointment_date)) < date("m/d/Y")  ){	
				$i++;
			 if( $key % 2 == 0) { echo "<div class=\"row note note-info\">"; }else{ echo "<div class=\"row note note-danger\">"; } ?>
				<div class="col-md-3">
					<p class="bold"><?php echo date("m/d/Y", strtotime($appointments[$key]->appointment_date)); ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Type: <?php
									$appointment_types = config('constants.appointment_types');
									foreach($appointment_types as $keyval => $appointment_type){
										if( $keyval == $appointments[$key]->appointment_type){
											echo $appointment_type;
										}
									} ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Physician:  <?php echo $appointments[$key]->name ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Created: <?php echo date("m/d/Y", strtotime($appointments[$key]->appcreated_at)); ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold"><?php echo $appointments[$key]->start_time." - ".$appointments[$key]->end_time ?></p>
				</div>
				<div class="col-md-3">
					<p class="bold">Status: <?php if($appointments[$key]->status == 1) {
						echo "Active"; }
						if($appointments[$key]->status == 2 ) {
						echo "Confirm"; }
						if($appointments[$key]->status == 3 ) {
						echo "Cancel"; } ?></p>
				</div>
				<?php if( $appointments[$key]->status == 3 ){ ?>
				<div class="col-md-3">
					<p class="bold">Cancel Reason : <?php echo $appointments[$key]->reason ?></p>
				</div>
				<?php } ?>
				<div class="col-md-3">
					<?php foreach ($userlist as $userlistkey => $userlistvalue) {
						if ($userlistvalue['id'] == $appointments[$key]->appcreated_by) {
							echo '<p class="bold"> By : '.$userlistvalue['name'].'</p>';
							break;
						}
					}
						?>
				</div>
			</div>
	<?php } } } if( $i == 0){?>
	<div class="row">
		<div class="col-md-12">
			<p class="bold">No Upcoiming appointment</p>
			</div>
		</div>

	<?php } ?>
	</div>
</div>
@endsection