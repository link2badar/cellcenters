@include('header')
<br>
<div class="row">
    <div class="col-md-2 pull-right">
        <a  href="<?php echo url('/patient/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-plus"></i> Add Patient</a>
        <br/>
    </div>
</div>
<div class="row">
        <div class="col-md-12" style="margin-top:8px">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>Search {{ $module}} </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('patients')}}" method="get">
                    <div class="form-body">
                        <section class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">First Name</label>
                                <input type="text" name="first_name" class="form-control" value="{{ @$_GET['first_name'] }}" placeholder="First Name">
                                
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Last Name</label>
                                <input type="text" name="Last_name" class="form-control" value="{{ @$_GET['last_name'] }}" placeholder="Last Name">
                                
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Clinics</label>
                                <select class="select2 form-control" name="preferred_clinic">
                                    <option value="">Select Clinic</option>
                                    @if(!empty($clinics))
                                        @foreach($clinics as $key => $row)
                                        <?php if($row['loc_id'] == @$_GET['preferred_clinic']) $selected = 'selected="selected"'; else $selected = ''; ?>
                                            <option value="{{$row['loc_id']}}" <?php echo $selected ?>>{{ $row['location_name'] }}</option>
                                        @endforeach 
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Email</label>
                                <input type="text" name="email" class="form-control" placeholder="Email">
                            </div>

                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Phone #</label>
                                <input type="text" name="phone_number" class="form-control" placeholder="Phone Number">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Gender</label>
                                <select type="text" class="form-control select2" name="gender" style="width:100% ">
                                    <option value="">Choose One Option</option>
                                    <?php
                                        $arr = config('constants.gender');
                                        foreach ($arr as $key => $val) {
                                            if($key == @$_GET['gender']) $selected = 'selected="selected"'; else $selected = '';
                                            echo '<option  value="'.$key.'" '.$selected.'>'.$val.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <?php if (Auth::user()->role == 0 || Auth::user()->role == 7) { ?>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Physician</label>
                                <select type="text" class="form-control select2" name="primary_physician" style="width:100% ">
                                    <option value="">Select Physician</option>
                                    <?php
                                        $arr = $physicians;
                                        foreach ($arr as $key => $val) {
                                            echo '<option  value="'.$val['id'].'">'.$val['name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        <?php } ?>
                        </section>
                    </div>
                    <div class="form-actions ">
                        <button type="submit" class="btn blue pull-right"><i class="fa fa-search"></i> Search</button>
                         <a href="{{url('patients')}}" class="btn blue pull-left"> Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
                                       <!--  <div class="tools"> </div> -->
                                    </div>
                                    <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover filter_table" id="patients_tables">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th> Location </th>
                                <th> DOB </th>
                                <th> Locations </th>
                                <th> Primary Concern(s) </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i      =   1;
                                $html   =   "";
                                if(count($list) > 0){
                                    foreach($list as $key=>$row)  { 
                                        $html .= '<tr class="row-'.$row['pa_id'].' list_'.++$key.' list">';
                                        $html .= '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
                                        $html .= '<td>'.$row['state'].' '.$row['city'].'</td>';
                                        if(!empty($row['date_birth'])){
                                            $html .= '<td>'.$row['date_birth'].'</td>';
                                        }else{
                                            $html .= '<td>No associated</td>';
                                        }
                                        $html .= '<td>'.$row['location_name'].'</td>';
                                        if(!empty($row['primary_concern'])){
                                            $primary_concerns   =   explode(',',$row['primary_concern']);
                                            $concerns           =   '';
                                            foreach ($primaryConcern as $concern => $concernVal){
                                                if (in_array($concernVal['id'], $primary_concerns)) {
                                                    $concerns .= $concernVal['title']."<br>";  
                                                }
                                            }
                                            $html .= '<td>'.$concerns.'</td>';
                                        }else{
                                            $html .= '<td>No associated conditions</td>';
                                        }
                                        $html .= '<td><a class="btn btn-xs blue" href="'.url('/patient/update/'.$row['pa_id']).'"><i class="fa fa-edit"></i></a>';
                                        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
                                            $html .= '<a class="delete btn btn-xs red" data-url="'.url('/patient/delete/').'/'.$row['pa_id'].'"     href="javascript:void(0);" data-remove="list_'.$key.'"><i class="fa fa-trash"></i></a></td>';
                                        }
                                        $html .= '</tr>';
                                    }
                                    echo $html;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</div>