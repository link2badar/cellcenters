@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/patient/add/'.@$seminar)}}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('first_name')? 'has-error': '' )}}">
                                <label class="control-label">First Name</label>
                                <input type="text" class="form-control required" id="first_name" value="{{ old('first_name') }}" name="first_name" placeholder="First Name">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('last_name')? 'has-error': '' )}}">
                                <label class="control-label">Last Name</label>
                                <input type="text" class="form-control required" id="last_name" value="{{ old('last_name') }}" name="last_name" placeholder="Last Name">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label">Email Address</label>
                                <input type="text" class="form-control required"  name="email" value="{{ old('email') }}" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('haboutus')? 'has-error': '' )}}">
                                <label class="control-label">How Did You Hear About Us? </label>
                                <select type="text" class="form-control select2 required" name="haboutus" style="width:100%">
                                    <option value="">Choose How Did You Hear About Us</option>
                                    <?php
                                        $arr = config('constants.haboutus');
                                        foreach ($arr as $key=>$val) {
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select>
                                @if ($errors->has('haboutus'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('haboutus') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('phone_number')? 'has-error': '' )}}">
                                <label class="control-label">Phone Number </label>
                                <input type="text" class="form-control required" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number">
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Cell Phone Number </label>
                                <input type="text" class="form-control" name="cell_phone_number" value="{{ old('cell_phone_number') }}" placeholder="Cell Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label">Address </label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}"placeholder="Address">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">City </label>
                                <input type="text" class="form-control" name="city" value="{{ old('city') }}" placeholder="City">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">State </label>
                                <input type="text" class="form-control" name="state" value="{{ old('state') }}" placeholder="State">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Zip/Postal Code  </label>
                                <input type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}" placeholder="Zip/Postal Code">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Date of Birth </label>
                                <input type="text" class="form-control datepicker" name="date_birth" value="{{ old('date_birth') }}" placeholder="(MM/DD/YYYY)" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" >Gender</label>
                                <select type="text" class="form-control select2" name="gender" style="width:100% ">
                                    <option value="">Choose One Option</option>
                                    <?php
                                        $arr = config('constants.gender');
                                        foreach ($arr as $key => $val) {
                                            echo '<option  value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select> 
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('phone_number')? 'has-error': '' )}}">
                                <label class="control-label">Preferred Clinic  </label>
                                <select type="text" class="form-control select2 preferredClinics" required="required" name="preferred_clinic" style="width:100%">
                                    <option value="" disabled="disabled" selected="selected">Choose One Clinic</option>
                                    <?php
                                        if(!empty($clinics)):
                                            foreach ($clinics as $key=>$val) {
                                                echo '<option  value="'.$val['loc_id'].'">'.$val['location_name'].'</option>';
                                            }
                                        endif;
                                    ?>
                                </select>
                                @if ($errors->has('preferred_clinic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('preferred_clinic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"> Case Manager   </label>
                                <select type="text" class="form-control select2" name="case_manager" style="width:100%" required="required">
                                    <option value="" disabled="disabled">Choose One Option</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Primary Physician   </label>
                                <select type="text" class="form-control select2" name="primary_physician" style="width:100%" required="required">
                                    <option value="" disabled="disabled">Choose One Option</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label">Primary Concern(s)</label>
                                <select type="text" class="form-control select2" name="primary_concern[]" style="width:100%" multiple>
                                    <?php 
                                        $arr = config('constants.primary_concerns');
                                        foreach ($primaryConcern as $key=>$val) {
                                            echo '<option  value="'.$val['id'].'">'.$val['title'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="control-label">Note:  </label>
                                <textarea  class="form-control ckeditor" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add {{ $module}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('footer')
</div>