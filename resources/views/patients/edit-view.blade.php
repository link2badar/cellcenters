@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/patient/update/'.$patient['pa_id'] )}}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="pa_id" value="{{ $patient['pa_id'] }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('first_name')? 'has-error': '' )}}">
                                <label class="control-label">First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ $patient['first_name'] }}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('last_name')? 'has-error': '' )}}">
                                <label class="control-label">Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ $patient['last_name'] }}">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('email')? 'has-error': '' )}}">
                                <label class="control-label">Email Address</label>
                                <input type="text" class="form-control"  name="email" placeholder="Email" value="{{ $patient['email'] }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('haboutus')? 'has-error': '' )}}">
                                <label class="control-label">How Did You Hear About Us? </label>
                                <select type="text" class="form-control select2" name="haboutus" style="width:100%">
                                    <option value="">Choose How Did You Hear About Us</option>
                                    <?php
                                        $arr = config('constants.haboutus');
                                        foreach ($arr as $key=>$val) {
                                            $sel = ($key == $patient['haboutus']) ? 'selected="selected"' : '';
                                            echo '<option '.$sel.' value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select>
                                @if ($errors->has('haboutus'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('haboutus') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('phone_number')? 'has-error': '' )}}">
                                <label class="control-label">Phone Number </label>
                                <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" value="{{ $patient['phone_number'] }}">
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Cell Phone Number </label>
                                <input type="text" class="form-control" name="cell_phone_number" placeholder="Cell Phone Number" value="{{ $patient['cell_phone_number'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label">Address </label>
                                <input type="text" class="form-control" name="address" placeholder="Address" value="{{ $patient['address'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">City </label>
                                <input type="text" class="form-control" name="city" placeholder="City" value="{{ $patient['city'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">State </label>
                                <input type="text" class="form-control" name="state" placeholder="State" value="{{ $patient['state'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Zip/Postal Code  </label>
                                <input type="text" class="form-control" name="zipcode" placeholder="Zip/Postal Code" value="{{ $patient['zipcode'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Date of Birth (MM/DD/YYYY)  </label>
                                <input type="text" class="form-control datepicker" name="date_birth" placeholder="(MM/DD/YYYY)" value="{{ $patient['date_birth'] }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" >Gender</label>
                                <select type="text" class="form-control select2" name="gender" style="width:100% ">
                                    <option value="">Choose One Option</option>
                                    <?php
                                        $arr = config('constants.gender');
                                        foreach ($arr as $key => $val) {
                                            $sel = ($key == $patient['gender']) ? 'selected="selected"' : '';
                                            echo '<option '.$sel.' value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select> 
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Preferred Clinic  </label>
                                <select type="text" class="form-control select2 preferredClinics" name="preferred_clinic" style="width:100% ">
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php
                                        if(!empty($clinics)):
                                            foreach ($clinics as $key => $val) {
                                                $sel = ($val['loc_id'] == $patient['preferred_clinic']) ? 'selected="selected"' : '';
                                                echo '<option '.$sel.'  value="'.$val['loc_id'].'">'.$val['location_name'].'</option>';
                                            }
                                        endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"> Case Manager   </label>
                                <select type="text" class="form-control select2" name="case_manager" style="width:100%">
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php if (!empty($manager)) {?>
                                <option value="<?php echo $manager[0]['id'] ?>"><?php echo $manager[0]['name'] ?></option>                                  
                                   <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Primary Physician   </label>
                                <select type="text" class="form-control select2" name="primary_physician" style="width:100%">
                                    <option value="" disabled="disabled">Choose One Option</option>
                                    <?php if (!empty($physician)) {?>
                                <option value="<?php echo $physician[0]['id'] ?>"><?php echo $physician[0]['name'] ?></option>                                  
                                   <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label">Primary Concern(s)</label>
                                <select type="text" class="form-control select2" name="primary_concern[]" style="width:100%" multiple>
                                    <?php 
                                        $concerns   =   [];
                                        if(!empty($patient['primary_concern'])){
                                            $concerns = explode(',',$patient['primary_concern']);
                                        }
                                        foreach ($primaryConcern as $key=>$val) {
                                            $sel =  (in_array($val['id'],$concerns)) ? 'selected="selected"' : '';
                                            echo '<option '.$sel.' value="'.$val['id'].'">'.$val['title'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="control-label">Note:  </label>
                                <textarea  class="form-control ckeditor" name="note">{{ $patient['note'] }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('footer')