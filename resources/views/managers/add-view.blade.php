@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/manager/add')}}" method="post" enctype="multipart/form-data" class="validate_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 form-group {{ ($errors->has('first_name')? 'has-error': '' )}}">
                                <label class="control-label" for="first_name">First Name</label>
                                <input type="text" class="form-control required" id="first_name" name="first_name" placeholder="First Name">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group {{ ($errors->has('last_name')? 'has-error': '' )}}">
                                <label class="control-label" for="last_name">Last Name</label>
                                <input type="text" class="form-control required" id="last_name" name="last_name" placeholder="Last Name">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="email">Email Address </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="phone_no">Phone Number </label>
                                <input type="text" class="form-control" name="phone_no" id="phone_no" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="cell_no">Cell Phone Number </label>
                                <input type="text" class="form-control" name="cell_no" id="cell_no" placeholder="Cell Phone Number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="address">Address </label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="city">City </label>
                                <input type="text" class="form-control" name="city" id="city" placeholder="City">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="state">State </label>
                                <input type="text" class="form-control" name="state" id="state" placeholder="State">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Date of Birth (MM/DD/YYYY)  </label>
                                <input type="text" class="form-control datepicker" name="date_birth" placeholder="(MM/DD/YYYY)">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Gender  </label>
                                <select type="text" class="form-control select2" name="gender">
                                    <option value="">Choose One Option</option>
                                    <?php 
                                        $arr    =   config('constants.gender');
                                        foreach ($arr as $key => $val) {
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="manager_image">Manager Image  </label>
                                <input type="file" class="form-control upload-image" name="manager_image" id="manager_image" placeholder="Manager Image">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="zipcode">Zip/Postal Code  </label>
                                <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip/Postal Code">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')