@include('header')
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <div class="pull-right">
            <a  href="<?php echo url('/manager/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add New {{ $module }} 
            </a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover filter_table" id="managers_tables">
                        <thead>
                            <tr>  
                                <th class="text-center"> #</th>
                                <th class="text-center"> Image </th>
                                <th class="text-center"> First Name </th>
                                <th class="text-center"> Last Name </th>
                                <th class="text-center"> Email </th>
                                <th class="text-center"> Location </th>
                                <th class="text-center"> Cell Number </th>
                                <th class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($list))
                                @foreach($list as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }}
                                        </td>
                                        <td>
                                            @if($li['image'] && !empty($li['image']))
                                                <img height="80px" src="{{ url('/managers_imgs/')}}/{{$li['image']}}" width="80px" class="image-viewer">
                                            @else
                                                <img height="80px" src="{{ url('/assets/img/')}}/no-image.jpeg" width="80px">
                                            @endif
                                        </td>
                                        <td>
                                            {!! ($li['first_name']) ? $li['first_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li['last_name']) ? $li['last_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                       <td>
                                            {!! ($li['email']) ? $li['email'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {{ $li['address'] }} {{ $li['city'] }} {{ $li['state'] }}
                                        </td>
                                        <td>
                                            {!! ($li['cell_no']) ? $li['cell_no'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            <a class="btn btn-xs blue" href="{{ url('/manager/update/').'/'.$li['manager_id'] }}"><i class="fa fa-edit"></i></a> -  
                                            <a class="delete btn btn-xs red" data-url="{{ url('/manager/delete/').'/'.$li['manager_id'] }}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')