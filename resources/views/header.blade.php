<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>{{$page_title}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ url('/')}}/public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ url('/')}}/public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ url('/')}}/public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ url('/')}}/public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/')}}/public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ url('/')}}/public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> 

    <link href="{{ asset('/') }}public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

    <!-- NOTIFICATION CSS -->
    <link href="{{ asset('/') }}public/assets/global/plugins/jquery-notific8/jquery.notific8.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!--  Image Viewer -->
    <link href="{{ asset('/') }}public/assets/image-viewer/imageviewer.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}public/assets/global/css/easycal.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
    #iv-container {
        position: fixed;
        background: #0d0d0d;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        display: none;
        z-index: 20000;
    }
    /* Select2 hide selected option */
    .select2-results__option[aria-selected=true] {
        display: none;
    }

    .no_diplay{

        display: none;

    }

    .calendarColor{
        height: 20px;
        width: 20px;
        border: 1px solid #ccc;
        float: left;
    }

/* .box {
    display:block;
    width:200px;
    height:100px;
    background-color:#DDD;
    } */
    #pop {
        padding:0px 0px;
    }
    #example {
        position:relative;
    }
    .closed{
        background: url('https://portal.stemcellcenters.com/img/unavailableTexture.jpg');
    }
    .bor{
        border: 1px solid #8e8e8e;
    }
</style>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript"> var site_url = "{{url('/')}}";</script>
<script src="{{ asset('/')}}/public/assets/global/scripts/easycal.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<!-- <script src="{{ asset('/')}}/public/assets/global/scripts/dataset.js" type="text/javascript"></script> -->


<script>
    tinymce.init({ selector: '.tinymce' });
</script>
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ url('/') }}" style="color: white;">
                         <img height="50" width="150" src="http://seminarsforstemcells.com/public/assets/layouts/layout/img/stemlogo.jpeg" alt="logo" class="logo-default" style="margin-top: 0px;"></a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user">
                        <?php if (Auth::user()->role != 6 && Auth::user()->role != 0 && Auth::user()->role != 7) { ?>
                    <select style="width: 100%; margin-top: 10px;" id="location_select"  type="text" data-placeholder="Choose Location" class="form-control" name="">
                        <?php 
                        if( Auth::user()->role != 0 ){
                            print_r(Auth::user());
                        $userlocations = App\User::where('id', Auth::user()->id)->get()->toArray();
                        $userlocations    =   explode(',',$userlocations[0]['location_id']);
                          if(!empty($userlocations)){
                             foreach ($userlocations as $key => $userlocation) {
                                  $selected   = "";
                                  $locdata =  App\Models\Locations::find($userlocation);
                                  if($userlocations[0] == $locdata['loc_id']){
                                    $selected   = 'selected="selected"';
                                    }
                                  echo '<option value="'.$key.'" '.$selected.'>'.$locdata['location_name'].'</option>';
                             }
                          }
                      }/*else {
                            $syslocations =  App\Models\Locations::all()->toArray();
                            if(!empty($syslocations)){
                                echo '<option value="" disabled="disabled" selected="selected">Choose Location</option>';
                                foreach ($syslocations as $key => $syslocation) {
                                    $selected   = " ";
                                if(Auth::user()->location_id == $syslocation['loc_id']){
                                    $selected   = 'selected="selected"';
                                    }
                                  echo '<option value="'.$syslocation['loc_id'].'" '.$selected.'>'.$syslocation['location_name'].'</option>';
                                }
                            }

                      }*/
                       ?>
                   </select>
               <?php }?>
                    </li>
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="{{ url('/')}}/public/assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{ url('/account')}}">
                                            <i class="fa fa-gear"></i> Settings </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/logout')}}">
                                                <i class="icon-key"></i> Log Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- END USER LOGIN DROPDOWN -->
                                </ul>
                            </div>
                            <!-- END TOP NAVIGATION MENU -->
                        </div>
                        <!-- END HEADER INNER -->
                    </div>
                    <!-- END HEADER -->
                    <!-- BEGIN HEADER & CONTENT DIVIDER -->
                    <div class="clearfix"> </div>
                    <!-- END HEADER & CONTENT DIVIDER -->
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN SIDEBAR -->
                        <div class="page-sidebar-wrapper">
                            <!-- BEGIN SIDEBAR -->
                            <div class="page-sidebar navbar-collapse collapse" style="position: fixed;">
                                <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                                    <li class="sidebar-toggler-wrapper hide">
                                        <div class="sidebar-toggler">
                                            <span></span>
                                        </div>
                                    </li>
                                    <!-- END SIDEBAR TOGGLER BUTTON -->
                                    <li class="nav-item start active">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <i class="icon-home"></i>
                                            <span class="title">Dashboard</span>
                                    <!-- <span class="selected"></span>
                                        <span class="arrow open"></span> -->
                                    </a>
                                </li>
                                <?php if(Auth::user()->role == 0){ ?>
                                    <li class="nav-item start">
                                        <a href="{{url('/physicians')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-user"></i>
                                            <span class="title">Physicians</span>
                                        </a>
                                    </li>
                                <?php } if (Auth::user()->role != 6) {?>
                                    <li class="nav-item start">
                                        <a href="{{url('patients')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-user"></i>
                                            <span class="title">Patients</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start">
                                        <a href="{{ url('/calendar?date='.date('d/m/Y'))}}" class="nav-link nav-toggle">
                                            <i class="fa fa-calendar"></i>
                                            <span class="title">Calendar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start">
                                        <a href="{{url('contacts')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Contact Queue</span>
                                        </a>
                                    </li>
                                    <?php if (Auth::user()->role != 7) { ?>
                                    <li class="nav-item start">
                                        <a href="{{ url('/locations')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-map-marker"></i>
                                            <span class="title">Locations</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start">
                                        <a href="{{ url('/staff') }}" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Staff</span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <?php } if(Auth::user()->role != 3 && Auth::user()->role != 6){ ?>
                                    <li class="nav-item start">
                                        <a href="{{ url('/primary-concern') }}" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Primary Concerns</span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                     <?php if(Auth::user()->role == 0 || Auth::user()->role == 7){ ?>
                                    <li class="nav-item start">
                                        <a href="{{ url('/templates') }}" class="nav-link nav-toggle">
                                            <i class="fa fa-clone"></i>
                                            <span class="title">Templates</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start">
                                        <a href="{{ url('/speakers') }}" class="nav-link nav-toggle">
                                            <i class="fa fa-bullhorn"></i>
                                            <span class="title"><?php if(Auth::user()->role == 0){ echo "Admin Employee/Speaker"; }else{ echo "Speaker"; } ?></span>
                                        </a>
                                    </li>
                                <?php } ?> <li class="nav-item start">
                                        <a href="{{ url('/seminars') }}" class="nav-link nav-toggle">
                                            <i class="fa fa-comments"></i>
                                            <span class="title">Seminars</span>
                                        </a>
                                    </li>
                           
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar" >
                            <ul class="page-breadcrumb">
                                @foreach ($breadcrumbs as $key => $value)
                                <li>
                                    @if($key != "#")
                                    <a href="{{url($key)}}">{{$value}}</a>
                                    <i class="fa fa-circle"></i>
                                    @else
                                    <span>{{$value}}</span>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                            <div class="page-toolbar">
                                @yield('content')
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        