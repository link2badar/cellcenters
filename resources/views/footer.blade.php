            </div><!-- Ends CONTENT -->
        </div><!-- Ends CONTAINER -->
    </div><!-- Ends Page Wrapper -->
</div><!-- Main Div Ends -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2018 &copy; Cellcenters
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- MOdal Html -->
<!-- Modal Html -->
<div class="modal fade in" id="data_modal" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <p style="text-align:center;"><br/><i class="fa fa-spinner fa-spin"></i>   Loading</p>
        </div>
    </div>
</div>
<?php 
$dataSet = [];
$locTiming = array();
if(isset($list[0]['appointment_id'])){
    $timingArr = []; $c = 0;
    $calendarTypes  = config('constants.calendarTypes');
    $calendarColor  = config('constants.calendarColor');
    foreach ($list as $key => $val) {
        $dataSet[] = [ 
            'id'                => $val['appointment_id'],
            'pa_id'             => $val['pa_id'],
            'fullName'          => $val['first_name'].' '.$val['last_name'],
            'title'             => ucfirst(substr($val['first_name'], 0, 1).', '.$val['last_name']),
            'start'             => date("d-m-Y", strtotime($val['appointment_date'])).' '.date("H:i:s", strtotime($val['start_time'])),
            'end'               => date("d-m-Y", strtotime($val['appointment_date'])).' '.date("H:i:s", strtotime($val['end_time'])),
            'start_time'        => date("h:i A", strtotime($val['start_time'])),
            'end_time'          => date("h:i A", strtotime($val['end_time'])),
            'backgroundColor'   => $calendarColor[$val['appointment_type']],
            'appointmentType'   => $calendarTypes[$val['appointment_type']],
            'textColor'         => '#FFF'
        ];

    }

} ?>
<script src="{{ asset('/')}}/public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<?php 
if(isset($_GET['date'])){
    $calendarDate = $_GET['date'];
    $colDateHeader= date('Y-m-d', strtotime($_GET['date']));
}else{
    $calendarDate = date("d-m-Y");
    $colDateHeader= date('Y-m-d');
}
$daily      =   'no';
if (isset($_GET['daily'])) {
    $daily   =  'yes';
}
?>
<script>
    $('.mycal').easycal({
        startDate : '<?php echo $calendarDate ?>', // OR 31/10/2104
        colDateHeader: '<?php echo $colDateHeader ?>',
        timeFormat : 'hh:mm A',
        moreInfo: '',
        manage  : 'manage',
        columnDateFormat : 'dddd, DD MMM',
        minTime : '00:00:00',
        maxTime : '24:00:00',
        slotDuration : 30,
        timeGranularity : 30,
        daily   : '<?php echo $daily ?>',
        //opening and closing time of clinic
        day1_o  :   '<?php echo @date("H:i:s", $locationTiming[0]['open_time']) ?>',
        day1_c  :   '<?php echo @date("H:i:s", $locationTiming[0]['close_time']) ?>',
        day2_o  :   '<?php echo @date("H:i:s", $locationTiming[1]['open_time']) ?>',
        day2_c  :   '<?php echo @date("H:i:s", $locationTiming[1]['close_time']) ?>',
        day3_o  :   '<?php echo @date("H:i:s", $locationTiming[2]['open_time']) ?>',
        day3_c  :   '<?php echo @date("H:i:s", $locationTiming[2]['close_time']) ?>',
        day4_o  :   '<?php echo @date("H:i:s", $locationTiming[3]['open_time']) ?>',
        day4_c  :   '<?php echo @date("H:i:s", $locationTiming[3]['close_time']) ?>',
        day5_o  :   '<?php echo @date("H:i:s", $locationTiming[4]['open_time']) ?>',
        day5_c  :   '<?php echo @date("H:i:s", $locationTiming[4]['close_time']) ?>',
        day6_o  :   '<?php echo @date("H:i:s", $locationTiming[5]['open_time']) ?>',
        day6_c  :   '<?php echo @date("H:i:s", $locationTiming[5]['close_time']) ?>',
        day7_o  :   '<?php echo @date("H:i:s", $locationTiming[6]['open_time']) ?>',
        day7_c  :   '<?php echo @date("H:i:s", $locationTiming[6]['close_time']) ?>',
                

        dayClick : function(el, startTime, startDate){
            dd = moment(startDate, 'DD/MM/YYYY').format('dd MM D YYYY');
            var startTime = startTime.substr(0, startTime.length - 3); 
            d = new Date(dd);
            d = d.getDay();
            $.ajax({
                type:  "GET",
                async: false,
                cache: false,
                url:   'location/check-timing?time='+startTime+'&day='+d+'&date='+startDate,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    if (res == 1) {
                      window.location.href="appointment/add?time="+startTime+'&date='+startDate;
            
                        //res[0].link;
                    }
                }
            });


            
        },
        eventClick : function(eventId){
            console.log('Event was clicked with id: ' + eventId);
        },

        events : getEvents(),
        
        overlapColor : '#FF0',
        overlapTextColor : '#000',
        overlapTitle : 'Multiple'
    });



    function getEvents(){

        var data = <?php echo json_encode(array_values($dataSet)); ?>;
        console.log(data);
        return data;
    }

    function changeCalendarType(selectedDate, daily = null, locid = null, phy = null) {
        var mydate = new Date(selectedDate);
        var month = parseInt(mydate.getMonth())+parseInt(1);

        if (locid != null && phy != null) {
            if (daily != null && daily != '') {
                window.location.href="?location_id="+locid+"&primary_physician="+phy+"&date="+mydate.getDate()+'-'+month+'-'+mydate.getFullYear()+'&daily=yes';
            }
            else{
                window.location.href="?location_id="+locid+"&primary_physician="+phy+"&date="+mydate.getDate()+'-'+month+'-'+mydate.getFullYear();
            }
        }else{
            if (daily != null && daily != '') {
                window.location.href="?date="+mydate.getDate()+'-'+month+'-'+mydate.getFullYear()+'&daily=yes';
            }
            else{
                window.location.href="?date="+mydate.getDate()+'-'+month+'-'+mydate.getFullYear();
            }
        }
    }
</script>

<!-- END CORE PLUGINS -->
<!-- Data tables plugins -->
<script src="{{ asset('/public/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('/public/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/public/assets/pages/scripts/table-datatables-buttons.min.js') }}" type="text/javascript"></script>
<!-- end datatables -->
<!-- Date Time Plugins -->
<script src="{{ asset('/')}}/public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- Other Plugins -->
<script src="{{ asset('/')}}/public/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('/')}}/public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/pages/scripts/form-validation.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('/')}}/public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="{{ asset('/')}}/public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--  Form Repeator -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/public/assets/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- Image Viewer -->
<script src="{{ asset('/') }}/public/assets/image-viewer/imageviewer.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });

    
    $(".pop").popover({ trigger: "manual" , html: true, animation:false})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });

</script>
@include('global_script')
</body>
</html>