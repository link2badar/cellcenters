@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/location/update/'.$location['loc_id'])}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="loc_id" value="{{ $location['loc_id']}}">
                    <div class="form-body">
                        <section class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label" for="location_name">Clinic Name</label>
                                <input type="text" class="form-control" required="required" id="location_name" name="location_name" placeholder="Clinic Name" value="{{ $location['location_name'] }}">
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="control-label" for="location_address">Physical Address </label>
                                <input type="text" class="form-control" name="location_address" id="location_address" placeholder="Physical Address" value="{{ $location['location_address'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_city">City </label>
                                <input type="text" class="form-control" name="location_city" id="location_city" placeholder="City" value="{{ $location['location_city'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_state">State </label>
                                <input type="text" class="form-control" name="location_state" id="location_state" placeholder="State" value="{{ $location['location_state'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_zipcode">Zip/Postal Code </label>
                                <input type="text" class="form-control" name="location_zipcode" id="location_zipcode" placeholder="Zip/Postal Code" value="{{ $location['location_zipcode'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_phone_no">Phone Number </label>
                                <input type="text" class="form-control" name="location_phone_no" id="location_phone_no" placeholder="Phone Number" value="{{ $location['location_phone_no'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_fax">Fax Number </label>
                                <input type="text" class="form-control" name="location_fax" id="location_fax" placeholder="Zip/Postal Code" value="{{ $location['location_fax'] }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label" for="location_image">Image </label>
                                <input type="file" class="form-control upload-image" name="location_image" id="location_image" placeholder="Clinic Image">
                            </div>
                        </section>
                        <label class="control-label">Location Hours</label>
                        <div class="table-responsive">
                            <table class="table table-bordered no-footer">
                                <thead class="">
                                    <tr role="row" class="heading">
                                        <th class="text-center"></th>
                                        <th class="text-center">MON</th>
                                        <th class="text-center">TUE</th>
                                        <th class="text-center">WED</th>
                                        <th class="text-center">THU</th>
                                        <th class="text-center">FRI</th>
                                        <th class="text-center">SAT</th>
                                        <th class="text-center">SUN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        @php $i = 1; @endphp
                                        @foreach($timings as $key => $time)
                                        <td>
                                            <div class="input-group margin-bottom-5">
                                                <label class="control-label" for="start_time"></label>
                                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="start_time" name="time[{{$i}}][open]" value="{{ date('h:i a',  $time['open_time']) }}">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-clock-o"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="input-group">
                                                <label class="control-label" for="start_time"></label>
                                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="start_time" name="time[{{$i}}][close]" value="{{ date('h:i a' ,  $time['close_time'] ) }}">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-clock-o"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        @php $i++ @endphp
                                        @endforeach
                                    </tr>
                                    <tr class="text-center">
                                        <td colspan="10">* If any days are left empty then it is assumed that this location is closed all day</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="hours_description">Description of hours (ex Mon-Fri 8am-5pm)</label>
                                <input type="text" name="hours_description" placeholder="Monday-Friday 8am-5pm" class="form-control" value="{{ $location['hours_description'] }}">
                            </div>
                            <div class="col-md-12"> 
                                <label class="control-label" for="description">Description</label>
                                <textarea class="ckeditor" class="form-control" name="description">{{ $location['description'] }}</textarea>  
                            </div>
                            <div class="col-md-12"> 
                                <label class="control-label" for="info_description">Directional Information</label>
                                <textarea class="ckeditor" class="form-control" name="info_description">{{ $location['info_description'] }}</textarea>  
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save Changes</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')