@include('header')
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
         <?php if(Auth::user()->role == 0){ ?>
        <div class="pull-right">
            <a  href="<?php echo url('/location/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add New {{ $module }}
            </a>
        </div>
    <?php } ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover filter_table" id="locations_tables">
                        <thead>
                            <tr>  
                                <th class="text-center"> #</th>
                                <th class="text-center"> Location Name </th>
                                <th class="text-center"> Location Address </th>
                                <?php if( Auth::user()->role != 3 ){ ?>
                                <th class="text-center"> Actions </th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($list))
                                @foreach($list as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }}
                                        </td>
                                        <td>
                                            {!! ($li['location_name']) ? $li['location_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {{ $li['location_address'] }} {{ $li['location_city'] }} {{ $li['location_state'] }}
                                        </td>
                                     <?php if( Auth::user()->role != 3 ){ ?>
                                        <td>
                                            <a class="btn btn-xs blue" href="{{ url('/location/update/').'/'.$li['loc_id'] }}"><i class="fa fa-edit"></i></a>
                                        
                                            
                                             <?php if( Auth::user()->role == 0  ){ ?> - 
                                            <a class="delete btn btn-xs red" data-url="{{ url('/location/delete/').'/'.$li['loc_id'] }}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')