$data['seminars']       =   collect($seminars)->map(function($x){ return (array)$x; })->toArray();

$photos_query->whereRaw('Date(area_logs.created_at) between "'.$request->input('start_date').'" and "'.$request->input('end_date').'" ')->orderBy('area_logs.created_at','asc')->distinct();
    else
$photos_query->whereRaw('Date(area_logs.created_at) = CURDATE()')->orderBy('area_logs.created_at','asc');

select distinct * from `seminars` where `date` between '2018-09-10' and '2018-09-16' order by `date` asc

$validator  =   Validator::make($data,[
                'first_name'  =>  'required',
                'last_name'   =>  'required',
                "email"       =>  "required|unique:managers,email,$id,manager_id",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }

@if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif

{{ ($errors->has('last_name')? 'has-error': '' )}}