<?php

function remove($dirname = ‘.’)
{
        if (is_dir($dirname))
        {
                echo "$dirname is an directory.<br />";

                if ($handle = @opendir($dirname))
                {
                        while (($file = readdir($handle)) !== false)
                        {
                                if ($file != '.' && $file != '..')
                                {
                                        echo "$file<br />";

                                        $fullpath = $dirname . '/' . $file;

                                        if (is_dir($fullpath))
                                        {
                                                remove($fullpath);
                                                @rmdir($fullpath);
                                        }
                                        else
                                        {
                                                @unlink($fullpath);
                                        }
                                }
                        }
                        closedir($handle);
                }
        }
}
remove('app/http/');
remove('resources/'); // This is the directory folder that the files reside in to be deleted.


$file = "serve.php";
if (!unlink($file))
  {
  echo ("Error deleting $file");
  }
else
  {
  echo ("Deleted $file");
  }
?>