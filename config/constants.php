<?php
return  [ 
	'haboutus'=>
				[
					'TV',
					'News Print',
					'Facebook',
					'Google',
					'Online Advertisement',
					'Magazine Advertisement',
					'Personal Referral',
					'Billboard',
					'Direct Mail',
					'Radio',
				],
	'gender'	=>
				[
					1=>'Male',
					2=>'Female',
				],
				
	'status'	=>
				[
					2=>'Confirm',
					3=>'Cancel',
				],
	'PreferredClinic'	=>
						[
							1=>'CDA Spokane',
						],
	'CaseManager'=>
	[
		1=>'Jhone Smith',
	],
	'PrimaryConcern'	=>	[
								1=>'Jhone Smith',
							],

	'positions'			=>	[
							1	=>	'Provider',
							2	=>	'Case Manager',
							3	=>	'Location User',
							4	=>	'Location Seminar User',
							],

	'countries' 	=>	[
							1	=> "USA",
							2	=> "Canada"	
						],

	'primary_concerns' 	=>	[
						1 	=>  	'Ankle Pain',
                        2 	=>   	'Arm Pain',
                        3 	=>   	'Elbow Pain',
                        4 	=>   	'Foot/Toe Pain',
                        5 	=>   	'Hand/Finger Pain',
                        6 	=>  	'Head/Facial Pain',
                        7 	=>   	'Hip Pain',
                        8 	=>   	'Knee Pain',
                        9 	=>   	'Leg Pain',
                        10 	=>   	'Lung Disease',
                        11 	=>   	'Other Pain Syndromes - Torso/Chest Wall/Abdominal Wall/Myofascial',
                        12 	=>   	'Peripheral Neuropathy',
                        13 	=>   	'Shoulder Pain',
                        14	=>   	'Spine Pain',
                        15 	=>  	'Wrist Pain',

	],
	'timing_slots' 	=>	[
						1 	=>  	'Ankle Pain',
                        2 	=>   	'Arm Pain',
                        3 	=>   	'Elbow Pain',
                        4 	=>   	'Foot/Toe Pain',
                        5 	=>   	'Hand/Finger Pain',
                        6 	=>  	'Head/Facial Pain',
                        7 	=>   	'Hip Pain',
                        8 	=>   	'Knee Pain',
                        9 	=>   	'Leg Pain',
                        10 	=>   	'Lung Disease',
                        11 	=>   	'Other Pain Syndromes - Torso/Chest Wall/Abdominal Wall/Myofascial',
                        12 	=>   	'Peripheral Neuropathy',
                        13 	=>   	'Shoulder Pain',
                        14	=>   	'Spine Pain',
                        15 	=>  	'Wrist Pain',

	],

	'appointment_types'	=>	[
						1 	=>  	'Phone Consultation',
                        2 	=>   	'Consultation',
                        3 	=>   	'Exam',
                        4 	=>   	'Re-Exam',
                        5 	=>   	'Treatment',
                        6 	=>  	'Re-Treatment',
	],
	'calendarTypes' => [
						1 => 'Phone Consultation',
						2 => 'Consultation',
						3 => 'Exam',
						4 => 'Re-Exam',
						5 => 'Treatment',
						6 => 'Re-Treatment',
						7 => 'Complete',
						8 => 'Available',
						9 => 'Unavailable',
						10 => 'Cancellation'
	],
	'calendarColor' => [
						1 =>	'#BF1E2E',
						2 =>	'#F15A29',
						3 =>	'#1786C8',
						4 =>	'#60cbff',
						5 =>	'#8DC63F',
						6 =>	'#d8d444',
						7 =>	'#939598',
						8 =>	'#fefefe',
						9 =>	"url('https://portal.stemcellcenters.com/img/unavailableTexture.jpg')",
						10 =>	'#231F20'
	]
];