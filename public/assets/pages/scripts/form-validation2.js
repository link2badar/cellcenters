
var handleValidation2 = function(rules) {

    var form2       = $('#form_sample_2');
    var error2      = $('.alert-danger', form2);
    var success2    = $('.alert-success', form2);

    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: rules,
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
        },

        unhighlight: function (element) { // revert the change done by hightlight
            
        },

        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            form[0].submit(); // submit the form
        }
    });
}

$(document).ready(function() {
    param = {
        category: {
            required: true
        },
        company_name: {
            required: true
        },
        name: {
            required: false    
        },
        fname: {
            required: true
        },
        cnic: {
            required: true
        },
        cnic_expiry: {
            required: true
        },
        driverlic: {
            required: true
        },
        license_category: {
            required: true
        },
        license_authority: {
            required: true
        },
        license_expiry: {
            required: true
        },
        mobileno: {
            required: true
        },
        driver_address: {
            required: true
        },
        driver_location: {
            required: true
        },
        joining_date: {
            required: true
        },
        bdate: {
            required: true
        },
        last_employer: {
            required: true
        },
        jobexp: {
            required: true
        },
    };


    if ( $('select[name="category"] option:selected').val() == 0 ){
        handleValidation2(param);
    }
    
    $('select[name="category"]').on('change', function() {
        if($(this).val() != 0){

            $('input[name="driverlic"]').rules('remove',
                "required"
            );
            $('input[name="license_category"]').rules('remove',
                "required"
            );
            $('input[name="license_authority"]').rules('remove',
                 "required"
            );
            $('input[name="license_expiry"]').rules('remove',
                 "required"
            );
        }else{

            $('input[name="driverlic"]').rules('add',
                {required:true}
            );
            $('input[name="license_category"]').rules('add',
                {required:true}
            );
            $('input[name="license_authority"]').rules('add',
                 {required:true}
            );
            $('input[name="license_expiry"]').rules('add',
                 {required:true}
            );

        }
    });
});
