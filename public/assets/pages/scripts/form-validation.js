var FormValidation = function () {

    var handleValidation2 = function() {

        var form2       = $('.validate_form');
        var error2      = $('.alert-danger', form2);
        var success2    = $('.alert-success', form2);

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {},
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                form[0].submit(); // submit the form
            }
        });
    }

    var handleValidation3 = function() {
        var form3       = $('#role_form');
        var error2      = $('.alert-danger', form3);
        var success2    = $('.alert-success', form3);

        form3.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                password:{
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    equalTo: '#password'
                },
                "permissions[]": { 
                    required: true, 
                    minlength: 1 
                } 
            },
            messages: {
                password: {
                    required:  'Please provide a password',
                    minlength: 'Your password must be at least 6 characters long'
                },
                confirm_password: {
                    required:  'Please provide a password',
                    minlength: 'Your password must be at least 6 characters long',
                    equalTo:   'Please enter the same password as above'
                },
                "permissions[]": "Please select at least one section of permissions."
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                form[0].submit(); // submit the form
            }
        });
    }

    var handleValidation4 = function() {
        var form4       = $('#edit_role_form');
        var error2      = $('.alert-danger', form4);
        var success2    = $('.alert-success', form4);

        form4.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                password:{
                    required: false,
                    minlength: 6
                },
                confirm_password: {
                    required: false,
                    equalTo: '#password'
                },
                "permissions[]": { 
                    required: true, 
                    minlength: 1 
                } 
            },
            messages: {
                password: {
                    minlength: 'Your password must be at least 6 characters long'
                },
                confirm_password: {
                    minlength: 'Your password must be at least 6 characters long',
                    equalTo:   'Please enter the same password as above'
                },
                "permissions[]": "Please select at least one section of permissions."
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                form[0].submit(); // submit the form
            }
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation2();
            handleValidation3();
            handleValidation4();
        }
    };
}();

$(document).ready(function() {
    FormValidation.init();

    if( $('select[name="category"] option:selected').val() != 0 ){
        $('input[name="driverlic"]').removeClass('required');
        $('input[name="license_category"]').removeClass('required');
        $('input[name="license_authority"]').removeClass('required');
        $('input[name="license_expiry"]').removeClass('required');
    }

    $('select[name="category"]').on('change',function() {

        if($(this).val() != 0){
            if ( $('input[name="license_expiry"]').hasClass( "required" ) ){
                $('input[class*=toggleValidation]').each(function(){
                    $(this).removeClass('required');
                    $(this).closest('.form-group').removeClass('has-error');
                    var icon = $(this).parent('.input-icon').children('i');
                    icon.removeClass('fa-warning');
                });
            } 
        }

        if($(this).val() == 0){
            if ( $('input[name="license_expiry"]').hasClass( "required" ) == false ){
                $('input[class*=toggleValidation]').each(function(){
                    $(this).addClass('required');
                });
            } 
        }

    });
});
