function getEvents(){

	return [
		{
			id : 'E01',
			title : 'Meeting with BA',
			start : '02-10-2018 10:30:00',
			end : '02-10-2018 11:00:00',
			backgroundColor: '#443322',
			textColor : '#FFF'
		},
		{
			id : 'E02',
			title : 'Lunch',
			start : '3-10-2018 12:45:00',
			end : '3-10-2018 13:30:00',
			backgroundColor: '#12CA6B',
			textColor : '#FFF'
		},
		{
			id : 'E03',
			title : 'Customer Appointment',
			start : '4-10-2018 09:00:00',
			end : '4-10-2018 09:30:00',
			backgroundColor: '#34BB22',
			textColor : '#FFF'
		},
		{
			id : 'E04',
			title : 'Buddy Time. Proactive contact. Long name',
			start : '4-10-2018 11:00:00',
			end : '4-10-2018 12:30:00',
			backgroundColor: '#AA3322',
			textColor : '#FFF'
		},
		{
			id : 'E05',
			title : 'Proactive Contact',
			start : '5-11-2018 10:30:00',
			end : '5-11-2018 11:15:00',
			backgroundColor: '#443322',
			textColor : '#FFF'
		}
	];
}