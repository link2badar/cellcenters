<?php

Route::group(['middlewareGroups' => ['web']], function() {
    Route::auth();
    
    /* Token Validation */
	Route::post('/verifyToken/validate-token' , 'LoginController@validateToken');


    /* Appointment  Routes  */
    Route::get('/calendar/{id?}'                  ,   'AppointmentController@index');
    Route::get('/appointment/add/{id?}'           ,   'AppointmentController@add');
    Route::post('/appointment/add'          ,   'AppointmentController@add');
    Route::get('/appointment/reschedule/{id}'  ,   'AppointmentController@reschedule');
    Route::post('/appointment/update_reschedule/'  ,   'AppointmentController@update_reschedule');
    Route::get('/filter/conditions/{id}'  ,   'AppointmentController@filter_conditions');
    Route::any('/appointment/view-detail/{id}'  ,   'AppointmentController@viewDetail');
    Route::get('/appointment/cancel/{id}'  ,   'AppointmentController@cancel_appointment');
    Route::post('/appointment/change_status/'  ,   'AppointmentController@change_status');
    Route::get('/appointment/filtertime/{id}/{name}'   ,   'AppointmentController@filterTime');
    Route::get('/appointments/cancel/{id?}'   ,   'AppointmentController@cancel_appfilter');


    /* Physicians  Routes  */
    Route::get('/locations'                 ,   'LocationController@index');
    Route::get('/location/add'              ,   'LocationController@add');
    Route::post('/location/add'             ,   'LocationController@add');
    Route::get('location/update/{id?}'      ,   'LocationController@update');
    Route::post('location/update/{id?}'     ,   'LocationController@update');
    Route::get('location/delete/{id}'       ,   'LocationController@delete');
    Route::get('/location/check-timing'     ,   'LocationController@checkTiming');
    Route::get('/update/sessionlocation/{id}'     ,   'LocationController@sessionlocation');



    /* Seminar Routes  */
    Route::get('/seminars'              ,   'SeminarController@index');
    Route::get('/seminar/add'           ,   'SeminarController@add');
    Route::post('/seminar/add'          ,   [
                                                'as'    =>  'clinicadd',
                                                'uses'  =>  'SeminarController@add'
                                            ]);
    Route::get('/seminar/detail/{id?}'  ,   'SeminarController@viewDetail');
    Route::get('seminar/update/{id}'    ,   'SeminarController@update');
    Route::post('seminar/update/'       ,   [
                                                'as'    =>  'updateclinic',
                                                'uses'  =>  'SeminarController@update'
                                             ]);
    Route::get('seminar/delete/{id}'    ,   'SeminarController@delete');

    Route::get('seminar/admin-modal'    ,   'SeminarController@seminarAdminModal');
    Route::post('seminar/add-admin'     ,   'SeminarController@addSeminarAdmin');
    Route::post('seminar/update-admin'  ,   'SeminarController@updateSeminarAdmin');
    Route::get('seminar/delete-admin/{id?}','SeminarController@deleteSeminarAdmin');

    Route::get('seminar/add-registrant/{id?}'   ,   'SeminarController@seminarAddRegistrant');
    Route::post('seminar/add-registrant/'       ,   [
                                                        'as'    =>  'RegistrantAdd',
                                                        'uses'  =>  'SeminarController@seminarAddRegistrant'
                                                    ]);
    Route::get('seminar/delete-registrant/{id}' ,   'SeminarController@deleteSeminarRegistrant');
    Route::post('seminar/registrant-attendance' ,   'SeminarController@markRegistrantAttendance');

    Route::get('seminar/edit-registrant/{id}'   ,   'SeminarController@viewEditRegistrant');
    Route::post('seminar/update-registrant'     ,   'SeminarController@updateRegistrant');
    Route::get('/seminar/reschedule'            ,   'SeminarController@reschedule');
    Route::post('/seminar/update-schedule/{id}' ,   'SeminarController@updateSchedule');
    
    Route::get('patient/forms/{id}'             ,   'SeminarController@viewPatientFroms');

    Route::get('seminar/create-note'            ,   'SeminarController@seminarCreateNoteModal');
    Route::post('seminar/create-note'           ,   'SeminarController@seminarCreateNote');
    Route::get('seminar/full-note'              ,   'SeminarController@seminarReadFullNote');


    /* Clinics Routes  */
    Route::get('/clinics'               ,   'ClinicController@index');
    Route::get('/clinic/add'            ,   'ClinicController@add');
    Route::post('/clinic/add'           ,   [
                                                'as'    =>  'clinicadd',
                                                'uses'  =>  'ClinicController@add'
                                            ]);
    Route::get('clinic/update/{id?}'    ,   'clinicController@update');
    Route::post('clinic/update/{id?}'   ,    [
                                                'as'    =>  'updateclinic',
                                                'uses'  =>  'clinicController@update'
                                        ]   );
    Route::get('clinic/delete/{id}'     ,   'clinicController@delete');


    /* Physicians  Routes  */
    Route::get('/physicians'                ,   'PhysicianController@index');
    Route::get('/physician/add'             ,   'PhysicianController@add');
    Route::post('/physician/add'            ,   [
                                                    'as'    =>  'clinicadd',
                                                    'uses'  =>  'PhysicianController@add'
                                                ]);
    Route::get('physician/update/{id?}'     ,   'PhysicianController@update');
    Route::post('physician/update/{id?}'    ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'PhysicianController@update'
                                                ]);
    Route::get('physician/delete/{id}'      ,   'PhysicianController@delete');


    /* Managers Routes  */
    Route::get('/managers'                 ,   'ManagerController@index');
    Route::get('/manager/add'              ,   'ManagerController@add');
    Route::post('/manager/add'             ,   [
                                                     'as'    =>  'clinicadd',
                                                    'uses'  =>  'ManagerController@add'
                                                ]);
    Route::get('manager/update/{id?}'      ,   'ManagerController@update');
    Route::post('manager/update/{id?}'     ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'ManagerController@update'
                                                ]);
    Route::get('manager/delete/{id}'       ,   'ManagerController@delete');

    /* PATIENT ROUTES */
    Route::get("/seminar-users"             ,   'SeminarUserController@index');
    Route::get('/seminar-user/add'          ,   'SeminarUserController@add');
    Route::post('/seminar-user/add'         ,   [
                                                    'as' => 'patientadd',
                                                    'uses' => 'SeminarUserController@add'
                                                ]);
    Route::get('seminar-user/delete/{id}'   ,   'SeminarUserController@delete');
    Route::get('seminar-user/update/{id?}'  ,   'SeminarUserController@update');
    Route::post('seminar-user/update/{id?}' ,   [
                                                    'as' => 'updatepatient',
                                                    'uses' => 'SeminarUserController@update'
                                                ]); 

    /* Staff Routes*/
    Route::get('/staff'                     ,   'StaffController@index');
    Route::get('/staff/add'                 ,   'StaffController@add');
    Route::post('/staff/add'                ,   [
                                                    'as'    =>  'clinicadd',
                                                    'uses'  =>  'StaffController@add'
                                                ]);
    Route::get('staff/update/{id?}'         ,   'StaffController@update');
    Route::post('staff/update/{id?}'        ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'StaffController@update'
                                                ]);
    Route::get('staff/delete/{id}'          ,   'StaffController@delete');

    /* PATIENT ROUTES */
    Route::get("/patients"                  ,   'PatientController@index');
    Route::get('/patient/add/{id?}'         ,   'PatientController@add');
    Route::post('/patient/add/{id?}'        ,   [
                                                'as'    =>  'patientadd',
                                                'uses'  =>  'PatientController@add'
                                            ]);
    Route::get('patient/delete/{id}'    ,   'PatientController@delete');
    Route::get('patient/update/{id?}'   ,   'PatientController@update');
    Route::post('patient/update/{id?}'  ,   [
                                                'as'    =>  'updatepatient',
                                                'uses'  =>  'PatientController@update'
                                        ]); 
    Route::post('/patient/filter-provider', 'PatientController@filterProvider');

    //edit patient -> log
    Route::get('patient/log/{id}'               ,   'PatientController@completedForm');
    Route::get('patient/seminar-notes/{id}'     ,   'PatientController@viewSeminarNotes');
    Route::get('patient/consultation-notes/{id}',   'PatientController@viewConsultationNotes');
    Route::get('patient/exam-notes/{id}'        ,   'PatientController@viewExamNotes');
    Route::get('patient/treatment-notes/{id}'   ,   'PatientController@viewTreatmentNotes');
    Route::get('patient/appointments/{id}'      ,   'PatientController@viewAppointments');
    Route::get('patient/invoices/{id}'          ,   'PatientController@viewInvoices');
    Route::get('patient/seminars/{id}'          ,   'PatientController@viewSeminars');
    Route::get('patient/invoice/create/{id}'    ,   'PatientController@create_invoice');





    //CONTACT ROUTES
    Route::get("/contacts"              ,   'ContactController@index');
    Route::get('/contact/add/{id?}'           ,   'ContactController@add');
    Route::post('/contact/add'          ,   [
                                            'as' => 'patientadd',
                                            'uses' => 'ContactController@add'
                                        ]);
    Route::get('/contact/delete/{id}'   ,   'ContactController@delete');
    Route::get('/contact/update/{id?}'  ,   'ContactController@update');
    Route::post('/contact/update/{id?}' ,   [
                                                'as' => 'updatepatient',
                                                'uses' => 'ContactController@update'
                                            ]);  
    
    /* DASHBAORD ROUTES */
    Route::get('/'              ,   'DashboardController@index');
    Route::get('/dashboard'     ,   'DashboardController@index');
    Route::get('/account'     ,   'DashboardController@account_setting');
    Route::post('/account'     ,   'DashboardController@account_setting');


    // TEMPLATES
    Route::get('/templates'                ,    'TemplateController@index');
    Route::any('/template/add'             ,    'TemplateController@add');
    Route::any('/template/update/{id}'     ,    'TemplateController@update');
    Route::get('/template/delete/{id}'     ,    'TemplateController@delete');


    // PRIMARY CONCERN
    Route::get('/primary-concern'                   ,    'PrimaryController@index');
    Route::any('/primary-concern/add'               ,    'PrimaryController@add');
    Route::any('/primary-concern/update/{id}'       ,    'PrimaryController@update');
    Route::get('/primary-concern/delete/{id}'       ,    'PrimaryController@delete');

    // SPEAKERS
    Route::get('/speakers'                  ,    'SpeakerController@index');
    Route::any('/speaker/add'               ,    'SpeakerController@add');
    Route::any('/speaker/update/{id}'       ,    'SpeakerController@update');
    Route::get('/speaker/delete/{id}'       ,    'SpeakerController@delete');



    

});

