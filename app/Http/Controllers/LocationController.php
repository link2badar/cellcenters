<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Locations;
use App\Models\LocationTiming;
use Auth,URL,Session,Redirect;
use DB;

class LocationController  extends Controller{

    private $plural     =  "Locations";    
	private $module     =  "Location";
    private $view       =  "locations/";  
    private $locAuth;  

    public function __construct()	{
        $this->contants     =   config('constants.appmanangement');
        $locAuth            =   explode(",", Auth::user()->location_id);
        $this->locAuth      =   $locAuth[0];
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        if (Auth::user()->role == 0) {
            $data['list']   =   Locations::all();            
        }else{
            $locAuthArr     =   explode(",", Auth::user()->location_id);
            $data['list']   =   Locations::whereIn('loc_id', $locAuthArr)->get();
        }
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->has('location_name')){
            $data       =   $request->all();
            if ($request->hasFile('location_image')) {
                $file            =  $request->file('location_image');
                $destinationPath =  base_path() . '/public/locations_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['location_image']);
            $location      =    new Locations();
            $location_id   =    $location->create($data)->loc_id;
            if(!empty($data['time'])){
                $location_timing    =   array();
                $i                  =   0; 
                foreach ($data['time'] as $day => $timing) {
                    $location_timing[$i]['location']    =   $location_id;  
                    $location_timing[$i]['day_id']      =   $day;
                    $location_timing[$i]['open_time']   =   strtotime($timing['open']);
                    $location_timing[$i]['close_time']  =   strtotime($timing['close']);
                    $i++;
                }
                if(!empty($location_timing))
                    LocationTiming::insert($location_timing);
            }
            return redirect('locations')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('clinics') =>  ucfirst($this->plural)." List" , '#' =>'Add New '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->has('location_name')){
            $data       =   $request->all();
            if ($request->hasFile('location_image')) {
                $file            =  $request->file('location_image');
                $destinationPath =  base_path() . '/public/locations_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['location_image']);
            $location   =   Locations::find($data['loc_id']);
            $location->update($data);
            if(!empty($data['time'])){
                $location_timing    =   array();
                $i                  =   0; 
                foreach ($data['time'] as $day => $timing) {
                    $location_timing[$i]['location']    =   $data['loc_id'];  
                    $location_timing[$i]['day_id']      =   $day;
                    $location_timing[$i]['open_time']   =   strtotime($timing['open']);
                    $location_timing[$i]['close_time']  =   strtotime($timing['close']);
                    $i++;
                }
                if(!empty($location_timing)){
                    LocationTiming::where('location',$data['loc_id'])->delete();
                    LocationTiming::insert($location_timing);
                }
            }
            return redirect('location/update/'.$data['loc_id'])->with('message', $this->module.' has been sucessfully updted !');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('locations') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['location']   =   Locations::find($id)->toArray();
        $data['timings']    =   LocationTiming::where('location',$id)->get()->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $location   =  Locations::find($id);
        $location->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }

    public function sessionlocation($id) {
        $authLocationArray      =   Auth::user()->location_id;
        $authLocationArray      =   explode(",", $authLocationArray);
        $zeroIndexLoc           =   $this->locAuth;
        $authLocationArray[0]   =   $authLocationArray[$id];
        $authLocationArray[$id] =   $zeroIndexLoc;
        $user                   =   Auth::user();
        $user->location_id      =   implode(",", $authLocationArray);
        $user->save();
        echo Auth::user()->location_id;
    }

    public function checkTiming(Request $request) {
        $authLoc        =          explode(",",Auth::user()->location_id);
        $time           =          strtotime($request->get('time'));
        $day            =          $request->get('day');
        if ($day == 0) {
            $day = 7;
        }
        $date           =          $request->get('date');
        $data['yes']    =          LocationTiming::where(array('location' => $authLoc[0], 'day_id' => $day))->where('open_time', '<=' , $time)->where('close_time', '>', $time)->count();
        if ($data['yes']>0) {
            echo 1;
        }
        else{
            echo 0;
        }
    }
}
