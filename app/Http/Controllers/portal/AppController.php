<?php

namespace App\Http\Controllers\appmanangement;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\appmanangement\Stores;
use App\Models\Utility;
use App\Classes\ImageUtility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class AppController extends Controller
{
	private $storege = "mobileapp/";
	private $module = "";
	public function __construct()	{
		$this->module = config('constants.appmanangement.module');
	}
	
	
    public function index(Request $request)	{
		$data = array(
            "page_title"   => "App Management | View All Apps",
            "page_heading" => "App Management | View All Apps",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );

        $query  =   Apps::where('parent_id','=',0);
        $data['list']   =   $query->paginate(50)->toArray();
        
        //echo "<pre>"; print_r($data); die();
		return view($this->module.'.app.view', $data);
	}
    public function add(Request $request)   {
        if($request->input('name')){
           
            
            $is_save                =   Apps::where('name','=',
                                                $request->name)
                                                ->where('parent_id','=',0)
                                                ->count();
            if($is_save > 0)    {
                return redirect($this->module.'/app/add')->with('message', 'App with this name alrady exist.');
            }
            $data = $request->all();
            $image = new ImageUtility($this->storege);
            $data['app_img'] = '';
            if($request->file('userfile')){
                $data['app_img'] = $image->uploadImage($this,$request,'userfile');
                unset($data['userfile']);
            }
            //echo "<pre>"; print_r($data); die();
            $app = new Apps();
            $app->create($data);
            return redirect($this->module.'/app')->with('message', 'App is added sucessfully!');
        }

        $data = array(
            "page_title"   => "App Management | Add App",
            "page_heading" => "App Management | Add App",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );
        $data['stores'] =   Stores::get()->toArray();
        return view($this->module.'.app.addview', $data);
    }

    public function delete($id) {
        $app               = Apps::find($id)->toArray();
        $unlink = storage_path($this->storege).$app['app_img'];
        if(file_exists($unlink)){
            unlink($unlink);
        }
        Apps::destroy($id);
        $response = array('flag'=>true,'msg'=>'App has been deleted.');
        echo json_encode($response); return;
    }
    public function update($id = NULL,Request $request) {
        //print_r($request->all()); die();
        if($request->input('name')){
            
            
            $is_save                =   Apps::where('name','=',
                                                $request->name)
                                                ->where('app_id','!=',
                                                $id)
                                                ->count();
            if($is_save > 0)    {
                echo 'already saved'; die();
                //return redirect($this->module.'/category/update/'.$id)->with('message', 'Login Failed');
            }
            $data = $request->all();
            $image = new ImageUtility($this->storege);
            if($request->file('userfile')){
                $app               = Apps::find($id)->toArray();
                $unlink = storage_path($this->storege).$app['app_img'];
                if(file_exists($unlink)){
                    unlink($unlink);
                }
                $data['app_img'] = $image->uploadImage($this,$request,'userfile');
                unset($data['userfile']);
            }
            $app               = Apps::find($id);
            $app->update($data);
        }
        $data = array(
            "page_title"   => "Twitter Assistance | Edit App",
            "page_heading" => "Twitter Assistance | Edit App",
             "module" => $this->module,
             "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => "twitterassistant"),
        );
        $data['list']   =    Apps::get();
        $data['data_row']    =   Apps::find($id);
        $data['stores'] =   Stores::get()->toArray();
        return view($this->module.'.app.editview', $data);
    }
	
}
