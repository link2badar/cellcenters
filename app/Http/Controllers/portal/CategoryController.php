<?php

namespace App\Http\Controllers\appmanangement;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\appmanangement\Stores;
use App\Models\Utility;
use App\Classes\ImageUtility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class CategoryController extends Controller
{
	private $storege = "mobileapp/";
	private $module = "";
    private $thumbs = [200,400,800];
	public function __construct()	{
		$this->module = config('constants.appmanangement.module');
	}
	
	
    public function index(Request $request,$id)	{
                
		$data = array(
            "page_title"   => "App Management | View All Category",
            "page_heading" => "App Management | View All Category",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );

        $query  =   Apps::where('parent_id','=',$id);
        $data['list']   =   $query->paginate(50)->toArray();
        $data['parent_id']   =   $id;
        
		return view($this->module.'.category.view', $data);
	}

    public function add(Request $request,$parent_id)   {
        if($request->input('name')){
           
            
            $is_save                =   Apps::where('name','=',
                                                $request->name)
                                                ->where('parent_id','=',$parent_id)
                                                ->count();
            if($is_save > 0)    {
                return redirect($this->module.'/category/add/'.$parent_id)->with('message', 'Category with this name alrady exist.');
            }

            $data = $request->all();

            $image = new ImageUtility($this->storege);
            $data['app_img'] = '';
            $data['parent_id'] = (int)$parent_id;
            if($request->file('userfile')){
                $data['app_img'] = $image->uploadImage($this,$request,'userfile',1);
                foreach ($this->thumbs as $key => $value) {
                    $image->createThumbnail($data['app_img'],$value,$value,storage_path($this->storege),storage_path($this->storege));
                }
                
                unset($data['userfile']);
            }
            //echo "<pre>"; print_r($data); die();
            $app = new Apps();
            $app->create($data);
            return redirect($this->module.'/category/'.$parent_id)->with('message', 'Category is added sucessfully!');
        }

        $data = array(
            "page_title"   => "App Management | Add Category",
            "page_heading" => "App Management | Add Category",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );
        $data['parent_id'] =   $parent_id;
        
        return view($this->module.'.category.addview', $data);
    }

    public function delete($id) {
        $app               = Apps::find($id)->toArray();
        $unlink = storage_path($this->storege).$app['app_img'];
        foreach ($this->thumbs as $key => $value) {
            $value = storage_path($this->storege.$value.'-'.$app['app_img']);
         
            if(file_exists($value)){
                @unlink($value);
            }
            $value = storage_path($this->storege.$app['app_img']);
            if(file_exists($value)){
                @unlink($value);
            }
            $value = storage_path();
        }        
        Apps::destroy($id);
        $response = array('flag'=>true,'msg'=>'Category has been deleted.');
        echo json_encode($response); return;
    }
    public function update($id = NULL,Request $request) {
         $app  = Apps::find($id)->toArray();
        //print_r($request->all()); die();
        if($request->input('name')){
            
            
            $is_save                =   Apps::where('name','=',$request->name)
                                                ->where('app_id','!=',$id)
                                                ->count();
            if($is_save > 0)    {
       
                return redirect($this->module.'/category/update/'.$id)->with('message', 'This category already exist');
            }
            $data = $request->all();
            $image = new ImageUtility($this->storege);
            if($request->file('userfile')){
                
                $data['app_img'] = $image->uploadImage($this,$request,'userfile',1);
                foreach ($this->thumbs as $key => $value) {
                    $image->createThumbnail($data['app_img'],$value,$value,storage_path($this->storege),storage_path($this->storege));
                }
                foreach ($this->thumbs as $key => $value) {
                    $value = storage_path($this->storege.$value.'-'.$app['app_img']);
                 
                    if(file_exists($value)){
                        @unlink($value);
                    }
                    $value = storage_path($this->storege.$app['app_img']);
                    if(file_exists($value)){
                        @unlink($value);
                    }
                    $value = storage_path();
                }  

            }
            $app               = Apps::find($id);
            $app->update($data);
            return redirect($this->module.'/category/'.$app['parent_id'])->with('message', 'Category is updated sucessfully!');
        }
        $data = array(
            "page_title"   => "Twitter Assistance | Edit App",
            "page_heading" => "Twitter Assistance | Edit App",
             "module" => $this->module,
             "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => "category"),
        );
        $data['list']       =   Apps::get();
        $data['data_row']   =   Apps::find($id);
        $data['stores']     =   Stores::get()->toArray();
        return view($this->module.'.category.editview', $data);
    }
	
}
