<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\Patients;
use App\Models\Contacts;
use App\Models\Locations;
use App\Models\Utility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class ContactController extends Controller
{
	private $storege = "media/";
    

	private $module = "";
    private $contants;
    private $locAuth;
	public function __construct()	{
		$this->module = config('constants.appmanangement.module');
        $this->contants = config('constants.appmanangement');
        $locAuth            =   explode(",", Auth::user()->location_id);
        $this->locAuth      =   $locAuth[0];
	}
	
	
    public function index(Request $request)	{
		$data = array(
            "page_title"   => "Contact Management | View All Contact",
            "page_heading" => "Contact Management | View All Contact",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home",'#'=>"Contact"),
        );
        if (Auth::user()->role == 0 || Auth::user()->role == 7 ) {
            $data['list']   =   Contacts::select('contacts.*', 'locations.location_name')->join('locations', 'locations.loc_id', '=', 'contacts.location_id')->get();
        }else{
            $locArr         =   explode(",", Auth::user()->location_id);
            $data['list']   =   Contacts::select('contacts.*', 'locations.location_name')->join('locations', 'locations.loc_id', '=', 'contacts.location_id')->whereIn('location_id', $locArr)->get();
        }
		return view('contact.view', $data);
	}
    public function add(Request $request , $id = null)   {
        if($request->input('first_name')){
            $data = $request->all();
            unset($data['_token']);
            $Contacts         = new Contacts;
            $Contacts->insert($data);
            return redirect('contacts')->with('message', 'Contact sucessfully added');
        }
        $data = array(
            "page_title"   => "Add Contact",
            "page_heading" => "Add Contact",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", url('contacts') => "Contact List",'#'=>'Add Contact'),
        );
        if(!empty($id)){
            $data['data_row']   =   Patients::find($id)->toArray();
        }
        $data['clinics']        =   Locations::get(['loc_id', 'location_name'])->toArray();

        return view('contact.addview', $data);
    }

    public function delete($id) {
        $Stores   = new Contacts;
        $Stores->find($id);
        Contacts::destroy($id);
        $response = array('flag'=>true,'msg'=>'Contacts has been deleted.');
        echo json_encode($response); return;
    }
    public function update(Request $request,$id = NULL) {
        if($request->input('first_name')){
            $data = $request->all();
            if(isset($data['_token'])) unset($data['_token']); 
            if($request->input('haboutus') != 6){
              $data['refrel_name']   =   '';
            }       
            $store               = Contacts::find($id);
            $store->update($data);
            return redirect('contacts')->with('message', 'Contact sucessfully added');          
        }
        $data = array(
            "page_title"   => "Edit Contact",
            "page_heading" => "Edit Contact",
            "breadcrumbs"  => array("dashboard" => "Home", url('contacts') => "Contacts List",'#'=>'Update Contact'),
        );
        $data['data_row']   =   Contacts::find($id)->toArray();
        $data['clinics']    =   Locations::get(['loc_id', 'location_name'])->toArray();
        return view('contact.editview', $data);
    }
	
}
