<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Staff;
use App\Models\Locations;
use Auth,URL,Session,Redirect,Validator;
Use App\User;
Use DB;

class StaffController   extends Controller{

    private $plural     =  "Staff Members";    
    private $module     =  "Staff Member";
    private $view       =  "staff/";     
    private $locAuth;  
    private $id;
    
    public function __construct()   {
        $this->contants     =   config('constants.appmanangement');
        $locAuth            =   explode(",", Auth::user()->location_id);
        $this->locAuth      =   $locAuth[0];
        $this->id           =   Auth::user()->id;   
    }
    
    public function index(Request $request) {
        $data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );

        if (Auth::user()->role == 0) {
            $data['staff'] = \DB::select("SELECT  u.name,u.email, u.id, GROUP_CONCAT(loc.location_name) AS location_name FROM users u INNER JOIN locations loc ON FIND_IN_SET(loc.loc_id, u.location_id) > 0  WHERE(u.role = 3) GROUP BY u.id");
        }else{
                $data['staff'] = \DB::select("SELECT  u.name,u.email, u.id, GROUP_CONCAT(loc.location_name) AS location_name FROM users u INNER JOIN locations loc ON FIND_IN_SET($this->locAuth, u.location_id) > 0  WHERE(u.role = 3) GROUP BY u.id");
            }

        if(Auth::user()->role == 3){
               $data['staff'] = \DB::select("SELECT  u.name,u.email, u.id, GROUP_CONCAT(loc.location_name) AS location_name FROM users u INNER JOIN locations loc ON FIND_IN_SET( $this->locAuth, u.location_id) > 0  WHERE(u.role = 3 and u.id != ".$this->id.") GROUP BY u.id");
        }
        return view($this->view.'.list',$data);
    }

    public function add(Request $request) {
        if($request->has('name')){
            $data       =   $request->all();
            $validator  =   Validator::make($request->all(),[
                'email'         =>  'unique:users',
                'password'      =>  'required|confirmed|min:6',
                'password'      =>  'required|confirmed|min:6',
                'positions'     =>  'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            $data['positions']  =   implode(',',$data['positions']);
            unset($data['_token'],$data['password_confirmation']);
            $data['password']   =   bcrypt($data['password']);
            $data['role']       =   3;
            $data['added_by']   =   $this->id;
            $data['location_id']=   implode(',',$data['location_id']);
            $staff     =   new User();
            $staff->insert($data);
            return redirect('staff')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add ". $this->module,
            "page_heading"  =>  "Add ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('staff') =>  ucfirst($this->plural)." List" , '#' =>'Add '.ucfirst($this->module)),
        );
        if (Auth::user()->role == 0) {
            $data['locations']   =   Locations::all();            
        }else{
            $locAuthArr     =   explode(",", Auth::user()->location_id);
            $data['locations']       =   Locations::whereIn('loc_id', $locAuthArr)->get();
        }

        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'name'     =>   'required',
                "email"    =>   "required|unique:users,email,$id,id",
                'password' =>   'confirmed|min:6',
                'positions'=>   'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $data['positions']   =    implode(",", $data['positions']);
            $data['location_id'] =   implode(',',$data['location_id']);
            $staff  =   User::find($id);
            if ($data['password'] == '') {
                $data['password'] = $staff['password'];
            }else{
                 $data['password'] = bcrypt($data['password']);
            }

            $staff->update($data);
            return redirect('staff')->with('message', 'Staff sucessfully Updated');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('staff') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['locations']   =   Locations::all();
        $data['staff']      =   User::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $manager   =  User::find($id);
        $manager->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
