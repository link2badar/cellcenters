<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Locations;
use Auth,URL,Session,Redirect,Validator;
Use App\User;
Use DB;

class SpeakerController   extends Controller{

    private $plural     =  "Members";    
    private $module     =  "Member";
    private $view       =  "speaker/";     
    private $locAuth;  
    private $id;
    
    public function __construct()   {
        $this->id           =   Auth::user()->id;   
    }
    
    public function index(Request $request) {
        $data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );

        $data['speaker']       =   User::where('role', '6')->orWhere('role', '7')->get();
        return view($this->view.'.list',$data);
    }

    public function add(Request $request) {
        if($request->has('name')){
            $data       =   $request->all();
            $validator  =   Validator::make($request->all(),[
                "email"    =>  "required|unique:users",
                'password'      =>  'required|confirmed|min:6',
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $data['password']   =   bcrypt($data['password']);
            $speaker            =   new User();
            $speaker->insert($data);
            return redirect('speakers')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add ". $this->module,
            "page_heading"  =>  "Add ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('speakers') =>  ucfirst($this->plural)." List" , '#' =>'Add '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'name'     =>   'required',
                "email"    =>   "required|unique:users,email,$id,id",
                'password' =>   'confirmed|min:6'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $speaker  =   User::find($id);
            if ($data['password'] == '') {
                $data['password'] = $speaker['password'];
            }else{
                 $data['password'] = bcrypt($data['password']);
            }

            $speaker->update($data);
            return redirect('speakers')->with('message', 'Speaker sucessfully Updated');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('speakers') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['speaker']      =   User::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $manager   =  User::find($id);
        $manager->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
