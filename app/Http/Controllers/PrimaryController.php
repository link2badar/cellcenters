<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\Locations;
use App\Models\Primary;
use App\Models\Utility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class PrimaryController extends Controller
{
	private $storege   =   "primary/";
	private $module    =   "primary";
    private $view      =   "primary/";  
    private $id;

	public function __construct()	{
        $this->id      =   Auth::user()->id;
	}
	
	
    public function index(Request $request)	{
		$data = array(
            "page_title"   => "Primary Concern Management | View All Primary Concern",
            "page_heading" => "Primary Concern Management | View All Primary Concern",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", '#'=>"Primary Concerns"),
        );
        if (Auth::user()->role == 0) {
            $data['list']   =   Primary::all();         
        }
		return view($this->view.'list', $data);
	}
    public function add(Request $request)   {
        if($request->input('title')){
            $data = $request->all();
            unset($data['_token']);
            $data['added_by']       =   $this->id;
            $primaryConcern         =   new Primary;
            $primaryConcern->insert($data);
            return redirect('primary-concern')->with('message', 'Primary Concern sucessfully added');
        }
        $data = array(
            "page_title"   => "Add Primary Concern",
            "page_heading" => "Add Primary Concern",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", url('primary-concern') => "Primary Concern List",'#'=>'Add Primary Concern'),
        );

        return view($this->view.'addview', $data);
    }

    public function delete($id) {
        $Stores   = new Primary;
        $Stores->find($id);
        Primary::destroy($id);
        $response = array('flag'=>true,'msg'=>'Primary Concern has been deleted.');
        echo json_encode($response); return;
    }
    public function update(Request $request,$id = NULL) {
        if($request->input('title')){
            $data = $request->all();
            if(isset($data['_token'])) unset($data['_token']);        
            $store               = Primary::find($id);
            $store->update($data);
            return redirect('primary-concern')->with('message', 'Primary Concern sucessfully added');          
        }
        $data = array(
            "page_title"   => "Edit Primary Concern",
            "page_heading" => "Edit Primary Concern",
            "breadcrumbs"  => array("dashboard" => "Home", url('primary-concern') => "Primary Concern List",'#'=>'Update Primary Concern'),
        );
        $data['data_row']   =   Primary::find($id)->toArray();
        return view($this->view.'editview', $data);
    }
	
}
