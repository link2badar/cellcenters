<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SeminarUsers;
use Auth,URL,Session,Redirect,DB,Validator;

class SeminarUserController  extends Controller{

	private $storage    =  "media/";
    private $plural     =  "Seminar Users";    
	private $module     =  "Seminar User";
    private $view       =  "seminar-users/";     
	
    public function __construct()	{
        $this->contants   =   config('constants.appmanangement');
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "storage"       =>  $this->storage,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $data['list']   =   SeminarUsers::all()->toArray();
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'  =>  'required',
                'last_name'   =>  'required',
                "email"       =>  "required|unique:seminar_users",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('user_image')) {
                $file            =  $request->file('user_image');
                $destinationPath =  base_path() . '/public/users_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['user_image']);
            $clinic     =   new SeminarUsers();
            $clinic->create($data);
            return redirect('seminar-users')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminar-users') =>  ucfirst($this->plural)." List" , '#' =>'Add New '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
       if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'  =>  'required',
                'last_name'   =>  'required',
                "email"       =>  "required|unique:seminar_users,email,$id,seminar_user_id",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('user_image')) {
                $file            =  $request->file('user_image');
                $destinationPath =  base_path() . '/public/users_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['user_image']);
            $user  =   SeminarUsers::find($id);
            $user->update($data);
            return redirect('seminar-user/update/'.$id)->with('message', $this->module.' has been sucessfully updated !');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminar-users') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['user']     =   SeminarUsers::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $user   =  SeminarUsers::find($id);
        $user->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
