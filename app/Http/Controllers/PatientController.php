<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Patients;
use App\Models\Clinics;
use App\Models\Locations;
use App\Models\Managers;
use App\Models\Physicians;
use App\Models\Primary;
use App\Models\Appointments;
use App\User;
use Auth,URL,Session,Redirect,Validator,DB;

class PatientController extends Controller{
	private $module    =   "Patient";
    private $view      =   "patients";   
    private $plural    =   "Patient";   
    private $locAuth;
    private $id;

	public function __construct()	{
        $locAuthArr    =    explode(",", Auth::user()->location_id);
        $this->locAuth =    $locAuthArr[0];
        $this->id      =    Auth::user()->id;
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  "Patient Management | View All Patient",
            "page_heading"  =>  "Patient Management | View All Patient",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home",'#'=>"Patient"),
        );

        $query              =   DB::table('patients');
        if($request->has('first_name'))
            $query->where('first_name' , $request->get('first_name'));
        if($request->has('last_name'))
            $query->where('last_name' , $request->get('last_name'));
        if($request->has('email'))
            $query->where('email' , $request->get('email'));
        if($request->has('gender'))
            $query->where('gender' , $request->get('gender'));
        if($request->has('preferred_clinic'))
            $query->where('preferred_clinic' , $request->get('preferred_clinic'));
        if($request->has('phone_number'))
            $query->where('phone_number' , $request->get('phone_number'));
        if($request->has('primary_physician'))
            $query->where('primary_physician' , $request->get('primary_physician'));

        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
            $query->join('locations', 'locations.loc_id', '=', 'patients.preferred_clinic')->select('patients.*', 'locations.location_name')->get();
            $data['primaryConcern']     =   Primary::all();

        }else{
            $query->join('locations', 'locations.loc_id', '=', 'patients.preferred_clinic')->select('patients.*', 'locations.location_name')->where(array('preferred_clinic' => $this->locAuth))->get();
            $data['primaryConcern']     =   Primary::all();

        }

        $list           =   $query->get();
        $data['list']   =   collect($list)->map(function($x){ return (array)$x; })->toArray();
        $userClinics            =   explode(",",Auth::user()->location_id);
        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
            $data['clinics']        =   Locations::all();
        }else{
            $data['clinics']        =   Locations::whereIn('loc_id', $userClinics)->get();
        }
        $data['physicians']     =   User::where('role', '2')->get();
		return view($this->view.'.list', $data);
	}

    public function add(Request $request, $id = null)   {
        if($request->method() == 'POST'){ 
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'    =>  'required',
                'last_name'     =>  'required',
                'email'         =>  'unique:patients|required',
                'haboutus'      =>  'required',
                'phone_number'  =>  'required',
                'preferred_clinic'=>'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if(!empty($data['primary_concern'])){
                $data['primary_concern']    =   implode(',',$data['primary_concern']);  
            }
            unset($data['_token']);

            if ($id != null) {
                $data['seminar']    =   $id;
            }
            $data['added_by']       =   $this->id;
            $Patients   =   new Patients;
            $Patients->insert($data);
            return redirect('patients')->with('message', 'Patient sucessfully added');
        }
        $data = array(
            "page_title"   =>   $this->module." Management | Add ".$this->module,
            "page_heading" =>   $this->module." Management | Add ".$this->module,
            "module"       =>   $this->module,
            "breadcrumbs"  =>   array("dashboard" => "Home", url('patients') => "Patients List",'#'=>'Add Patient'),
        );
        if (Auth::user()->role == 0 || Auth::user()->role == 7 ) {
            $data['clinics']            =   Locations::all();
            $data['primaryConcern']     =   Primary::all();
        }else{
            $userClinics = explode(",",Auth::user()->location_id);
            $data['clinics']            =   Locations::whereIn('loc_id', $userClinics)->get();
            $data['primaryConcern']     =   Primary::all();
        }
        if ($id != null) {
            $data['seminar'] =  $id;
        }

        return view($this->view.'.add-view', $data);
    }

    public function filterProvider(Request $request) {
        $location_id        =   $request->all();
        $location_id        =   $location_id['param'];
        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
            $result['manager']  =   User::select('name', 'id')->whereRaw("FIND_IN_SET('".$location_id."',location_id)")->whereRaw('FIND_IN_SET(2,positions)')->where('role', '3')->distinct()->get(); 
            $result['patients'] =   Patients::where('preferred_clinic', $location_id)->get();
        }
        else{
            $result['manager']  =   User::select('name', 'id')->whereRaw("FIND_IN_SET('".$location_id."',location_id)")->whereRaw('FIND_IN_SET(2,positions)')->where(array('role' => '3'))->distinct()->get(); 
        }
        $result['physician']=   User::select('name', 'id')->whereRaw("FIND_IN_SET('".$location_id."',location_id)")->where(array('role' => '2'))->distinct()->get(); 
        echo json_encode($result);
    }
    public function delete($id) {
        $patient   = new Patients;
        $patient->find($id);
        Patients::destroy($id);
        $response = array('flag'=>true,'msg'=>'Patient has been deleted.');
        echo json_encode($response); return;
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == 'POST'){ 
            $data   =   $request->all();
            $id     =   $data['pa_id'];
            $validator  =   Validator::make($data,[
                "first_name"    =>  "required",
                "last_name"     =>  "required",
                "email"         =>  "required|unique:patients,email,$id,pa_id",
                "haboutus"      =>  "required",
                "phone_number"  =>  "required",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if(!empty($data['primary_concern'])){
                $data['primary_concern']    =   implode(',',$data['primary_concern']);  
            }
            if(isset($data['_token'])) unset($data['_token']);
            $patient  = Patients::find($id);
            $patient->update($data);
            return redirect('patient/update/'.$id)->with('message', 'Patient updated sucessfully!');
            
        }
        $data = array(
            "page_title"   =>   "Edit Patient",
            "page_heading" =>   "Edit Patient",
            "module"       =>   $this->module,
            "breadcrumbs"  =>   array("dashboard" => "Home", url('patients') => "Patient List",'#'=>'Edit Patient'),
        );
        
        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
            $data['clinics']    =   Locations::all();
            $data['primaryConcern']     =   Primary::all();
        }else{
            $userClinics = explode(",",Auth::user()->location_id);
            $data['clinics']            =   Locations::whereIn('loc_id', $userClinics)->get();
            $data['primaryConcern']     =   Primary::all();
        }

        $data['patient']    =   Patients::find($id)->toArray();
        $data['manager']    =   User::select("name", "id")->join('patients', 'patients.case_manager', '=', 'users.id')->where('pa_id', $id)->get();
        $data['physician']  =   User::select("name", "id")->join('patients', 'patients.primary_physician', '=', 'users.id')->where('pa_id', $id)->get();

        if ($data['manager']->isEmpty()) {
            $data['manager'][0] = array('id' => '', 'name' => '');
        }
        if ($data['physician']->isEmpty()) {
            $data['physician'][0] = array('id' => '0', 'name' => '');
        }
        
        $data['current_tab']    =   'patient_info';
        $data['registrant_id']  =   $id;
        return view('patients.tabs.edit-view',$data);
    }

    public function completedForm($id){
        $data = array(
            "page_title"        =>  "Patient Log",
            "page_heading"      =>  "View Patient Log",
            "module"            =>  $this->module,
            "breadcrumbs"       =>  array("dashboard" => "Home", url('patients') =>  ucfirst($this->plural)." List" , '#' =>"Patient Log"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log';
        $data['current_pill']   =   'Completed Forms';
        $data['patient']        =   Patients::find($id)->toArray();
        $data['appointments'] = DB::table('appointments')
                        ->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')
                        ->join('locations', 'locations.loc_id', '=', 'appointments.location_id')
                        ->join('users', 'users.id', '=', 'appointments.doctor_id')
                        /*->select('patients.first_name as p_fname',
                                  'patients.last_name as p_lname',
                                  )*/
                        ->where('patients.pa_id', '=', $id)->distinct('appointment_id')
                        ->get();
        return view('patients.tabs.forms', $data); 
    }

    public function viewSeminarNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log';
        $data['current_pill']   =   'Seminar Notes';
        $data['patient']        =   Patients::find($id)->toArray();

        $seminar_id             =   Patients::find($id)->seminar;
        $notes                  =   DB::table('notes')
                                    ->join('users','users.id','=','notes.note_user_id')
                                    ->where(['notes.note_seminar_id'=>$seminar_id,'notes.note_pa_id'=>$id])
                                    ->select('users.name','notes.*')->get();
        $data['notes']          =   collect($notes)->map(function($x){ return (array)$x; })->toArray();
        return view('patients.tabs.seminar-notes', $data); 
    }

    public function viewConsultationNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log ';
        $data['current_pill']   =   'Consultation Notes';
        $data['patient']        =   Patients::find($id)->toArray();
        $data['appointments'] = DB::table('appointments')
                        ->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')
                        ->join('locations', 'locations.loc_id', '=', 'appointments.location_id')
                        ->join('users', 'users.id', '=', 'appointments.doctor_id')
                        /*->select('patients.first_name as p_fname',
                                  'patients.last_name as p_lname',
                                  )*/
                        ->where('patients.pa_id', '=', $id)->distinct('appointment_id')
                        ->get();
        return view('patients.tabs.consultation-notes', $data); 
    }

    public function viewExamNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log';
        $data['current_pill']   =   'Exam Notes';
        $data['patient']        =   Patients::find($id)->toArray();
        $data['appointments'] = DB::table('appointments')
                        ->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')
                        ->join('locations', 'locations.loc_id', '=', 'appointments.location_id')
                        ->join('users', 'users.id', '=', 'appointments.doctor_id')
                        /*->select('patients.first_name as p_fname',
                                  'patients.last_name as p_lname',
                                  )*/
                        ->where('patients.pa_id', '=', $id)->distinct('appointment_id')
                        ->get();
        return view('patients.tabs.exam-notes', $data); 
    }

    public function viewTreatmentNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log';
        $data['current_pill']   =   'Treatment Notes';
        $data['patient']        =   Patients::find($id)->toArray();
        $data['appointments'] = DB::table('appointments')
                        ->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')
                        ->join('locations', 'locations.loc_id', '=', 'appointments.location_id')
                        ->join('users', 'users.id', '=', 'appointments.doctor_id')
                        /*->select('patients.first_name as p_fname',
                                  'patients.last_name as p_lname',
                                  )*/
                        ->where('patients.pa_id', '=', $id)->distinct('appointment_id')
                        ->get();
        return view('patients.tabs.treatment-notes', $data); 
    }
    public function viewAppointments($id){
        $data = array(
            "page_title"    =>  "Patient Appointment",
            "page_heading"  =>  "View Patient Appointment",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('patients') =>  ucfirst($this->plural)." List" ,'#'=>'Patient Appointments'),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'appointments';
        $data['patient']['pa_id']          =   $id;
        $data['appointments'] = DB::table('appointments')
                        ->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')
                        ->join('locations', 'locations.loc_id', '=', 'appointments.location_id')
                        ->join('users', 'users.id', '=', 'appointments.doctor_id')
                        ->where('patients.pa_id', '=', $id)->distinct('appointment_id')
                        ->get();
        $data['userlist']   =   User::all()->toArray();
        return view('patients.tabs.appointments', $data);
    }

    public function viewInvoices($id){
        $data = array(
            "page_title"    =>  "Patient Invoices",
            "page_heading"  =>  "View Patient Invoices",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('patients') =>  ucfirst($this->plural)." List" , '#' =>" Patient Invoices"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'invoices';
        $data['patient']['pa_id']          =   $id;

        return view('patients.tabs.invoices', $data);
    }

    public function viewSeminars($id){
        $data = array(
            "page_title"    =>  "Patient Seminar",
            "page_heading"  =>  "View Patient Seminar",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('patients') =>  ucfirst($this->plural)." List" ,'#' =>" Patient Seminar"),
        );
        $seminars   =   DB::table('patients')
                        ->join('seminars','seminars.seminar_id','=','patients.seminar')
                        ->select('patients.attendance','seminars.*')
                        ->where('patients.pa_id',$id)
                        ->get();
        $data['seminars']       =   collect($seminars)->map(function($x){ return (array)$x; })->toArray();
        $data['registrant_id']  =   $id;
         $data['patient']['pa_id']          =   $id;
        $data['current_tab']    =   'seminars';
        return view('patients.tabs.seminars', $data);
    }

    public function create_invoice($id){
        $data = array(
            "page_title"    =>  "Create Patient invoice",
            "page_heading"  =>  "Create Patient invoice",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('patient') =>  ucfirst($this->plural)." List" ,'#' =>"Create Patient Invoice"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'invoices';
        $data['patient']  = Patients::find($id);

        return view('patients.tabs.create_invoices', $data);
    }

	
}
