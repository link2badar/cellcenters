<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Seminars;
use App\Models\Clinics;
use App\Models\SeminarUsers;
use App\Models\SeminarsAdmins;
use App\Models\Patients;
use App\Models\Managers;
use App\Models\Physicians;
use App\Models\Notes;
use App\Models\Primary;
use App\Models\Locations;
use Auth,URL,Session,Redirect,DB;
use App\user;

class SeminarController  extends Controller{

    private $plural     =  "Seminars";    
	private $module     =  "Seminar";
    private $view       =  "seminars/";
    private $user_id;  
    private $locAuth;   
	
    public function __construct()	{
        $this->middleware('auth');
        $this->user_id      =  Auth()->user()->id;
        $locAuth            =  explode(",",Auth::user()->location_id);
        $this->locAuth      =  $locAuth[0];

	}
	
    /* MAIN SEMINAR SECTION */
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $query              =   DB::table('seminars');
        if($request->has('start_date') && $request->has('end_date'))
            $query->whereBetween('date',[$request->get('start_date'),$request->get('end_date')])->orderBy('date','asc')->distinct();
        if($request->has('city'))
            $query->where('city' , $request->get('city'));
        if($request->has('title'))
            $query->where('title' , $request->get('title'));
        if($request->has('state'))
            $query->where('state', $request->get('state'));
        if($request->has('zip_code'))
            $query->where('zip_code' , $request->get('zip_code'));
        if($request->has('location_id'))
            $query->where('location_id' , $request->get('location_id'));

        if(Auth::user()->role == 0 || Auth::user()->role == 7){
            $seminars           = $query->where('seminar_id', '>', 0)->get();
            $data['locations']     =   Locations::all()->toArray();
        }
        else if(Auth::user()->role !=6){
            $seminars           =   $query->where('location_id', $this->locAuth)->get();
        }
        if (Auth::user()->role == 6) {
            $seminars       =   $query->join('seminar_admins', 'seminar_admins.seminar', '=', 'seminars.seminar_id')->where('seminar_admins.user', Auth::user()->id)->get();
        }
        $data['seminars']   =   collect($seminars)->map(function($x){ return (array)$x; })->toArray();
        if(Auth::user()->role == 0 || Auth::user()->role == 7){
            $data['cities']     =   Seminars::where('seminar_id', '>', 0)->distinct('city')->get(['city'])->toArray();
            $data['states']     =   Seminars::where('seminar_id', '>', 0)->distinct('state')->get(['state'])->toArray();
        }else{
            $data['cities']     =   Seminars::where('location_id', $this->locAuth)->distinct('city')->get(['city'])->toArray();
        $data['states']     =   Seminars::where('location_id', $this->locAuth)->distinct('state')->get(['state'])->toArray();
        }
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->has('title')){
            $data       =   $request->all();
            if ($request->hasFile('image')) {
                $file            =  $request->file('image');
                $destinationPath =  base_path() . '/public/seminars_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            $data['start_time']         =   strtotime($data['start_time']);
            $data['end_time']           =   strtotime($data['end_time']);
            unset($data['_token']);
            $seminar        =   new Seminars();
            $seminar_id     =   $seminar->create($data)->seminar_id;
            if(!empty($data['seminar_admin_ids'])){
                $all_seminar_users  =   [];
                $i                  =   0;
                foreach ($data['seminar_admin_ids'] as $seminar_user) {
                    $all_seminar_users[$i]['seminar']    =   $seminar_id;
                    $all_seminar_users[$i]['user']       =   (int)$seminar_user;
                    $i++;
                }
                SeminarsAdmins::insert($all_seminar_users);
            }
            return redirect('seminars')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add ". $this->module,
            "page_heading"  =>  "Add ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" , '#' =>'Add '.ucfirst($this->module)),
        );
        $data['clinics']    =   Locations::all()->toArray();
        $data['users']      =   User::whereRaw("FIND_IN_SET('4',positions)")->get();
        return view($this->view.'.add-view' , $data);
    }

    public function viewDetail($id){
        $data = array(
            "page_title"    =>  $this->module." Detail",
            "page_heading"  =>  $this->module." Detail",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') => ucfirst($this->plural)." List" , '#' =>ucfirst($this->module)." Detail"),
        );
        $data['seminar']    =   Seminars::find($id)->toArray();
        $admins =   DB::table('seminar_admins')
                    ->Join('users','users.id','=','seminar_admins.user')
                    ->select('seminar_admins.*','users.name')
                    ->where('seminar_admins.seminar',$id)
                    ->get();
        $data['seminar_admins']     =   collect($admins)->map(function($x){ return (array) $x; })->toArray();
        $data['registrants']        =   Patients::where(['seminar'=>$id])->get()->toArray();
        $data['primaryConcern']     =   Primary::all()->toArray();

        //haboutus count
        for ($i=0; $i < 10; $i++) { 
            $data['haboutus'][$i]   =   Patients::where(['seminar'=>$id, 'haboutus' => $i])->get()->count();
        }
        return view($this->view.'.view-detail' , $data);  
    }

    public function update(Request $request,$id = NULL) {
        if($request->has('title')){
            $data   =   $request->all();
            //echo "<pre>";print_r($data);die;
            if ($request->hasFile('image')) {
                $file            =  $request->file('image');
                $destinationPath =  base_path() . '/public/seminars_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            $data['start_time']         =   strtotime($data['start_time']);
            $data['end_time']           =   strtotime($data['end_time']);
            unset($data['_token']);
            $seminar                    =   Seminars::find($data['seminar_id']);
            $seminar->update($data);
            if(!empty($data['seminar_admin_ids'])){
                $all_seminar_users  =   [];
                $i                  =   0;
                foreach ($data['seminar_admin_ids'] as $seminar_user) {
                    $all_seminar_users[$i]['seminar']    =   $data['seminar_id'];
                    $all_seminar_users[$i]['user']       =   (int)$seminar_user;
                    $i++;
                }
                SeminarsAdmins::where('seminar',$data['seminar_id'])->delete();
                SeminarsAdmins::insert($all_seminar_users);
            }
            return redirect(URL::previous())->with('message', 'Seminar has been updated Successfully');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['seminar']    =   Seminars::find($id)->toArray();
        $data['clinics']    =   Locations::all()->toArray();
        $data['seminarUser']=   SeminarsAdmins::where('seminar',$id)->get(['user']);
        
        $seminar_admins     =   SeminarsAdmins::where('seminar',$id)->get(['user'])->toArray();
        $admins_ids         =   array_column($seminar_admins,'user');
        $data['usersOut']   =   User::whereNotIn('id',$admins_ids)->whereRaw("FIND_IN_SET('5',positions)")->get()->toArray();
        $data['usersIn']    =   User::whereIn('id',$admins_ids)->whereRaw("FIND_IN_SET('5',positions)")->get()->toArray();
        //echo "<pre>";print_r($data);die;
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $employee   =  Clinics::find($id);
        $employee->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }

    /* SEMINAR ADMIN SECTION */
    public function seminarAdminModal(Request $request){
        $data['title']      =   "Add ".$this->module." Admin";
        $data['seminar_id'] =   $request->get('param');
        $seminar_admins     =   SeminarsAdmins::where('seminar',$data['seminar_id'])->get(['user'])->toArray();
        $admins_ids         =   array_column($seminar_admins,'user');
        $data['users']      =   User::whereNotIn('id',$admins_ids)->where("role", '6')->get()->toArray();
        return view($this->view.'add-seminar-admin-modal',$data);
    }

    public function addSeminarAdmin(Request $request){
        $data   =   $request->all();
        SeminarsAdmins::create($data);
        return redirect('seminar/detail/'.$data['seminar'])->with('message','Seminar Admin Has Been Added Successfully'); 
    }

    public function updateSeminarAdmin(Request $request){
        $data                       =   $request->all();
        $data_array                 =   explode('-',$data['user']);
        $data['user']               =   $data_array[1];
        SeminarsAdmins::where('seminar',$data['seminar_id'])->update(['admin'=>0]);
        $user                       =   SeminarsAdmins::find($data_array[0])->update($data);
        $response                   =   array('flag'=>true,'message'=>'A presenter has been selected for this seminar!','reload'=>true);
        echo json_encode($response); return;
    }

    public function deleteSeminarAdmin($id){
        $seminar_admin   =  SeminarsAdmins::find($id);
        $seminar_admin->delete();
        $response = array('flag' => true, 'message' => 'Seminar User Has Been Deleted','reload'=>true);
        echo json_encode($response);
    }

    /* SEMINAR REGISTRANT SECTION */
    public function seminarAddRegistrant(Request $request , $id = null){
        if($request->input('first_name')){
            $data                       =   $request->all();
            $data['seminar_registrant'] =   1;
            $other_registrant           =   array();
            if(!empty($data['guest'])){
                $other_registrant   =   $data['guest'];
                unset($data['guest']);
                foreach ($other_registrant as $key => $registrant) {
                    if(!empty($registrant['first_name']) && !empty($registrant['last_name'])){
                        $other_registrant[$key]['seminar']     =   $data['seminar'];
                        $other_registrant[$key]['haboutus']    =   $data['haboutus'];
                    }else
                        unset($other_registrant[$key]);
                }
            }
            unset($data['_token']);
            $Patients   =   new Patients;
            $Patients->insert($data);
            if(!empty($other_registrant))
                Patients::insert($other_registrant);
            return redirect('seminar/detail/'.$data['seminar'])->with('message','Seminar Registrant Added Successfully!');
        }
        $data = array(
            "page_title"    =>  "Add ". $this->module." Registrant",
            "page_heading"  =>  "Add ". $this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Add '.ucfirst($this->module)." Registrant"),
        );
        $data['seminar_id'] =   $id;   
        return view('seminar-registrants.add-view' , $data);
    }

    public function deleteSeminarRegistrant($id){
        $patient = Patients::find($id)->delete();
        $response = array('flag' => true, 'message' => 'Seminar Registrant Has Been Deleted','reload' => true);
        echo json_encode($response);
    }

    public function markRegistrantAttendance(Request $request){
        $data       =   $request->all();
        $params     =   explode('-',$data['param']);
        $registrant =   Patients::find($params[1])->update(['attendance'=>$params[0]]); 
        $response = array('flag' => true, 'message' => 'Registrant Attendance Has Been Marked','reload' => false);
        echo json_encode($response); 
    }

    public function reschedule(Request $request) {
        $data['patientID']       =   $request->get('param');
        $data['seminarID']       =   $request->get('param2');
        $data['title']           =   "Update ".$this->module;
        $data['currentSeminar']   =   Seminars::where('seminar_id'  ,$data['seminarID'])->get();
        $data['allSeminar']       =   Seminars::where('seminar_id','<>', $data['seminarID'])->get();
        return view($this->view.'reschedule',$data);
    }

    public function updateSchedule(Request $request){
        $data                       =   $request->all();
        $patientID                  =   $data['patientID'];
        $oldSeminar                 =   $data['oldSeminar'];
        unset($data['patientID'], $data['oldSeminar']);
        Patients::where('pa_id',$patientID)->update(['seminar'=>$data['seminarID']]);
        return redirect('seminar/detail/'.$oldSeminar)->with('message','Seminar Reschudle for this patient!');
    }

    /*public function viewEditRegistrant($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        
        $data['registrant']     =   Patients::find($id)->toArray();
        $data['clinics']        =   Clinics::all()->toArray();
        $data['managers']       =   Managers::all()->toArray();
        $data['physicians']     =   Physicians::all()->toArray();
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'patient_info';
        return view('seminar-registrants.tabs.edit-view', $data);
    }*/

    /*public function updateRegistrant(Request $request){
        $data               =   $request->all();
        $seminar_registrant =   Patients::find($data['pa_id']);
        $seminar_registrant->update($data);
        return redirect(URL::previous())->with('message',$this->module.' Registrant has been updated sucessfully!');
    }*/

    /*public function viewPatientFroms($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'log';
        return view('seminar-registrants.tabs.forms', $data); 
    }

    public function viewSeminarNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'seminars';
        $seminar_id             =   Patients::find($id)->seminar;
        $notes                  =   DB::table('notes')
                                    ->join('users','users.id','=','notes.note_user_id')
                                    ->where(['notes.note_seminar_id'=>$seminar_id,'notes.note_pa_id'=>$id])
                                    ->select('users.name','notes.*')->get();
        $data['notes']          =   collect($notes)->map(function($x){ return (array)$x; })->toArray();
        return view('seminar-registrants.tabs.seminar-notes', $data); 
    }

    public function viewConsultationNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'seminars';
        return view('seminar-registrants.tabs.consultation-notes', $data); 
    }

    public function viewExamNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'seminars';
        return view('seminar-registrants.tabs.exam-notes', $data); 
    }

    public function viewTreatmentNotes($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'seminars';
        return view('seminar-registrants.tabs.treatment-notes', $data); 
    }*/

    /*public function viewAppointments($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'appointments';
        return view('seminar-registrants.tabs.appointments', $data);
    }

    public function viewInvoices($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'invoices';
        return view('seminar-registrants.tabs.invoices', $data);
    }*/

    /*public function viewSeminars($id){
        $data = array(
            "page_title"    =>  "Edit ".$this->module." Registrant",
            "page_heading"  =>  "Edit ".$this->module." Registrant",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('seminars') =>  ucfirst($this->plural)." List" ,url('seminar/detail/'.$id) => 'Seminar Detail' , '#' =>'Edit '.ucfirst($this->module)." Registrant"),
        );
        $seminars   =   DB::table('patients')
                        ->join('seminars','seminars.seminar_id','=','patients.seminar')
                        ->select('patients.attendance','seminars.*')
                        ->where('patients.pa_id',$id)
                        ->get();
        $data['seminars']       =   collect($seminars)->map(function($x){ return (array)$x; })->toArray();
        $data['registrant_id']  =   $id;
        $data['current_tab']    =   'seminars';
        return view('seminar-registrants.tabs.seminars', $data);
    }*/

    public function seminarCreateNoteModal(Request $request){
        $data['patient_id']     =   $request->get('param');
        $data['seminar_id']     =   Patients::find($data['patient_id'])->seminar;
        $data['user_id']        =   $this->user_id;
        return view('seminar-registrants.tabs.create-seminar-note-modal',$data);
    }

    public function seminarCreateNote(Request $request){
        $data               =   $request->all();
        $data['note_type']  =   1;
        $note               =   new Notes();  
        $note->create($data);
        $response           =   array('flag' => true, 'msg' => $this->module . ' Note has been added Successfully!','reload'=>true);
        echo json_encode($response);
    }

    public function seminarReadFullNote(Request $request){
        $note_id        =   $request->get('param');
        $data['note']   =   Notes::find($note_id)->toArray();
        return view('seminar-registrants.tabs.seminar-note-detail-modal',$data);
    }
}
