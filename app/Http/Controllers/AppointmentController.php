<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Physicians;
use App\Models\Patients;
use App\Models\Primary;
use App\Models\Locations;
use App\Models\Appointments;
use App\Models\LocationTiming;
use App\User;
use Auth,URL,Session,Redirect;
use DB;

class AppointmentController extends Controller{

    private $plural     =  "Appointments";    
    private $module     =  "Appointment";
    private $view       =  "appointments/";  
    private $locAuth;   
    private $id;

    public function __construct()	{
        $this->contants   =     config('constants.appmanangement');
        $locAuthArr       =     explode(",", Auth::user()->location_id);
        $this->locAuth    =     $locAuthArr[0];
        $this->id         =     Auth::user()->id;
    }

    public function index(Request $request, $id = null)	{
      $data = array(
        "page_title"    =>  'Calendar',
        "page_heading"  =>  'Calendar',
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => "Calendar")
    );

      if (Auth::user()->role == 0 || Auth::user()->role == 7) {
        $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where('location_id', $this->locAuth)->where('status', '<>', '3')->get()->toArray();
        $data['locations'] =    Locations::all()->toArray();
    }
    else{
        $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $this->locAuth, 'doctor_id' => $this->id))->where('status', '<>', '3')->get()->toArray();
    }
    if ($id != null) {
         $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $this->locAuth, 'doctor_id' => $id))->where('status', '<>', '3')->get()->toArray();
         $data['approvalCheck'] = $id;
    }
    if (Auth::user()->role == 3) {
        $data['physicians'] =   User::where('role',2)->whereRaw("FIND_IN_SET('".$this->locAuth."',location_id)")->get()->toArray();
    }
     $data['locationTiming']  =  LocationTiming::where('location', $this->locAuth)->get()->toArray();
    if (isset($_GET['location_id'])) {
        $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $_GET['location_id'], 'doctor_id' => $_GET['primary_physician']))->where('status', '<>', '3')->get()->toArray();
         $data['locationTiming']  =  LocationTiming::where('location', $_GET['location_id'])->get()->toArray();
    }
   
    return view($this->view.'.calendar',$data);
}

public function add(Request $request , $id = NULL) {

    if($request->has('doctor_id')){
        $data       =   $request->all();
        unset($data['_token']);
        if(!empty($data['primary_concerns'])){
            $data['primary_concerns']    =   implode(',',$data['primary_concerns']);  
        }
        if (Auth::user()->role == 0 || Auth::user()->role == 7) {
            $data['location_id']    =   $data['preferredClinics'];
            unset($data['preferredClinics']);
        }else{
            $data['location_id']        =   $this->locAuth;
        }
        $data['status']             =   1;
        $data['appcreated_at']      =   date("m/d/Y");
        $data['appcreated_by']      =   $this->id;
        $appointments               =   new Appointments();
        $appointments->insert($data);
        return redirect('/calendar?date='.date('d/m/Y'))->with('message', $this->module.' has been sucessfully added !');
    }

    $data = array(
        "page_title"    =>  "Add New ". $this->module,
        "page_heading"  =>  "Add New ". $this->module,
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", url('calendar') => " Calendar" , '#' =>'Add New '.ucfirst($this->module)),
    );
    if(!empty($id)){
        $data['data_row']   =  patients::find($id)->toArray();
    }

    if (Auth::user()->role == 0 || Auth::user()->role == 7) {
        $data['physicians'] =   User::where(array('role'=>2))->get()->toArray();  
        $data['patients']   =   Patients::get()->toArray();

    }else{
        $data['physicians'] =   User::where('role',2)->whereRaw("FIND_IN_SET('".$this->locAuth."',location_id)")->get()->toArray();
        $data['patients']   =   Patients::where('preferred_clinic', $this->locAuth)->get()->toArray();
    }
    $data['locations']      =   Locations::all()->toArray();
    $data['primaryConcern'] =   Primary::all();

    return view($this->view.'.add-view' , $data);
}

public function update(Request $request,$id = NULL) {
    if($request->has('first_name')){
        $data   =   $request->all();
        if ($request->hasFile('physician_image')) {
            $file            =  $request->file('physician_image');
            $destinationPath =  base_path() . '/public/physicians_imgs/';
            $filename        =  $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $data['image']   =  $filename;
        }
        unset($data['_token'],$data['physician_image']);
        $clinic  =   Physicians::find($id);
        $clinic->update($data);
        return redirect('physician/update/'.$id)->with('message', 'Contact sucessfully added');
    }
    $data = array(
        "page_title"    =>  "Edit ".$this->module,
        "page_heading"  =>  "Edit ".$this->module,
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", url('physicians') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
    );
    $data['physician']  =   Physicians::find($id)->toArray();
    return view($this->view.'.edit-view', $data);
}

public function delete($id) {
    $physician   =  Physicians::find($id);
    $physician->delete();
    $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
    echo json_encode($response);
}

public function filter_conditions($id) {
    $conditions   =  Patients::find($id)->toArray();
    print_r($conditions['primary_concern']);

}

public function reschedule($id) {

    $data = array(
        "page_title"    =>  "Reschedule ". $this->module,
        "page_heading"  =>  "Reschedule ". $this->module,
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", url('calendar') => " Calendar" , '#' =>'Reschedule '.ucfirst($this->module)),
    );
    $data['locations']      =   Locations::all()->toArray();
    $data['appointment']   =  Appointments::find($id)->toArray();
    $data['physicians'] =   User::where('role',2)->get()->toArray();
    $data['patients']   =   Patients::all()->toArray();
    $data['primaryConcern']     =   Primary::all();
    return view($this->view.'.reschedule' , $data);
}

public function update_reschedule(Request $request) {
    if($request->has('appointment_id')){
        $data       =   $request->all();
        unset($data['_token']);
        if(!empty($data['primary_concerns'])){
            $data['primary_concerns']    =   implode(',',$data['primary_concerns']);  
        }
        $appointments  =   Appointments::find($data['appointment_id']);
        $appointments->update($data);
        return redirect('/calendar?date='.date('d/m/Y'))->with('message', $this->module.' has been sucessfully updated !');
    }
}

public function viewDetail(Request $request, $id) {
    $data = array(
        "page_title"    =>  $this->module." Detail",
        "page_heading"  =>  $this->module." Detail",
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", url('/calendar?date='.date('d/m/Y')) => ucfirst($this->plural)." List" , '#' =>ucfirst($this->module)." Detail"),
    );
    $data['appointment']   =  Appointments::find($id)->toArray();
    $data['physicians'] =   User::where('role',2)->get()->toArray();
    $data['patients']   =   Patients::all()->toArray();
    return view($this->view.'.view-detail' , $data);
}
public function cancel_appointment(Request $request, $id){
    $data = array(
        "page_title"    =>  $this->module." Detail",
        "page_heading"  =>  $this->module." Detail",
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", url('/calendar?date='.date('d/m/Y')) => ucfirst($this->plural)." List" , '#' =>ucfirst($this->module)." Detail"),
    );
    $data['appointment']   =  Appointments::find($id)->toArray();
    $data['physicians'] =   User::where('role',2)->get()->toArray();
    $data['patients']   =   Patients::all()->toArray();
    return view($this->view.'cancel_appointment' , $data);

}

public function change_status(Request $request){
    if($request->has('appointment_id')){
        $data       =   $request->all();
        if($data['status'] == 2){
            $data['reason'] = '';
            $data['appointment_type']        =  7;
        }
        if(empty($data['status'])){
            $data['status'] = 1;
        }
        if ($data['status'] == 3) {
             $data['appointment_type']        =  10;
        }
        unset($data['_token']);
        if(!empty($data['primary_concerns'])){
            $data['primary_concerns']    =   implode(',',$data['primary_concerns']);  
        }
        $appointments  =   Appointments::find($data['appointment_id']);
        $appointments->update($data);
        return redirect('/calendar?date='.date('d/m/Y'))->with('message', $this->module.' has been sucessfully updated !');
    }
}

public function filterTime($day, $doctor_id) {
    if ($day == 0) {
        $day = 7;
    }
    if (Auth::user()->role == 0 || Auth::user()->role == 7) {
        $data         =       LocationTiming::where(array('location' => @$_GET['locid'], 'day_id' => $day))->get(['open_time', 'close_time'])->toArray();    
    } else{
        $data         =       LocationTiming::where(array('location' => $this->locAuth, 'day_id' => $day))->get(['open_time', 'close_time'])->toArray();
    }
    $data['open_time']      =       date("h:i", $data[0]['open_time']);
    $data['close_time']     =       date("h:i", $data[0]['close_time']);
    if($data['open_time'] != $data['close_time']){
        $duration = '60';  


        $array_of_time = array ();
        $start_time    = $data[0]['open_time'];//change to strtotime
        $end_time      = $data[0]['close_time']; //change to strtotime

        $add_mins  = $duration * 30;


        while ($start_time < $end_time) {
            $record   =   Appointments::where(array('doctor_id' => $doctor_id, 'appointment_date' => @$_GET['date'],'start_time' => date ("h:i A", $start_time)))->where('status', '!=', '3')->count();
            if ($record>0) {
                 $start_time += $add_mins;
            }else{
                $timeArray['from'][] = date ("h:i A", $start_time);
                 $start_time += $add_mins;
            }
        } 

        $duration = '60';
        $add_mins  = $duration * 30;
        $start_time    = $data[0]['open_time'];//change to strtotime
        $end_time      = $data[0]['close_time']; //change to strtotime
        $firstone      = 1;

        while ($start_time <= $end_time) { 
            $record   =   Appointments::where(array('doctor_id' => $doctor_id, 'appointment_date' => @$_GET['date'],'end_time' => date ("h:i A", $start_time)))->where('status', '!=', '3')->count();
            if ($record>0 || $firstone == 1) {
                $firstone++;
                 $start_time += $add_mins;
            }else{

                $timeArray['to'][] = date ("h:i A", $start_time);
                 $start_time += $add_mins;
            }

        } 

        echo json_encode($timeArray);
    }
    else{
        echo 0;
    }
    }

    public function cancel_appfilter(Request $request, $id = null){
        $data = array(
        "page_title"    =>  'Cancel Appointments',
        "page_heading"  =>  'Cancel Appointments',
        "module"        =>  $this->module,
        "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => "Calendar")
    );
    $data['show_cancel'] = 1;
    $data['locations'] =    Locations::all()->toArray();
    $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $this->locAuth, 'doctor_id' => $this->id))->where('status', '=', '3')->get()->toArray();
    if ($id != null) {
         $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $this->locAuth, 'doctor_id' => $id))->where('status', '=', '3')->get()->toArray();
         $data['approvalCheck'] = $id;
    }
    $data['locationTiming']  =  LocationTiming::where('location', $this->locAuth)->get()->toArray();
    if (Auth::user()->role == 3) {
        $data['physicians'] =   User::where('role',2)->whereRaw("FIND_IN_SET('".$this->locAuth."',location_id)")->get()->toArray();
    }
    if (isset($_GET['location_id'])) {
        $data['list']   =   Appointments::select('appointments.*', 'patients.pa_id', 'patients.first_name', 'patients.last_name')->join('patients', 'patients.pa_id', '=', 'appointments.patient_id')->where(array('location_id' => $_GET['location_id'], 'doctor_id' => $_GET['primary_physician']))->where('status', '=', '3')->get()->toArray();
         $data['locationTiming']  =  LocationTiming::where('location', $_GET['location_id'])->get()->toArray();
    }
    return view($this->view.'.calendar',$data);
    }
}
