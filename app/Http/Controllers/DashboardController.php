<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth,URL,Session,Redirect,Validator;
Use App\User;

class DashboardController extends Controller{
    
    private $singular = "Dashboard";
    private $plural   = "Dashboard";
    private $view     = "dashboard/";
    private $locAuth;  

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
    	$data = array();
        $data = array(
            "page_title"   => $this->plural,
            "page_heading" => $this->plural,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->plural),
        );
    	return view($this->view . 'dashboard', $data);
    }

    public function account_setting(Request $request){
        if($request->method() == "POST"){
            $data       =   $request->all();
            $id          = $data['id'];
            $validator  =   Validator::make($data,[
                'name'     =>   'required',
                "email"    =>   "required|unique:users,email,$id,id",
                'password' =>   'confirmed|min:6'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $user  =   User::find($id);
            if ($data['password'] == '') {
                $data['password'] = $user['password'];
            }else{
                $data['password']   =   bcrypt($data['password']);
            }

            $user->update($data);
            return redirect('account')->with('message', 'Account sucessfully updated');
        }
        $data = array();
        $data = array(
            "page_title"   => "Account Setting",
            "page_heading" => "Account Setting",
            "breadcrumbs"  => array("dashboard" => "Home", "#" => "Account Setting"),
        );
        return view($this->view . 'account_setting', $data);
    }
}
