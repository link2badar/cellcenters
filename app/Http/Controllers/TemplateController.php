<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\Patients;
use App\Models\Contacts;

use App\Models\Templates;
use App\Models\Utility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class TemplateController extends Controller
{
	private $storege = "template/";
    

	private $module = "template";
    private $contants;
	public function __construct()	{
	}
	
	
    public function index(Request $request)	{
		$data = array(
            "page_title"   => "Template Management | View All Template",
            "page_heading" => "Template Management | View All Template",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", '#'=>"Templates"),
        );
        $data['list']   =   Templates::where('location_id', Auth::user()->location_id)->get();        
		return view('template.view', $data);
	}
    public function add(Request $request)   {
        if($request->input('title')){
            $data = $request->all();
            unset($data['_token']);
            $data['location_id']    =   Auth::user()->location_id;
            $Contacts         = new Templates;
            $Contacts->insert($data);
            return redirect('templates')->with('message', 'Template sucessfully added');
        }
        $data = array(
            "page_title"   => "Add Template",
            "page_heading" => "Add Template",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", url('templates') => "Template List",'#'=>'Add Template'),
        );

        return view('template.addview', $data);
    }

    public function delete($id) {
        $Stores   = new Templates;
        $Stores->find($id);
        Templates::destroy($id);
        $response = array('flag'=>true,'msg'=>'Template has been deleted.');
        echo json_encode($response); return;
    }
    public function update(Request $request,$id = NULL) {
        if($request->input('title')){
            $data = $request->all();
            if(isset($data['_token'])) unset($data['_token']);        
            $store               = Templates::find($id);
            $store->update($data);
            return redirect('templates')->with('message', 'Template sucessfully added');          
        }
        $data = array(
            "page_title"   => "Edit Template",
            "page_heading" => "Edit Template",
            "breadcrumbs"  => array("dashboard" => "Home", url('templates') => "Template List",'#'=>'Update Template'),
        );
        $data['data_row']   =   Templates::find($id)->toArray();
        return view('template.editview', $data);
    }
	
}
