<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinics extends Model
{
    protected $table 		=	'clinics';
	protected $primaryKey 	=	'clinic_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['clinic_name','clinic_phone_no','clinic_cell_no','clinic_address','clinic_city','clinic_state','clinic_state','image'];
}
