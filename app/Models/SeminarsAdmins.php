<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarsAdmins extends Model
{
    protected $table 		=	'seminar_admins';
	protected $primaryKey 	=	'seminar_admin_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['seminar','user','admin'];
}
