<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seminars extends Model
{
    protected $table 		=	'seminars';
	protected $primaryKey 	=	'seminar_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['title','date','start_time','end_time','location_id','location_name','address','city','state','zip_code','country','neighborhood','registration_open','registrant_notification','description','private','image','attendance'];
}
