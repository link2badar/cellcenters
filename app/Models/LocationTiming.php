<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationTiming extends Model
{
    protected $table 		=	'location_timings';
	protected $primaryKey 	=	'loc_time_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['location','day_id','open_time','close_time'];
}
