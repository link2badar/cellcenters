<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $table 		=	'appointments';
	protected $primaryKey 	=	'appointment_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['doctor_id','patient_id','appointment_type','appointment_date','start_time','end_time','primary_concerns','appcreated_at','status','reason','consultation_notes','exam_notes','treatment_notes','completed_forms', 'appcreated_by'];
}
