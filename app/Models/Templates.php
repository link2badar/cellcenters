<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'templates';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = ['id','title','template_data', 'location_id'];
}
