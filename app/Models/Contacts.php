<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';
	protected $primaryKey = 'con_id';
	public $timestamps = false;
	protected $fillable = ['first_name','last_name','email','haboutus','refrel_name','phone_number','address','city','state','zipcode','date_birth','Gender','PreferredClinic','CaseManager','Message'];
}
