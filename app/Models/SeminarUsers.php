<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarUsers extends Model
{
    protected $table 		=	'seminar_users';
	protected $primaryKey 	=	'seminar_user_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['first_name','last_name','email','phone_no','cell_no','address','city','state','date_birth','gender','zipcode','image'];
}
