<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Primary extends Model
{
    protected $table = 'primary_concern';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = ['id','title', 'location_id'];
}
